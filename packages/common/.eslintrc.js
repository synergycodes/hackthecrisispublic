module.exports = {
    'extends': [
        'plugin:jest/recommended',
        '@synergycodes/eslint-config'
    ],
    'parser': '@typescript-eslint/parser',
    'plugins': [
        '@typescript-eslint',
        'import',
        'jest'
    ],
    'parserOptions': {
        'sourceType': 'module',
        'ecmaVersion': 7
    },
    'rules': {
        'eqeqeq': [
            'error',
            'always',
            {
                'null': 'ignore'
            }
        ],
        'import/no-duplicates': 'warn',
        'no-param-reassign': 'warn',
        'no-console': 'warn',
        'newline-per-chained-call': 'off',
        'no-magic-numbers': 'off',
        '@typescript-eslint/interface-name-prefix': [
            'error',
            {
                'prefixWithI': 'always',
                'allowUnderscorePrefix': false
            }
        ],
        'max-len': [
            'error',
            {
                'code': 80,
                'ignorePattern': '^import\\s.+\\sfrom\\s.+;$',
                'ignoreUrls': true
            }
        ],
        'import/no-default-export': 'error',
        'arrow-parens': 'warn',
        'operator-linebreak': [
            'error',
            'before'
        ],
        'function-paren-newline': [
            'error',
            'multiline-arguments'
        ],
        'import/order': ['warn',
            {
                'groups': [
                    'builtin',
                    'external',
                    'internal'
                ],
                'newlines-between': 'always'
            }
        ],
        'import/no-extraneous-dependencies': [
            'error',
            {
                'devDependencies': [
                    '**/*.test.ts*',
                    '**/test/**'
                ]
            }
        ],
        'indent': [
            1,
            4,
            {
                'SwitchCase': 1
            }
        ],
        'object-shorthand': ['error'],
        'quotes': [1, 'single'],
        'object-curly-spacing': [1, 'always'],
        'linebreak-style': ['error', 'unix'],
        '@typescript-eslint/no-unused-vars': [
            'warn'
        ]
    }
};
