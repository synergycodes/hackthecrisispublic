const getTSLinting = (filenames) => {
    const paths = filenames.join(' ');
    return [
        `eslint --fix --max-warnings 0 ${paths}`,
        'tsc --noEmit',
        `git add ${paths}`
    ];
};

const getStyleLinting = (filenames) => {
    const paths = filenames.join(' ');
    return [
        `stylelint --max-warnings 0 ${paths}`,
        `git add ${paths}`
    ];
};

module.exports = {
    getTSLinting,
    getStyleLinting
};
