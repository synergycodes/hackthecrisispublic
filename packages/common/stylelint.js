module.exports = {
    processors: [
        'stylelint-processor-styled-components'
    ],
    extends: [
        'stylelint-config-recommended',
        'stylelint-config-styled-components'
    ],
    rules: {
        indentation: 4,
        'selector-type-no-unknown': null,
        'font-family-no-missing-generic-family-keyword': null
    }
};
