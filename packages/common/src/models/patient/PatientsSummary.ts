export type PatientsSummary = {
    infected: number;
    quarantine: number;
    recovered: number;
    newApplications: number;
};
