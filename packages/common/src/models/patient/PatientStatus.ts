export enum PatientStatus {
    NewApplication = 'New application',
    InQuarantine = 'In quarantine',
    Infected = 'Infected',
    Healthy = 'Healthy'
};
