export enum PatientAgeRange {
    From1To10 =  '1 - 10',
    From11To20 =  '11 - 20',
    From21To30 =  '21 - 30',
    From31To40 =  '31 - 40',
    From41To50 =  '41 - 50',
    From51To60 =  '51 - 60',
    From61To70 =  '61 - 70',
    From71To80 =  '71 - 80',
    From81To90 =  '81 - 90',
    From91To100 =  '91 - 100'
};
