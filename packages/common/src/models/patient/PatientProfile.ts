import { PatientStatus } from './PatientStatus';
import { PatientCategory } from './PatientCategory';

export type PatientProfile = {
    id: number;
    name: string;
    surname: string;
    pesel?: string;
    phone?: string;
    email?: string
    isMainDeviceUser?: boolean;
    score?: number;
    category?: PatientCategory;
    deviceId?: string;
    status?: PatientStatus;
}
