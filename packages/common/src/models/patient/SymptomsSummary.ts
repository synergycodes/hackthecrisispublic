export type SymptomsSummary = {
    coughing?: boolean;
    dyspoena?: boolean;
    lastBodyTemperature?: number;
};
