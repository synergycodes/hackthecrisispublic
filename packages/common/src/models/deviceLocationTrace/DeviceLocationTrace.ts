import { Point } from './Point';

export type DeviceLocationTrace = {
    id: number;
    location: Point;
    deviceId: string;
    timestamp: Date;
};
