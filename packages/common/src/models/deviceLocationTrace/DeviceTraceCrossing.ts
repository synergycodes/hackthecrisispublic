import { Point } from './Point';

export type DeviceTraceCrossing = {
    distance: number;
    deviceId: string;
    timestamp: Date;
    withDevice: string;
    location?: Point | any;
}
