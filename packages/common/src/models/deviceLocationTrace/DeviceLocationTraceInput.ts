import { Point } from './Point';

export type DeviceLocationTraceInput = {
    location: Point;
    timestamp: Date;
    deviceId: string;
};
