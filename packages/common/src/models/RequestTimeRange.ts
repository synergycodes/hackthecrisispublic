export enum RequestTimeRange {
    Today = 'TODAY',
    ThisWeek = 'THIS_WEEK',
    LastTwoWeeks = 'LAST_TWO_WEEKS'
};
