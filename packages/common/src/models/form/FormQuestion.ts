import { FormQuestionType } from './FormQuestionType';

export type FormQuestion = {
    id: number;
    order: number;
    text: string;
    type: FormQuestionType;
    parent?: FormQuestion;
    children?: FormQuestion[];
};
