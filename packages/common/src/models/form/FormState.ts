import { FormAnswer } from './FormAnswer';

export type FormState = {
    id: number;
    answers: FormAnswer[];
    userId: number;
    createDate: Date;
};
