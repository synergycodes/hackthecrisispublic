import { FormQuestion } from './FormQuestion';

export type FormAnswer = {
    value: string;
    question: FormQuestion;
};
