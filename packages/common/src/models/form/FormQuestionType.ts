export enum FormQuestionType {
    Boolean = 'BOOLEAN',
    Group = 'GROUP',
    String = 'STRING',
    Number = 'NUMBER'
}
