const { CONFIG } = require('./src/config.ts');

module.exports = {
    type: CONFIG.DB_TYPE,
    host: CONFIG.DB_HOST,
    port: CONFIG.DB_PORT,
    username: CONFIG.DB_USERNAME,
    password: CONFIG.DB_PASSWORD,
    database: CONFIG.DB_NAME,
    synchronize: false,
    logging: false,
    entities: ['src/model/entities/**/*.ts'],
    subscribers: ['src/model/subscribers/**/*.ts'],
    migrations: ['migration/**/*.ts'],
    cli: {
        entitiesDir: 'src/model/entities',
        subscribersDir: 'src/model/subscribers',
        migrationsDir: 'migration'
    }
};
