import {MigrationInterface, QueryRunner} from "typeorm";

export class AddFormAnswerScore1584622424043 implements MigrationInterface {
    name = 'AddFormAnswerScore1584622424043'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "form_answer_score" ("questionId" integer NOT NULL, "answer" character varying NOT NULL, "score" integer NOT NULL, CONSTRAINT "PK_33640e0471e818216f34c5fe627" PRIMARY KEY ("questionId", "answer"))`, undefined);
        await queryRunner.query(`ALTER TABLE "device_location_trace" ALTER COLUMN "location" TYPE geography`, undefined);
        await queryRunner.query(`ALTER TABLE "form_answer_score" ADD CONSTRAINT "FK_de85bac9cd8912e2e88844907b6" FOREIGN KEY ("questionId") REFERENCES "form_question"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "form_answer_score" DROP CONSTRAINT "FK_de85bac9cd8912e2e88844907b6"`, undefined);
        await queryRunner.query(`ALTER TABLE "device_location_trace" ALTER COLUMN "location" TYPE geography(Geometry,0)`, undefined);
        await queryRunner.query(`DROP TABLE "form_answer_score"`, undefined);
    }

}
