import {MigrationInterface, QueryRunner} from "typeorm";

export class AddUserProfiles1584694893973 implements MigrationInterface {
    name = 'AddUserProfiles1584694893973'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "patient_profile" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "surname" character varying NOT NULL, "pesel" character varying, "phone" character varying, "email" character varying, "isMainDeviceUser" boolean DEFAULT false, "deviceId" character varying, CONSTRAINT "PK_17f75e7aa12a2d0b0c3924b2e81" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "device" ("id" character varying NOT NULL, "createDate" TIMESTAMP NOT NULL, CONSTRAINT "PK_2dc10972aa4e27c01378dad2c72" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`ALTER TABLE "form_state" DROP COLUMN "userId"`, undefined);
        await queryRunner.query(`ALTER TABLE "form_state" ADD "userId" integer`, undefined);
        await queryRunner.query(`ALTER TABLE "device_location_trace" ALTER COLUMN "location" TYPE geography`, undefined);
        await queryRunner.query(`ALTER TABLE "form_state" ADD CONSTRAINT "FK_53639ffaabd86f68ce2d4e8cf48" FOREIGN KEY ("userId") REFERENCES "patient_profile"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "patient_profile" ADD CONSTRAINT "FK_625efa0bcd355c8bcd7a6080d9a" FOREIGN KEY ("deviceId") REFERENCES "device"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "patient_profile" DROP CONSTRAINT "FK_625efa0bcd355c8bcd7a6080d9a"`, undefined);
        await queryRunner.query(`ALTER TABLE "form_state" DROP CONSTRAINT "FK_53639ffaabd86f68ce2d4e8cf48"`, undefined);
        await queryRunner.query(`ALTER TABLE "device_location_trace" ALTER COLUMN "location" TYPE geography(Geometry,0)`, undefined);
        await queryRunner.query(`ALTER TABLE "form_state" DROP COLUMN "userId"`, undefined);
        await queryRunner.query(`ALTER TABLE "form_state" ADD "userId" character varying NOT NULL`, undefined);
        await queryRunner.query(`DROP TABLE "device"`, undefined);
        await queryRunner.query(`DROP TABLE "patient_profile"`, undefined);
    }

}
