import {MigrationInterface, QueryRunner} from "typeorm";

export class AddUserLocationTrace1584609778481 implements MigrationInterface {
    name = 'AddUserLocationTrace1584609778481'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "device_location_trace" ("id" SERIAL NOT NULL, "deviceId" character varying NOT NULL, "location" geography NOT NULL, "timestamp" TIMESTAMP NOT NULL, CONSTRAINT "PK_f8260d509f9887f80f9eb06e216" PRIMARY KEY ("id"))`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "device_location_trace"`, undefined);
    }

}
