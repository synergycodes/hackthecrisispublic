import { MigrationInterface, QueryRunner } from 'typeorm';

export class ModifyNotNull1584820055602 implements MigrationInterface {
    name = 'ModifyNotNull1584820055602'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query('ALTER TABLE "patient_profile" ALTER COLUMN "name" DROP NOT NULL', undefined);
        await queryRunner.query('ALTER TABLE "patient_profile" ALTER COLUMN "surname" DROP NOT NULL', undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query('ALTER TABLE "patient_profile" ALTER COLUMN "surname" SET NOT NULL', undefined);
        await queryRunner.query('ALTER TABLE "patient_profile" ALTER COLUMN "name" SET NOT NULL', undefined);
    }

}
