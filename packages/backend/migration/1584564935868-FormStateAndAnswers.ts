import {MigrationInterface, QueryRunner} from "typeorm";

export class FormStateAndAnswers1584564935868 implements MigrationInterface {
    name = 'FormStateAndAnswers1584564935868'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "form_state" ("id" SERIAL NOT NULL, "userId" character varying NOT NULL, "createDate" TIMESTAMP NOT NULL, "changeDate" TIMESTAMP, CONSTRAINT "PK_bf9f89a062b90c348c7c8c72eac" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "form_answer" ("id" SERIAL NOT NULL, "value" character varying NOT NULL, "questionId" integer, "formStateId" integer, CONSTRAINT "PK_18043b968b1e988a4da85bb1666" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`ALTER TABLE "form_answer" ADD CONSTRAINT "FK_36cf2d1b54965b0303241157cc4" FOREIGN KEY ("questionId") REFERENCES "form_question"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "form_answer" ADD CONSTRAINT "FK_54af578697f8155733decc40c03" FOREIGN KEY ("formStateId") REFERENCES "form_state"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "form_answer" DROP CONSTRAINT "FK_54af578697f8155733decc40c03"`, undefined);
        await queryRunner.query(`ALTER TABLE "form_answer" DROP CONSTRAINT "FK_36cf2d1b54965b0303241157cc4"`, undefined);
        await queryRunner.query(`DROP TABLE "form_answer"`, undefined);
        await queryRunner.query(`DROP TABLE "form_state"`, undefined);
    }

}
