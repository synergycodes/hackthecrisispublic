import {MigrationInterface, QueryRunner} from "typeorm";

export class AddNotificationPushToken1584785587868 implements MigrationInterface {
    name = 'AddNotificationPushToken1584785587868'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "device_notification_configuration" ("deviceId" character varying NOT NULL, "pushToken" character varying NOT NULL, CONSTRAINT "PK_71cff3261740d794ce55dff8e4c" PRIMARY KEY ("deviceId"))`, undefined);
        await queryRunner.query(`ALTER TABLE "device_location_trace" ALTER COLUMN "location" TYPE geography`, undefined);
        await queryRunner.query(`ALTER TABLE "device_trace_crossing" ALTER COLUMN "location" TYPE geography`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "device_trace_crossing" ALTER COLUMN "location" TYPE geography(Geometry,0)`, undefined);
        await queryRunner.query(`ALTER TABLE "device_location_trace" ALTER COLUMN "location" TYPE geography(Geometry,0)`, undefined);
        await queryRunner.query(`DROP TABLE "device_notification_configuration"`, undefined);
    }

}
