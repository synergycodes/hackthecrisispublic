import {MigrationInterface, QueryRunner} from "typeorm";

export class DropPatientDeviceRelation1584739421469 implements MigrationInterface {
    name = 'DropPatientDeviceRelation1584739421469'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "patient_profile" DROP CONSTRAINT "FK_625efa0bcd355c8bcd7a6080d9a"`, undefined);
        await queryRunner.query(`ALTER TABLE "device_location_trace" ALTER COLUMN "location" TYPE geography`, undefined);
        await queryRunner.query(`ALTER TABLE "device_trace_crossing" ALTER COLUMN "location" TYPE geography`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "device_trace_crossing" ALTER COLUMN "location" TYPE geography(Geometry,0)`, undefined);
        await queryRunner.query(`ALTER TABLE "device_location_trace" ALTER COLUMN "location" TYPE geography(Geometry,0)`, undefined);
        await queryRunner.query(`ALTER TABLE "patient_profile" ADD CONSTRAINT "FK_625efa0bcd355c8bcd7a6080d9a" FOREIGN KEY ("deviceId") REFERENCES "device"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
    }

}
