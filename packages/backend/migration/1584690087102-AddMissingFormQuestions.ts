import { MigrationInterface, QueryRunner } from 'typeorm';
import * as _ from 'lodash/fp';

import { MOCK_QUESTIONS } from './__mock__/formQuestions';
import { MOCK_FORM_ANSWER_SCORE } from './__mock__/formAnwerScore';

export class AddMissingFormQuestions1584690087102
implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        // Remove current entities
        await queryRunner.query('DELETE FROM public.form_answer');
        await queryRunner.query('DELETE FROM public.form_state');
        await queryRunner.query('DELETE FROM public.form_answer_score');
        await queryRunner.query('DELETE FROM public.form_question');

        // Seed new questions
        const formQuestions = getFormQuestions();
        await queryRunner.query(`
        INSERT INTO "form_question"
            (id, "order", "text", "type", "parentId")
        VALUES
            ${formQuestions}
        `);

        // Seed new score
        const formAnswerScore = getFormAnswerScore();
        await queryRunner.query(`
        INSERT INTO "form_answer_score"
            ("questionId", "answer", "score")
        VALUES
            ${formAnswerScore}
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}

const getFormQuestions = () => {
    const prepareValues = ({ id, order, text, type, parentId }) => _.compact(
        [id, order, `'${text}'`, `'${type}'`, parentId ? parentId : 'null']
    );

    const stringifyValues = (values) => `(${_.join(', ', values)})`;

    const generateValues = _.flowRight(
        stringifyValues,
        prepareValues
    );

    const mapAllValues = _.flowRight(
        _.join(', \n'),
        _.map(generateValues)
    );

    return mapAllValues(MOCK_QUESTIONS as any);
};

const getFormAnswerScore = () => {
    const prepareValues = ({ questionId, answer, score }) => _.compact(
        [questionId, `'${answer}'`, score]
    );

    const stringifyValues = (values) => `(${_.join(', ', values)})`;

    const generateValues = _.flowRight(
        stringifyValues,
        prepareValues
    );

    const mapAllValues = _.flowRight(
        _.join(', \n'),
        _.map(generateValues)
    );

    return mapAllValues(MOCK_FORM_ANSWER_SCORE as any);
};
