import {MigrationInterface, QueryRunner} from "typeorm";

export class RemoveFormStateChangeDate1584608313985 implements MigrationInterface {
    name = 'RemoveFormStateChangeDate1584608313985'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "form_state" DROP COLUMN "changeDate"`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "form_state" ADD "changeDate" TIMESTAMP`, undefined);
    }

}
