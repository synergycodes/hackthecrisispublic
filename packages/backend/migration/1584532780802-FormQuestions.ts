import {MigrationInterface, QueryRunner} from "typeorm";

export class FormQuestions1584532780802 implements MigrationInterface {
    name = 'FormQuestions1584532780802'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "example" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, CONSTRAINT "PK_608dd5fd6f0783062b07346ed1c" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "form_question" ("id" SERIAL NOT NULL, "order" integer NOT NULL, "text" character varying NOT NULL, "type" character varying NOT NULL, "parentId" integer, CONSTRAINT "PK_5bc5aa86b9da4b82b41726d8126" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`ALTER TABLE "form_question" ADD CONSTRAINT "FK_110b8c8c872dd0718fa24728a97" FOREIGN KEY ("parentId") REFERENCES "form_question"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "form_question" DROP CONSTRAINT "FK_110b8c8c872dd0718fa24728a97"`, undefined);
        await queryRunner.query(`DROP TABLE "form_question"`, undefined);
        await queryRunner.query(`DROP TABLE "example"`, undefined);
    }

}
