import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateFormAnswers1584606963513 implements MigrationInterface {
    name = 'UpdateFormAnswers1584606963513'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "form_answer" DROP CONSTRAINT "PK_18043b968b1e988a4da85bb1666"`, undefined);
        await queryRunner.query(`ALTER TABLE "form_answer" DROP COLUMN "id"`, undefined);
        await queryRunner.query(`ALTER TABLE "form_answer" ADD CONSTRAINT "PK_de03eac3ad01e1ece7f9082f1df" PRIMARY KEY ("questionId", "formStateId")`, undefined);
        await queryRunner.query(`ALTER TABLE "form_answer" DROP CONSTRAINT "FK_36cf2d1b54965b0303241157cc4"`, undefined);
        await queryRunner.query(`ALTER TABLE "form_answer" DROP CONSTRAINT "FK_54af578697f8155733decc40c03"`, undefined);
        await queryRunner.query(`ALTER TABLE "form_answer" ALTER COLUMN "questionId" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "form_answer" ALTER COLUMN "formStateId" SET NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "form_answer" ADD CONSTRAINT "FK_36cf2d1b54965b0303241157cc4" FOREIGN KEY ("questionId") REFERENCES "form_question"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "form_answer" ADD CONSTRAINT "FK_54af578697f8155733decc40c03" FOREIGN KEY ("formStateId") REFERENCES "form_state"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "form_answer" DROP CONSTRAINT "FK_54af578697f8155733decc40c03"`, undefined);
        await queryRunner.query(`ALTER TABLE "form_answer" DROP CONSTRAINT "FK_36cf2d1b54965b0303241157cc4"`, undefined);
        await queryRunner.query(`ALTER TABLE "form_answer" ALTER COLUMN "formStateId" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "form_answer" ALTER COLUMN "questionId" DROP NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "form_answer" ADD CONSTRAINT "FK_54af578697f8155733decc40c03" FOREIGN KEY ("formStateId") REFERENCES "form_state"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "form_answer" ADD CONSTRAINT "FK_36cf2d1b54965b0303241157cc4" FOREIGN KEY ("questionId") REFERENCES "form_question"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "form_answer" DROP CONSTRAINT "PK_de03eac3ad01e1ece7f9082f1df"`, undefined);
        await queryRunner.query(`ALTER TABLE "form_answer" ADD "id" SERIAL NOT NULL`, undefined);
        await queryRunner.query(`ALTER TABLE "form_answer" ADD CONSTRAINT "PK_18043b968b1e988a4da85bb1666" PRIMARY KEY ("id")`, undefined);
    }

}
