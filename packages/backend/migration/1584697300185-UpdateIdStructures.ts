import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateIdStructures1584697300185 implements MigrationInterface {
    name = 'UpdateIdStructures1584697300185'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "device_location_trace" ALTER COLUMN "location" TYPE geography`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "device_location_trace" ALTER COLUMN "location" TYPE geography(Geometry,0)`, undefined);
    }

}
