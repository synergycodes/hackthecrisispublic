import {MigrationInterface, QueryRunner} from "typeorm";

export class AddFormAnswerScoreRelationOnAnswer1584627780738 implements MigrationInterface {
    name = 'AddFormAnswerScoreRelationOnAnswer1584627780738'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "device_location_trace" ALTER COLUMN "location" TYPE geography`, undefined);
        await queryRunner.query(`ALTER TABLE "form_answer" ADD CONSTRAINT "FK_c37997aaaf1b4ec7d499b99ce09" FOREIGN KEY ("questionId", "value") REFERENCES "form_answer_score"("questionId","answer") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "form_answer" DROP CONSTRAINT "FK_c37997aaaf1b4ec7d499b99ce09"`, undefined);
        await queryRunner.query(`ALTER TABLE "device_location_trace" ALTER COLUMN "location" TYPE geography(Geometry,0)`, undefined);
    }

}
