/* eslint-disable */
import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddPatientStatus1584707901173 implements MigrationInterface {
    name = 'AddPatientStatus1584707901173'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "patient_profile" ADD "status" character varying DEFAULT 'New application';`);
        await queryRunner.query(`UPDATE public."patient_profile" SET status = 'New application';`);
        await queryRunner.query(`ALTER TABLE public."patient_profile" ALTER COLUMN "status" SET NOT NULL;`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "patient_profile" DROP COLUMN "status"`, undefined);
    }

}
