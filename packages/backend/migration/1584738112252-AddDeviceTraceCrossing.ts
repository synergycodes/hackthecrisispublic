import {MigrationInterface, QueryRunner} from "typeorm";

export class AddDeviceTraceCrossing1584738112252 implements MigrationInterface {
    name = 'AddDeviceTraceCrossing1584738112252'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "device_trace_crossing" ("deviceId" character varying NOT NULL, "withDevice" character varying NOT NULL, "distance" integer NOT NULL, "location" geography NOT NULL, "timestamp" TIMESTAMP NOT NULL, CONSTRAINT "PK_1cc9b8da21e769c786e511b5ef7" PRIMARY KEY ("deviceId", "withDevice"))`, undefined);
        await queryRunner.query(`ALTER TABLE "device_location_trace" ALTER COLUMN "location" TYPE geography`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "device_location_trace" ALTER COLUMN "location" TYPE geography(Geometry,0)`, undefined);
        await queryRunner.query(`DROP TABLE "device_trace_crossing"`, undefined);
    }

}
