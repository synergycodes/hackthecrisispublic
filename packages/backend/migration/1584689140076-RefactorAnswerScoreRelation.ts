import {MigrationInterface, QueryRunner} from "typeorm";

export class RefactorAnswerScoreRelation1584689140076 implements MigrationInterface {
    name = 'RefactorAnswerScoreRelation1584689140076'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "device_location_trace" ALTER COLUMN "location" TYPE geography`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "device_location_trace" ALTER COLUMN "location" TYPE geography(Geometry,0)`, undefined);
    }

}
