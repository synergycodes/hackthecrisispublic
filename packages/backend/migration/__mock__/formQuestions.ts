/* eslint-disable max-len */
export const MOCK_QUESTIONS = [
    {
        id: 1,
        order: 1,
        type: 'boolean',
        parentId: null,
        text: 'Have you been in the coronavirus transmission areas during the last 14 days?'
    },
    {
        id: 2,
        order: 2,
        type: 'boolean',
        parentId: null,
        text: 'Have you had contact with a person who has been confirmed to have SARS CoV-2 infection in last 14 days?'
    },
    {
        id: 3,
        order: 3,
        type: 'number',
        parentId: null,
        text: 'How old are you?'
    },
    {
        id: 4,
        order: 4,
        type: 'number',
        parentId: null,
        text: 'What is your body temperature'
    },
    {
        id: 5,
        order: 5,
        type: 'group',
        parentId: null,
        text: 'Do you have this symptom?'
    },
    {
        id: 6,
        order: 1,
        type: 'boolean',
        parentId: 5,
        text: 'Cough'
    },
    {
        id: 7,
        order: 2,
        type: 'boolean',
        parentId: 5,
        text: 'Shortness of breath'
    },
    {
        id: 8,
        order: 6,
        type: 'string',
        parentId: null,
        text: 'Would you like to tell something more?'
    }
];
