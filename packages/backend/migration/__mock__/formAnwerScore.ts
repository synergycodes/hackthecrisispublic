export const MOCK_FORM_ANSWER_SCORE = [
    {
        questionId: 1,
        score: 10,
        answer: 'true'
    },
    {
        questionId: 1,
        score: 1,
        answer: 'false'
    },
    {
        questionId: 2,
        score: 10,
        answer: 'true'
    },
    {
        questionId: 2,
        score: 1,
        answer: 'false'
    },
    {
        questionId: 3,
        score: 5,
        answer: '1'
    },
    {
        questionId: 3,
        score: 5,
        answer: '2'
    },
    {
        questionId: 3,
        score: 5,
        answer: '3'
    },
    {
        questionId: 3,
        score: 5,
        answer: '4'
    },
    {
        questionId: 3,
        score: 5,
        answer: '5'
    },
    {
        questionId: 3,
        score: 5,
        answer: '6'
    },
    {
        questionId: 3,
        score: 5,
        answer: '7'
    },
    {
        questionId: 3,
        score: 5,
        answer: '8'
    },
    {
        questionId: 3,
        score: 5,
        answer: '9'
    },
    {
        questionId: 3,
        score: 5,
        answer: '10'
    },
    {
        questionId: 3,
        score: 5,
        answer: '11'
    },
    {
        questionId: 3,
        score: 5,
        answer: '12'
    },
    {
        questionId: 3,
        score: 5,
        answer: '13'
    },
    {
        questionId: 3,
        score: 5,
        answer: '14'
    },
    {
        questionId: 3,
        score: 5,
        answer: '15'
    },
    {
        questionId: 3,
        score: 5,
        answer: '16'
    },
    {
        questionId: 3,
        score: 5,
        answer: '17'
    },
    {
        questionId: 3,
        score: 5,
        answer: '18'
    },
    {
        questionId: 3,
        score: 5,
        answer: '19'
    },
    {
        questionId: 3,
        score: 5,
        answer: '20'
    },
    {
        questionId: 3,
        score: 5,
        answer: '21'
    },
    {
        questionId: 3,
        score: 5,
        answer: '22'
    },
    {
        questionId: 3,
        score: 5,
        answer: '23'
    },
    {
        questionId: 3,
        score: 5,
        answer: '24'
    },
    {
        questionId: 3,
        score: 5,
        answer: '25'
    },
    {
        questionId: 3,
        score: 10,
        answer: '26'
    },
    {
        questionId: 3,
        score: 10,
        answer: '27'
    },
    {
        questionId: 3,
        score: 10,
        answer: '28'
    },
    {
        questionId: 3,
        score: 10,
        answer: '29'
    },
    {
        questionId: 3,
        score: 10,
        answer: '30'
    },
    {
        questionId: 3,
        score: 10,
        answer: '31'
    },
    {
        questionId: 3,
        score: 10,
        answer: '32'
    },
    {
        questionId: 3,
        score: 10,
        answer: '33'
    },
    {
        questionId: 3,
        score: 10,
        answer: '34'
    },
    {
        questionId: 3,
        score: 10,
        answer: '35'
    },
    {
        questionId: 3,
        score: 10,
        answer: '36'
    },
    {
        questionId: 3,
        score: 10,
        answer: '37'
    },
    {
        questionId: 3,
        score: 10,
        answer: '38'
    },
    {
        questionId: 3,
        score: 10,
        answer: '39'
    },
    {
        questionId: 3,
        score: 10,
        answer: '40'
    },
    {
        questionId: 3,
        score: 10,
        answer: '41'
    },
    {
        questionId: 3,
        score: 10,
        answer: '42'
    },
    {
        questionId: 3,
        score: 10,
        answer: '43'
    },
    {
        questionId: 3,
        score: 10,
        answer: '44'
    },
    {
        questionId: 3,
        score: 10,
        answer: '45'
    },
    {
        questionId: 3,
        score: 15,
        answer: '46'
    },
    {
        questionId: 3,
        score: 15,
        answer: '47'
    },
    {
        questionId: 3,
        score: 15,
        answer: '48'
    },
    {
        questionId: 3,
        score: 15,
        answer: '49'
    },
    {
        questionId: 3,
        score: 15,
        answer: '50'
    },
    {
        questionId: 3,
        score: 15,
        answer: '51'
    },
    {
        questionId: 3,
        score: 15,
        answer: '52'
    },
    {
        questionId: 3,
        score: 15,
        answer: '53'
    },
    {
        questionId: 3,
        score: 15,
        answer: '54'
    },
    {
        questionId: 3,
        score: 15,
        answer: '55'
    },
    {
        questionId: 3,
        score: 15,
        answer: '56'
    },
    {
        questionId: 3,
        score: 15,
        answer: '57'
    },
    {
        questionId: 3,
        score: 15,
        answer: '58'
    },
    {
        questionId: 3,
        score: 15,
        answer: '59'
    },
    {
        questionId: 3,
        score: 15,
        answer: '60'
    },
    {
        questionId: 3,
        score: 15,
        answer: '61'
    },
    {
        questionId: 3,
        score: 15,
        answer: '62'
    },
    {
        questionId: 3,
        score: 15,
        answer: '63'
    },
    {
        questionId: 3,
        score: 15,
        answer: '64'
    },
    {
        questionId: 3,
        score: 15,
        answer: '65'
    },
    {
        questionId: 3,
        score: 20,
        answer: '66'
    },
    {
        questionId: 3,
        score: 20,
        answer: '67'
    },
    {
        questionId: 3,
        score: 20,
        answer: '68'
    },
    {
        questionId: 3,
        score: 20,
        answer: '69'
    },
    {
        questionId: 3,
        score: 20,
        answer: '70'
    },
    {
        questionId: 3,
        score: 20,
        answer: '71'
    },
    {
        questionId: 3,
        score: 20,
        answer: '72'
    },
    {
        questionId: 3,
        score: 20,
        answer: '73'
    },
    {
        questionId: 3,
        score: 20,
        answer: '74'
    },
    {
        questionId: 3,
        score: 20,
        answer: '75'
    },
    {
        questionId: 3,
        score: 20,
        answer: '76'
    },
    {
        questionId: 3,
        score: 20,
        answer: '77'
    },
    {
        questionId: 3,
        score: 20,
        answer: '78'
    },
    {
        questionId: 3,
        score: 20,
        answer: '79'
    },
    {
        questionId: 3,
        score: 20,
        answer: '80'
    },
    {
        questionId: 3,
        score: 20,
        answer: '81'
    },
    {
        questionId: 3,
        score: 20,
        answer: '82'
    },
    {
        questionId: 3,
        score: 20,
        answer: '83'
    },
    {
        questionId: 3,
        score: 20,
        answer: '84'
    },
    {
        questionId: 3,
        score: 20,
        answer: '85'
    },
    {
        questionId: 3,
        score: 20,
        answer: '86'
    },
    {
        questionId: 3,
        score: 20,
        answer: '87'
    },
    {
        questionId: 3,
        score: 20,
        answer: '88'
    },
    {
        questionId: 3,
        score: 20,
        answer: '89'
    },
    {
        questionId: 3,
        score: 20,
        answer: '90'
    },
    {
        questionId: 3,
        score: 20,
        answer: '91'
    },
    {
        questionId: 3,
        score: 20,
        answer: '92'
    },
    {
        questionId: 3,
        score: 20,
        answer: '93'
    },
    {
        questionId: 3,
        score: 20,
        answer: '94'
    },
    {
        questionId: 3,
        score: 20,
        answer: '95'
    },
    {
        questionId: 3,
        score: 20,
        answer: '96'
    },
    {
        questionId: 3,
        score: 20,
        answer: '97'
    },
    {
        questionId: 3,
        score: 20,
        answer: '98'
    },
    {
        questionId: 3,
        score: 20,
        answer: '99'
    },
    {
        questionId: 3,
        score: 20,
        answer: '100'
    },
    {
        questionId: 4,
        score: 15,
        answer: '32'
    },
    {
        questionId: 4,
        score: 15,
        answer: '32.1'
    },
    {
        questionId: 4,
        score: 15,
        answer: '32.2'
    },
    {
        questionId: 4,
        score: 15,
        answer: '32.3'
    },
    {
        questionId: 4,
        score: 15,
        answer: '32.4'
    },
    {
        questionId: 4,
        score: 15,
        answer: '32.5'
    },
    {
        questionId: 4,
        score: 15,
        answer: '32.6'
    },
    {
        questionId: 4,
        score: 15,
        answer: '32.7'
    },
    {
        questionId: 4,
        score: 15,
        answer: '32.8'
    },
    {
        questionId: 4,
        score: 10,
        answer: '32.9'
    },
    {
        questionId: 4,
        score: 10,
        answer: '33'
    },
    {
        questionId: 4,
        score: 10,
        answer: '33.1'
    },
    {
        questionId: 4,
        score: 10,
        answer: '33.2'
    },
    {
        questionId: 4,
        score: 10,
        answer: '33.3'
    },
    {
        questionId: 4,
        score: 10,
        answer: '33.4'
    },
    {
        questionId: 4,
        score: 10,
        answer: '33.5'
    },
    {
        questionId: 4,
        score: 10,
        answer: '33.6'
    },
    {
        questionId: 4,
        score: 10,
        answer: '33.7'
    },
    {
        questionId: 4,
        score: 10,
        answer: '33.8'
    },
    {
        questionId: 4,
        score: 10,
        answer: '33.9'
    },
    {
        questionId: 4,
        score: 10,
        answer: '34'
    },
    {
        questionId: 4,
        score: 10,
        answer: '34.1'
    },
    {
        questionId: 4,
        score: 10,
        answer: '34.2'
    },
    {
        questionId: 4,
        score: 10,
        answer: '34.3'
    },
    {
        questionId: 4,
        score: 10,
        answer: '34.4'
    },
    {
        questionId: 4,
        score: 10,
        answer: '34.5'
    },
    {
        questionId: 4,
        score: 10,
        answer: '34.6'
    },
    {
        questionId: 4,
        score: 10,
        answer: '34.7'
    },
    {
        questionId: 4,
        score: 10,
        answer: '34.8'
    },
    {
        questionId: 4,
        score: 10,
        answer: '34.9'
    },
    {
        questionId: 4,
        score: 5,
        answer: '35'
    },
    {
        questionId: 4,
        score: 5,
        answer: '35.1'
    },
    {
        questionId: 4,
        score: 5,
        answer: '35.2'
    },
    {
        questionId: 4,
        score: 5,
        answer: '35.3'
    },
    {
        questionId: 4,
        score: 5,
        answer: '35.4'
    },
    {
        questionId: 4,
        score: 5,
        answer: '35.5'
    },
    {
        questionId: 4,
        score: 5,
        answer: '35.6'
    },
    {
        questionId: 4,
        score: 1,
        answer: '35.7'
    },
    {
        questionId: 4,
        score: 1,
        answer: '35.8'
    },
    {
        questionId: 4,
        score: 1,
        answer: '35.9'
    },
    {
        questionId: 4,
        score: 1,
        answer: '36'
    },
    {
        questionId: 4,
        score: 1,
        answer: '36.1'
    },
    {
        questionId: 4,
        score: 1,
        answer: '36.2'
    },
    {
        questionId: 4,
        score: 1,
        answer: '36.3'
    },
    {
        questionId: 4,
        score: 1,
        answer: '36.4'
    },
    {
        questionId: 4,
        score: 1,
        answer: '36.5'
    },
    {
        questionId: 4,
        score: 1,
        answer: '36.6'
    },
    {
        questionId: 4,
        score: 1,
        answer: '36.7'
    },
    {
        questionId: 4,
        score: 1,
        answer: '36.8'
    },
    {
        questionId: 4,
        score: 1,
        answer: '36.9'
    },
    {
        questionId: 4,
        score: 1,
        answer: '37'
    },
    {
        questionId: 4,
        score: 5,
        answer: '37.1'
    },
    {
        questionId: 4,
        score: 5,
        answer: '37.2'
    },
    {
        questionId: 4,
        score: 5,
        answer: '37.3'
    },
    {
        questionId: 4,
        score: 5,
        answer: '37.4'
    },
    {
        questionId: 4,
        score: 5,
        answer: '37.5'
    },
    {
        questionId: 4,
        score: 5,
        answer: '37.6'
    },
    {
        questionId: 4,
        score: 5,
        answer: '37.7'
    },
    {
        questionId: 4,
        score: 5,
        answer: '37.8'
    },
    {
        questionId: 4,
        score: 5,
        answer: '37.9'
    },
    {
        questionId: 4,
        score: 10,
        answer: '38'
    },
    {
        questionId: 4,
        score: 10,
        answer: '38.1'
    },
    {
        questionId: 4,
        score: 10,
        answer: '38.2'
    },
    {
        questionId: 4,
        score: 10,
        answer: '38.3'
    },
    {
        questionId: 4,
        score: 10,
        answer: '38.4'
    },
    {
        questionId: 4,
        score: 10,
        answer: '38.5'
    },
    {
        questionId: 4,
        score: 10,
        answer: '38.6'
    },
    {
        questionId: 4,
        score: 10,
        answer: '38.7'
    },
    {
        questionId: 4,
        score: 10,
        answer: '38.8'
    },
    {
        questionId: 4,
        score: 10,
        answer: '38.9'
    },
    {
        questionId: 4,
        score: 15,
        answer: '39'
    },
    {
        questionId: 4,
        score: 15,
        answer: '39.1'
    },
    {
        questionId: 4,
        score: 15,
        answer: '39.2'
    },
    {
        questionId: 4,
        score: 15,
        answer: '39.3'
    },
    {
        questionId: 4,
        score: 15,
        answer: '39.4'
    },
    {
        questionId: 4,
        score: 15,
        answer: '39.5'
    },
    {
        questionId: 4,
        score: 15,
        answer: '39.6'
    },
    {
        questionId: 4,
        score: 15,
        answer: '39.7'
    },
    {
        questionId: 4,
        score: 15,
        answer: '39.8'
    },
    {
        questionId: 4,
        score: 15,
        answer: '39.9'
    },
    {
        questionId: 4,
        score: 15,
        answer: '40'
    },
    {
        questionId: 4,
        score: 15,
        answer: '40.1'
    },
    {
        questionId: 4,
        score: 15,
        answer: '40.2'
    },
    {
        questionId: 4,
        score: 15,
        answer: '40.3'
    },
    {
        questionId: 4,
        score: 15,
        answer: '40.4'
    },
    {
        questionId: 4,
        score: 15,
        answer: '40.5'
    },
    {
        questionId: 4,
        score: 15,
        answer: '40.6'
    },
    {
        questionId: 4,
        score: 15,
        answer: '40.7'
    },
    {
        questionId: 4,
        score: 15,
        answer: '40.8'
    },
    {
        questionId: 4,
        score: 15,
        answer: '40.9'
    },
    {
        questionId: 4,
        score: 15,
        answer: '41'
    },
    {
        questionId: 4,
        score: 15,
        answer: '41.1'
    },
    {
        questionId: 4,
        score: 15,
        answer: '41.2'
    },
    {
        questionId: 4,
        score: 15,
        answer: '41.3'
    },
    {
        questionId: 4,
        score: 15,
        answer: '41.4'
    },
    {
        questionId: 4,
        score: 15,
        answer: '41.5'
    },
    {
        questionId: 4,
        score: 20,
        answer: '41.6'
    },
    {
        questionId: 4,
        score: 20,
        answer: '41.7'
    },
    {
        questionId: 4,
        score: 20,
        answer: '41.8'
    },
    {
        questionId: 4,
        score: 20,
        answer: '41.9'
    },
    {
        questionId: 4,
        score: 20,
        answer: '42'
    },
    {
        questionId: 4,
        score: 20,
        answer: '42.1'
    },
    {
        questionId: 4,
        score: 20,
        answer: '42.2'
    },
    {
        questionId: 4,
        score: 20,
        answer: '42.3'
    },
    {
        questionId: 4,
        score: 20,
        answer: '42.4'
    },
    {
        questionId: 4,
        score: 20,
        answer: '42.5'
    },
    {
        questionId: 4,
        score: 20,
        answer: '42.6'
    },
    {
        questionId: 4,
        score: 20,
        answer: '42.7'
    },
    {
        questionId: 4,
        score: 20,
        answer: '42.8'
    },
    {
        questionId: 4,
        score: 20,
        answer: '42.9'
    },
    {
        questionId: 4,
        score: 20,
        answer: '43'
    },
    {
        questionId: 6,
        score: 10,
        answer: 'true'
    },
    {
        questionId: 6,
        score: 1,
        answer: 'false'
    },
    {
        questionId: 7,
        score: 10,
        answer: 'true'
    },
    {
        questionId: 7,
        score: 1,
        answer: 'false'
    }
];
