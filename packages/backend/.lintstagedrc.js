const helpers = require('@hackthecrisis/common/lintstaged');

module.exports = {
    'src/**/*.{js,ts,tsx}': helpers.getTSLinting
};
