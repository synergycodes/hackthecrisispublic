import { registerEnumType } from 'type-graphql';
import { FormQuestionType } from '@hackthecrisis/common';

registerEnumType(FormQuestionType, {
    name: 'FormQuestionType'
});
