import { Resolver, Query } from 'type-graphql';
import { getCustomRepository } from 'typeorm';

import { FormQuestionsRepository } from '../../model/repositories/FormQuestionsRepository';
import { FormQuestion } from './formQuestion.type';
import './formQuestionType.type';

@Resolver()
export class FormQuestionsResolver {

    @Query(() => [FormQuestion])
    async formQuestions(): Promise<FormQuestion[]> {
        return getCustomRepository(FormQuestionsRepository)
            .getWithChildren();
    }

}
