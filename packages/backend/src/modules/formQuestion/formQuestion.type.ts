import { ObjectType, Field, ID, Int } from 'type-graphql';
import { FormQuestion as BaseFormQuestion, FormQuestionType } from '@hackthecrisis/common';

@ObjectType()
export class FormQuestion implements BaseFormQuestion {

    @Field(() => ID)
    id: number;

    @Field(() => Int)
    order: number;

    @Field()
    text: string;

    @Field()
    type: FormQuestionType;

    @Field(
        () => [FormQuestion],
        {
            nullable: true
        }
    )
    children?: FormQuestion[];

}
