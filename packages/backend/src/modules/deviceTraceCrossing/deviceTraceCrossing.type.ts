import { ObjectType, Field } from 'type-graphql';
import { DeviceTraceCrossing as BaseDeviceTraceCrossing } from '@hackthecrisis/common';

@ObjectType()
class DeviceTraceCrossingLocation {
    @Field()
    lat: number;

    @Field()
    lon: number;
}

@ObjectType()
export class DeviceTraceCrossing implements BaseDeviceTraceCrossing{

    @Field()
    deviceId: string;

    @Field()
    distance: number;

    @Field(() => DeviceTraceCrossingLocation)
    location: DeviceTraceCrossingLocation;

    @Field()
    timestamp: Date;

    @Field()
    withDevice: string;

}
