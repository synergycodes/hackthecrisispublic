/* eslint-disable @typescript-eslint/indent */
import { Resolver, Query, Arg, Subscription, Root } from 'type-graphql';
import { getCustomRepository } from 'typeorm';

import { DeviceTraceCrossing } from './deviceTraceCrossing.type';
import { DeviceTraceCrossingRepository } from '../../model/repositories/DeviceTraceCrossingRepository';
import { DeviceTraceCrossing as DeviceTraceCrossingEntity } from '../../model/entities/DeviceTraceCrossing';
import { INFECTION_CROSSING_TOPIC } from '../../subscriptions/infectionCrossing';

@Resolver()
export class DeviceTraceCrossingResolver {

    @Query(() => [DeviceTraceCrossing])
    async deviceTraceCrossings(
        @Arg('deviceId', { nullable: true }) deviceId: string
    ): Promise<DeviceTraceCrossing[]> {
        return (await getCustomRepository(DeviceTraceCrossingRepository)
            .getForDevice(deviceId))
            .map(({ location, ...crossing }) => ({
                ...crossing,
                location: {
                    lon: location.coordinates[0],
                    lat: location.coordinates[1]
                }
            }));
    }

    @Subscription(
        () => DeviceTraceCrossing,
        {
            topics: INFECTION_CROSSING_TOPIC,
            filter: ({ payload, args }) => args.deviceId === payload.deviceId
        }
    )
    deviceTraceCrossing(
        @Arg('deviceId') deviceId: string,
        @Root() crossing: DeviceTraceCrossingEntity
    ) {
        const { location } = crossing;
        return {
            ...crossing,
            location: {
                lon: location.coordinates[0],
                lat: location.coordinates[1]
            }
        };
    }

}
