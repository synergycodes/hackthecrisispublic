import { Resolver, Query } from 'type-graphql';
import { getCustomRepository } from 'typeorm';

import { PatientsSummary } from './patientsSummary.type';
import { PatientProfileRepository } from '../../model/repositories/PatientProfileRepository';

@Resolver()
export class PatientsSummaryResolver {

    @Query(() => PatientsSummary)
    patientsSummary(): Promise<PatientsSummary> {
        return getCustomRepository(PatientProfileRepository)
            .getSummary();
    }

}
