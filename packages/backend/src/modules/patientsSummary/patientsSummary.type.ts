import { ObjectType, Field } from 'type-graphql';
import { PatientsSummary as BasePatientsSummary } from '@hackthecrisis/common';

@ObjectType()
export class PatientsSummary implements BasePatientsSummary {

    @Field()
    infected: number;

    @Field()
    quarantine: number;

    @Field()
    recovered: number;

    @Field()
    newApplications: number;

}
