import { registerEnumType } from 'type-graphql';
import { PatientStatus } from '@hackthecrisis/common';

registerEnumType(PatientStatus, {
    name: 'PatientStatus'
});
