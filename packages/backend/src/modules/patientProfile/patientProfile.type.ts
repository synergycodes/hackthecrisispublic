import { ObjectType, Field, ID, InputType } from 'type-graphql';
import { PatientProfile as BasePatientProfile, PatientStatus, PatientCategory } from '@hackthecrisis/common';

import { FormStateWithAnswers } from '../formState/formState.type';
import './patientProfileStatus.type';
import { SymptomsSummary } from '../symptomsSummary/symptomsSummary.type';

@ObjectType()
export class PatientProfile implements BasePatientProfile {

    @Field(() => ID)
    id: number;

    @Field({
        nullable: true
    })
    name: string;

    @Field({
        nullable: true
    })
    surname: string;

    @Field({
        nullable: true
    })
    pesel?: string;

    @Field({
        nullable: true
    })
    phone?: string;

    @Field({
        nullable: true
    })
    score?: number;

    @Field(
        () => String,
        {
            nullable: true
        }
    )
    category?: PatientCategory;

    @Field({
        nullable: true
    })
    email?: string;

    @Field()
    status?: PatientStatus;

    @Field({
        nullable: true
    })
    isMainDeviceUser?: boolean;

    @Field({
        nullable: true
    })
    deviceId?: string;

    @Field(
        () => [FormStateWithAnswers],
        {
            nullable: true
        }
    )
    formStates?: FormStateWithAnswers[];

    @Field({
        nullable: true
    })
    age: number;

    @Field(
        () => FormStateWithAnswers,
        {
            nullable: true
        }
    )
    newestFormState?: FormStateWithAnswers;

    @Field(
        () => SymptomsSummary,
        {
            nullable: true
        }
    )
    symptoms?: SymptomsSummary;
}

@InputType()
export class PatientProfileInputType extends PatientProfile {

    @Field(
        () => ID,
        {
            nullable: true
        }
    )
    id: number;

    @Field(
        {
            nullable: true
        }
    )
    name: string;

    @Field(
        {
            nullable: true
        }
    )
    surname: string;

    @Field({
        nullable: true
    })
    pesel?: string;

    @Field({
        nullable: true
    })
    phone?: string;

    @Field({
        nullable: true
    })
    email?: string;

    @Field({
        nullable: true
    })
    status?: PatientStatus;

    @Field({
        nullable: true
    })
    isMainDeviceUser?: boolean;

    @Field({
        nullable: true
    })
    deviceId?: string;
}
