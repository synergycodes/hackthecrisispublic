import { InputType, Field, registerEnumType } from 'type-graphql';
import { PatientStatus, PatientCategory, PatientAgeRange, RequestTimeRange } from '@hackthecrisis/common';

registerEnumType(PatientCategory, {
    name: 'PatientCategory'
});

registerEnumType(PatientAgeRange, {
    name: 'PatientAgeRange'
});

registerEnumType(RequestTimeRange, {
    name: 'RequestTimeRange'
});

@InputType()
export class PatientProfileFilterInput {

    @Field(
        () => [PatientCategory],
        {
            nullable: true
        }
    )
    categories?: PatientCategory[];

    @Field(
        () => [PatientStatus],
        {
            nullable: true
        }
    )
    statuses?: PatientStatus[];

    @Field(
        () => [PatientAgeRange],
        {
            nullable: true
        }
    )
    ageRanges?: PatientAgeRange[];

    @Field(
        () => RequestTimeRange,
        {
            nullable: true
        }
    )
    timeRange?: RequestTimeRange;

}
