import { Resolver, Mutation, Arg, Query } from 'type-graphql';
import { getCustomRepository } from 'typeorm';

import { PatientProfileInputType, PatientProfile } from './patientProfile.type';
import { PatientProfileRepository } from '../../model/repositories/PatientProfileRepository';
import { PatientProfileFilterInput } from './patientProfileFilter.type';

/* eslint-disable @typescript-eslint/indent, indent */
@Resolver()
export class PatientProfileResolver {

    @Mutation(() => PatientProfile)
    async createOrUpdatePatientProfile(
        @Arg('profile') profile: PatientProfileInputType
    ): Promise<PatientProfile> {
        return await getCustomRepository(PatientProfileRepository)
            .createOrUpdate(profile);
    }

    @Query(() => [PatientProfile])
    async patientProfiles(
        @Arg(
            'filter',
            () => PatientProfileFilterInput,
            {
                nullable: true
            }
        )
        filter: PatientProfileFilterInput,
        @Arg(
            'filterState',
            () => Boolean,
            {
                defaultValue: true,
                nullable: true
            }
        )
        filterState: boolean
    ): Promise<PatientProfile[]> {
        const repository = getCustomRepository(PatientProfileRepository);
        const patients = await repository.getWithScore();
        const filtered = repository.filterBy(filter, patients);

        return filterState
            ? filtered.filter((profile) => profile.newestFormState)
            : filtered;
    }

    @Query(() => PatientProfile)
    patientProfile(
        @Arg('id') id: number
    ): Promise<PatientProfile> {
        return getCustomRepository(PatientProfileRepository)
            .getById(id);
    }

}
/* eslint-enable */
