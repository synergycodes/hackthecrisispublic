import { Field } from 'type-graphql';

export class UserAppContext {
    @Field()
    deviceId: string;
}
