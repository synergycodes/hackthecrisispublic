import { Field, ObjectType } from 'type-graphql';
import { DeviceLocationTrace as BaseDeviceLocationTrace } from '@hackthecrisis/common';

import { Point } from '../point/point.type';

@ObjectType()
export class DeviceLocationTrace implements BaseDeviceLocationTrace {
    @Field()
    id: number;

    @Field(() => Point)
    location: Point;

    @Field()
    deviceId: string;

    @Field()
    timestamp: Date;
}
