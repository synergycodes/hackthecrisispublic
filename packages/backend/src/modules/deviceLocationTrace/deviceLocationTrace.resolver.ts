import { Arg, Mutation, Query, Resolver } from 'type-graphql';
import { getRepository } from 'typeorm';

import { DeviceLocationTrace as DeviceLocationTraceEntity } from '../../model/entities/DeviceLocationTrace';
import { DeviceLocationInput } from './deviceLocationInput.type';
import { DeviceLocationTrace } from './deviceLocationTrace.type';

@Resolver(() => DeviceLocationTrace)
export class DeviceLocationTraceResolver {

    @Mutation(() => DeviceLocationTrace)
    async addDeviceTrace(
        @Arg('data') input: DeviceLocationInput
    ): Promise<DeviceLocationTrace> {
        const entity = new DeviceLocationTraceEntity();
        entity.deviceId = input.deviceId;
        entity.timestamp = input.timestamp;
        entity.location = {
            type: 'Point',
            coordinates: [input.location.lon, input.location.lat]
        };
        await entity.save();

        const result = new DeviceLocationTrace();
        result.deviceId = input.deviceId;
        result.location = input.location;
        result.id = entity.id;
        result.timestamp = input.timestamp;

        return result;
    }

    @Query(() => DeviceLocationTrace)
    async test(@Arg('id') id: number): Promise<DeviceLocationTrace> {
        const repo = getRepository(DeviceLocationTraceEntity);

        const entity = await repo.findOne(id);

        const result = new DeviceLocationTrace();
        result.deviceId = entity.deviceId;
        result.location = {
            lon: entity.location.coordinates[0],
            lat: entity.location.coordinates[1]
        };
        result.id = entity.id;
        result.timestamp = entity.timestamp;

        return result;
    }
}
