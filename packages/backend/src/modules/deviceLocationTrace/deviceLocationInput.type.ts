import { Field, InputType } from 'type-graphql';
import {
    DeviceLocationTraceInput as BaseDeviceLocationTraceInput,
    Point as BasePoint
} from '@hackthecrisis/common';

@InputType()
export class InputPoint implements BasePoint {
    @Field()
    lat: number;

    @Field()
    lon: number;
}

@InputType()
export class DeviceLocationInput implements BaseDeviceLocationTraceInput {
    @Field(() => InputPoint)
    location: InputPoint;

    @Field()
    timestamp: Date;

    @Field()
    deviceId: string;
}
