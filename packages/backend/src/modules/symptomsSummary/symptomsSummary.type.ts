import { ObjectType, Field } from 'type-graphql';
import { SymptomsSummary as BaseSymptomsSummary } from '@hackthecrisis/common';

@ObjectType()
export class SymptomsSummary implements BaseSymptomsSummary {

    @Field({
        nullable: true
    })
    coughing?: boolean;

    @Field({
        nullable: true
    })
    dyspoena?: boolean;

    @Field({
        nullable: true
    })
    lastBodyTemperature?: number;

}
