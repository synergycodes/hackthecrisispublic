import { ObjectType, Field, ID, InputType } from 'type-graphql';
import { FormState as BaseFormState } from '@hackthecrisis/common';

import { FormAnswer, SubmitFormAnswerInputType, SubmitFormAnswer } from '../formAnswer/formAnswer.type';

@ObjectType()
export class FormState implements BaseFormState {

    @Field(() => ID)
    id: number;

    @Field(
        () => ID,
        {
            nullable: true
        }
    )
    userId: number;

    @Field(
        () => [FormAnswer],
        {
            nullable: true
        }
    )
    answers: FormAnswer[];

    @Field(() => Date)
    createDate: Date;

}

@InputType()
export class SubmitFormInputType {

    @Field(
        () => ID,
        {
            nullable: true
        }
    )
    userId: number;

    @Field(
        () => [SubmitFormAnswerInputType],
        {
            nullable: true
        }
    )
    answers: SubmitFormAnswerInputType[];

}

@ObjectType()
export class SubmitFormReturnType {

    @Field(() => ID)
    id: number;

    @Field(
        () => ID,
        {
            nullable: true
        }
    )
    userId: number;

    @Field(() => Date)
    createDate: Date;

    @Field({
        nullable: true
    })
    score: number;

    @Field({
        nullable: true
    })
    category: string;

}

@ObjectType()
export class FormStateWithAnswers extends SubmitFormReturnType {

    @Field(
        () => [SubmitFormAnswer],
        {
            nullable: true
        }
    )
    answers: SubmitFormAnswer[];

}
