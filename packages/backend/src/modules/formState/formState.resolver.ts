import { Resolver, Query, Arg, Mutation, Subscription, Root } from 'type-graphql';
import { getCustomRepository } from 'typeorm';

import { SubmitFormInputType, SubmitFormReturnType, FormStateWithAnswers } from './formState.type';
import { FormStateRepository } from '../../model/repositories/FormStateRepository';
import { formSubmitSubscriber, FORM_SUBMIT_TOPIC } from '../../subscriptions/formSubmit';

/* eslint-disable @typescript-eslint/indent */
@Resolver()
export class FormStateResolver {

    @Query(() => [FormStateWithAnswers])
    async formStatesWithAnswers() {
        return getCustomRepository(FormStateRepository)
            .getWithAnswers();
    }

    @Mutation(() => SubmitFormReturnType)
    async submitForm(
        @Arg('state') state: SubmitFormInputType
    ): Promise<SubmitFormReturnType> {
        const { userId, answers } = state;
        const result = await getCustomRepository(FormStateRepository)
            .submitFormStateWithAnswers(userId, answers);

        formSubmitSubscriber.onFormSubmit(result);

        return result;
    }

    @Mutation(() => Boolean)
    async setUser(
        @Arg('stateId') stateId: number,
        @Arg('userId') userId: number
    ): Promise<boolean> {
        const repo = getCustomRepository(FormStateRepository);
        await repo.update(stateId, {
            userId
        });
        return true;
    }

    @Subscription(
        () => SubmitFormReturnType,
        {
            topics: FORM_SUBMIT_TOPIC
        }
    )
    onFormSubmit(
        @Root() form: SubmitFormReturnType
    ) {
        return form;
    }

}
/* eslint-enable */
