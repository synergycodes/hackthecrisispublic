import { InputType, Field } from 'type-graphql';

@InputType()
export class DeviceConfigurationInputType {

    @Field()
    deviceId: string;

    @Field()
    pushToken: string;

}
