/* eslint-disable @typescript-eslint/indent */
import { Resolver, Mutation, Arg } from 'type-graphql';
import { getCustomRepository } from 'typeorm';

import { DeviceConfigurationInputType } from './deviceConfiguration.type';
import { DeviceNotificationConfigurationRepository } from '../../model/repositories/DeviceNotificationConfigurationRepository';

@Resolver()
export class DeviceConfigurationResolver {

    @Mutation(
        () => Boolean
    )
    setDevicePushToken (
        @Arg('conf') conf: DeviceConfigurationInputType
    ): Promise<boolean> {
        return getCustomRepository(DeviceNotificationConfigurationRepository)
            .setDeviceConfiguration(conf);
    }

}
