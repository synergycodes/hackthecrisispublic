import { Field, Float, ObjectType } from 'type-graphql';
import { Point as BasePoint } from '@hackthecrisis/common';

@ObjectType()
export class Point implements BasePoint {
    @Field(() => Float)
    lon: number;

    @Field(() => Float)
    lat: number;
}
