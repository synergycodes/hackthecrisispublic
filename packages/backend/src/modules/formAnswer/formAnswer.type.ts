import { ObjectType, Field, InputType } from 'type-graphql';
import { FormAnswer as BaseFormAnswer } from '@hackthecrisis/common';

import { FormQuestion } from '../formQuestion/formQuestion.type';

@ObjectType()
export class FormAnswer implements BaseFormAnswer {

    @Field(() => FormQuestion)
    question: FormQuestion;

    @Field()
    value: string;

}

@InputType()
export class SubmitFormAnswerInputType {

    @Field()
    questionId: number;

    @Field()
    value: string;

}

@ObjectType()
export class SubmitFormAnswer {

    @Field()
    questionId: number;

    @Field()
    value: string;

}
