import { createLogger, format, transports } from 'winston';

import { CONFIG } from '../config';

export const logger = createLogger({
    level: CONFIG.LOG_LEVEL,
    format: format.combine(
        format.colorize(),
        format.simple()
    ),
    defaultMeta: { service: '@hackthecrisis/apollo' },
    transports: [
        new transports.Console()
    ]
});
