import { SymptomsSummary } from '@hackthecrisis/common';

import { PatientProfile } from '../model/entities/PatientProfile';
import { FormAnswer } from '../model/entities/FormAnswer';

const TEMPERATURE_QUESTION_ID = 4;
const COUGHING_QUESTION_ID = 6;
const DYSPOENA_QUESTION_ID = 7;

export const getPatientSymptoms = (
    patient: PatientProfile
): SymptomsSummary => {
    const newestFormState = patient.newestFormState;
    if (!newestFormState) {
        return { };
    }

    const answers = newestFormState.answers;
    return {
        coughing: getCoughing(answers),
        dyspoena: getDyspoena(answers),
        lastBodyTemperature: getTemperature(answers)
    };
};

const findById = (answers: FormAnswer[], id: number) => answers.find(
    ({ questionId }) => questionId === id
);

const getCoughing = (answers: FormAnswer[]): boolean => {
    const answer = findById(answers, COUGHING_QUESTION_ID);

    return answer
        ? answer.value === 'true'
        : null;
};

const getDyspoena = (answers: FormAnswer[]): boolean => {
    const answer = findById(answers, DYSPOENA_QUESTION_ID);

    return answer
        ? answer.value === 'true'
        : null;
};

const getTemperature = (answers: FormAnswer[]): number => {
    const answer = findById(answers, TEMPERATURE_QUESTION_ID);

    return answer
        ? parseFloat(answer.value)
        : null;
};
