import { PatientCategory } from '@hackthecrisis/common';

export const mapScoreToCategory = (score: number): PatientCategory => {
    if (score >= 40) {
        return PatientCategory.C;
    }

    if (score >= 20) {
        return PatientCategory.B;
    }

    return PatientCategory.A;
};
