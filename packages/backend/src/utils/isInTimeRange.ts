import { RequestTimeRange } from '@hackthecrisis/common';

const timeRangeValueMap = {
    [RequestTimeRange.Today]: 86400000,
    [RequestTimeRange.ThisWeek]: 604800000,
    [RequestTimeRange.LastTwoWeeks]: 1209600000
};

export const isInTimeRange = (
    timeRange: RequestTimeRange,
    date: Date
) => {
    const then = date.getTime();
    const now = (new Date()).getTime();

    const timeDiff = now - then;
    const selected = timeRangeValueMap[timeRange];

    return timeDiff < selected;
};
