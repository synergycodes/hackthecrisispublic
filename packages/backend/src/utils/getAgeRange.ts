import { PatientAgeRange } from '@hackthecrisis/common';


const ageRanges = [
    {
        from: 1,
        to: 10,
        range: PatientAgeRange.From1To10
    },
    {
        from: 11,
        to: 20,
        range: PatientAgeRange.From11To20
    },
    {
        from: 21,
        to: 30,
        range: PatientAgeRange.From21To30
    },
    {
        from: 31,
        to: 40,
        range: PatientAgeRange.From31To40
    },
    {
        from: 41,
        to: 50,
        range: PatientAgeRange.From41To50
    },
    {
        from: 51,
        to: 60,
        range: PatientAgeRange.From51To60
    },
    {
        from: 61,
        to: 70,
        range: PatientAgeRange.From61To70
    },
    {
        from: 71,
        to: 80,
        range: PatientAgeRange.From71To80
    },
    {
        from: 81,
        to: 90,
        range: PatientAgeRange.From81To90
    },
    {
        from: 91,
        to: 100,
        range: PatientAgeRange.From91To100
    }
];

export const getAgeRange = (age: number): PatientAgeRange => {
    const range = ageRanges.find(
        ({ from, to }) => age >= from && age <= to
    );

    return range
        ? range.range
        : null;
};
