import * as _ from 'lodash/fp';

import { PatientProfile } from '../modules/patientProfile/patientProfile.type';

const AGE_QUESTION_ID = 3;

export const getPatientAge = (patientProfile: PatientProfile): number => {
    const formState = _.last(patientProfile.formStates.sort(({ id }) => id));
    const ageAnswer = formState.answers.find(
        (x) => x.questionId === AGE_QUESTION_ID
    );

    return ageAnswer
        ? parseInt(ageAnswer.value, 10)
        : 32;
};
