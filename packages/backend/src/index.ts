import './init';
import { buildSchema } from 'type-graphql';

import { startApolloServer } from './configuration/apollo.config';
import { connectDatabase } from './configuration/db.config';
import { resolvers } from './modules/resolvers';
import { _setDbConnection } from './context/db';
import { logger } from './utils/logger';
import { getPubSub } from './subscriptions/getPubSub';
import { registerFormSubmit } from './subscriptions/formSubmit';
import { registerInfectionCrossingSubscriber } from './subscriptions/infectionCrossing';
import { registerPatientStatusSubscriber } from './subscriptions/patientStatus';
import { registerPushNotificationSubscriber } from './subscriptions/pushNotification';

const pubSub = getPubSub([
    registerFormSubmit,
    registerInfectionCrossingSubscriber,
    registerPatientStatusSubscriber,
    registerPushNotificationSubscriber
]);

const start = async () => {
    try {
        const [ dbConnection, apolloResults ] = await Promise.all(
            [
                connectDatabase(),
                startApolloServer({
                    schema: await buildSchema({
                        resolvers,
                        dateScalarMode: 'isoDate',
                        validate: false,
                        pubSub
                    }),
                    subscriptions: {
                        path: '/subscriptions'
                    }
                })
            ]
        );

        _setDbConnection(dbConnection);
        logger.info(`Connected to the ${dbConnection.options.type} database`);

        const { url, subscriptionsUrl } = apolloResults;
        logger.info(`Server ready at ${url}`);
        logger.info(`Subscriptions server ready at ${subscriptionsUrl}`);
    } catch (error) {
        logger.error('There was a startup error', error);
    }
};

start();
