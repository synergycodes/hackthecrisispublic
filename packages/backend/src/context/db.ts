import { Connection } from 'typeorm';

export let dbConnection: Connection;

export const _setDbConnection = (conn: Connection) =>
    dbConnection = conn;
