import { PubSub } from 'apollo-server';

type RegisterSubscriptionSource = (pubSub: PubSub) => PubSub;

export const getPubSub = (sources: RegisterSubscriptionSource[]): PubSub => {
    const initialPubSub = new PubSub();

    return sources.reduce(
        (prevSub, currentSource) => currentSource(prevSub),
        initialPubSub
    );
};
