import { PubSub } from 'apollo-server';

import { PushNotificationSubscriber } from '../model/subscribers/PushNotificationSubscriber';

export const PUSH_NOTIFICATION_SUBSCRIBER = 'PUSH_NOTIFICATION_SUBSCRIBER';

export const registerPushNotificationSubscriber = (pubSub: PubSub) => {
    pushNotificationSubscriber.register(PUSH_NOTIFICATION_SUBSCRIBER, pubSub);
    return pubSub;
};

export const pushNotificationSubscriber = new PushNotificationSubscriber();
