import { PubSub } from 'apollo-server';

import { FormSubmitSubscriber } from '../model/subscribers/FormSubmitSubscriber';

export const FORM_SUBMIT_TOPIC = 'FORM_SUBMIT_TOPIC';

export const registerFormSubmit = (pubSub: PubSub) => {
    formSubmitSubscriber.register(FORM_SUBMIT_TOPIC, pubSub);
    return pubSub;
};

export const formSubmitSubscriber = new FormSubmitSubscriber();
