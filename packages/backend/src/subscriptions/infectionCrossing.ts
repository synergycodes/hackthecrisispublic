import { PubSub } from 'apollo-server';

import { InfectionCrossingSubscriber } from '../model/subscribers/InfectionCrossingSubscriber';

export const INFECTION_CROSSING_TOPIC = 'INFECTION_CROSSING_TOPIC';

export const registerInfectionCrossingSubscriber = (pubSub: PubSub) => {
    infectionCrossingSubscriber.register(INFECTION_CROSSING_TOPIC, pubSub);
    return pubSub;
};

export const infectionCrossingSubscriber = new InfectionCrossingSubscriber();
