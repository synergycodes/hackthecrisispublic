import { PubSub } from 'apollo-server';

import { PatientStatusSubscriber } from '../model/subscribers/PatientStatusSubscriber';

export const PATIENT_STATUS_CHANGE_TOPIC = 'PATIENT_STATUS_CHANGE_TOPIC';

export const registerPatientStatusSubscriber = (pubSub: PubSub) => {
    patientStatusSubscriber.register(PATIENT_STATUS_CHANGE_TOPIC, pubSub);
    return pubSub;
};

export const patientStatusSubscriber = new PatientStatusSubscriber();
