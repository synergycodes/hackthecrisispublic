import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity()
export class DeviceNotificationConfiguration {

    @Column()
    @PrimaryColumn()
    deviceId: string;

    @Column()
    pushToken: string;

}
