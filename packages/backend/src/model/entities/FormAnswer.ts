import { Column, ManyToOne, JoinColumn, Entity, PrimaryColumn } from 'typeorm';
import { FormAnswer as BaseFormAnswer } from '@hackthecrisis/common';

import { FormQuestion } from './FormQuestion';
import { FormState } from './FormState';
import { FormAnswerScore } from './FormAnswerScore';

@Entity()
export class FormAnswer implements BaseFormAnswer {

    @ManyToOne(
        () => FormQuestion,
        (question) => question.answers,
        {
            primary: true
        }
    )
    @JoinColumn({
        name: 'questionId'
    })
    question: FormQuestion;

    @Column()
    @PrimaryColumn()
    questionId: number;

    @ManyToOne(
        () => FormState,
        (formState) => formState.answers,
        {
            primary: true
        }
    )
    @JoinColumn({
        name: 'formStateId'
    })
    formState: FormState;

    @Column()
    @PrimaryColumn()
    formStateId: number;

    @Column()
    value: string;

    @ManyToOne(
        () => FormAnswerScore,
        (formAnswerScore) => formAnswerScore.answers,
        {
            nullable: true
        }
    )
    @JoinColumn([
        {
            name: 'questionId',
            referencedColumnName: 'questionId'
        },
        {
            name: 'value',
            referencedColumnName: 'answer'
        }
    ])
    score: FormAnswerScore;

}
