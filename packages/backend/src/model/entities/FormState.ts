import { Entity, PrimaryGeneratedColumn, OneToMany, Column, AfterLoad, ManyToOne, JoinColumn } from 'typeorm';

import { FormAnswer } from './FormAnswer';
import { PatientProfile } from './PatientProfile';

@Entity()
export class FormState {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        nullable: true
    })
    userId: number;

    @ManyToOne(
        () => PatientProfile,
        (patientProfile) => patientProfile.formStates,
        {
            nullable: true
        }
    )
    @JoinColumn({
        name: 'userId'
    })
    patientProfile: PatientProfile;

    @OneToMany(() => FormAnswer, (answer) => answer.formState)
    answers: FormAnswer[];

    @Column()
    createDate: Date;

    score: number;
    category: string;

    @AfterLoad()
    setTotalScore() {
        const answers = this.answers || [];
        const scores = answers.map((answer) => answer.score)
            .filter((x) => x);

        if (scores.length === 0) {
            this.score = null;
            return;
        }

        this.score = scores.reduce((a, b) => a + b.score, 0);
    }

}
