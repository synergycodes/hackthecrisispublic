import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { Point } from 'geojson';

@Entity()
export class DeviceLocationTrace extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    deviceId: string;

    @Column('geography')
    location: Point;

    @Column()
    timestamp: Date;

}
