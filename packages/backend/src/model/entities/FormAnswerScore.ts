import { Entity, ManyToOne, JoinColumn, Column, PrimaryColumn, OneToMany } from 'typeorm';

import { FormQuestion } from './FormQuestion';
import { FormAnswer } from './FormAnswer';

@Entity()
export class FormAnswerScore {

    @ManyToOne(
        () => FormQuestion,
        (question) => question.score,
        {
            primary: true
        }
    )
    @JoinColumn({
        name: 'questionId'
    })
    question: FormQuestion;

    @Column()
    @PrimaryColumn()
    questionId: number;

    @Column()
    @PrimaryColumn()
    answer: string;

    @Column()
    score: number;

    @OneToMany(() => FormAnswer, (answer) => answer.score)
    answers: FormAnswer[];

}
