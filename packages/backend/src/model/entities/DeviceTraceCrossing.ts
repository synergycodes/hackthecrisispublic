import { Entity, Column, PrimaryColumn } from 'typeorm';
import { DeviceTraceCrossing as BaseDeviceTraceCrossing } from '@hackthecrisis/common';
import { Point } from 'geojson';

@Entity()
export class DeviceTraceCrossing implements BaseDeviceTraceCrossing {

    @Column()
    @PrimaryColumn()
    deviceId: string;

    @Column()
    @PrimaryColumn()
    withDevice: string;

    @Column()
    distance: number;

    @Column('geography')
    location: Point;

    @Column()
    timestamp: Date;

}
