import { Entity, PrimaryGeneratedColumn, Column, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { FormQuestion as BaseFormQuestion, FormQuestionType } from '@hackthecrisis/common';

import { FormAnswer } from './FormAnswer';
import { FormAnswerScore } from './FormAnswerScore';

@Entity()
export class FormQuestion implements BaseFormQuestion {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    order: number;

    @Column()
    text: string;

    @Column({
        enum: FormQuestionType
    })
    type: FormQuestionType;

    @ManyToOne(() => FormQuestion, (parent) => parent.children)
    @JoinColumn({
        name: 'parentId'
    })
    parent: FormQuestion;

    @OneToMany(() => FormQuestion, (child) => child.parent)
    children: FormQuestion[];

    @OneToMany(() => FormAnswer, (child) => child.question)
    answers: FormAnswer[];

    @OneToMany(() => FormAnswerScore, (score) => score.question)
    score: FormAnswerScore;

}
