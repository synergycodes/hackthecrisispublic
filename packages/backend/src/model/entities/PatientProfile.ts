import * as _ from 'lodash/fp';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany, AfterLoad } from 'typeorm';
import { PatientProfile as BasePatientProfile, PatientStatus, PatientCategory, SymptomsSummary } from '@hackthecrisis/common';

import { FormState } from './FormState';
import { mapScoreToCategory } from '../../utils/mapScoreToCategory';
import { getPatientAge } from '../../utils/getPatientAge';
import { getPatientSymptoms } from '../../utils/getPatientSymptoms';

@Entity()
export class PatientProfile implements BasePatientProfile {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        nullable: true
    })
    name: string;

    @Column({
        nullable: true
    })
    surname: string;

    @Column({
        nullable: true
    })
    pesel: string;

    @Column({
        nullable: true
    })
    phone: string;

    @Column({
        nullable: true
    })
    email: string;

    @Column({
        nullable: true,
        default: false
    })
    isMainDeviceUser: boolean;

    @Column({
        enum: PatientStatus,
        default: PatientStatus.NewApplication
    })
    status: PatientStatus;

    @Column({
        nullable: true
    })
    deviceId: string;

    @OneToMany(
        () => FormState,
        (formState) => formState.patientProfile
    )
    formStates: FormState[];

    score: number = null;

    category?: PatientCategory = null;

    newestFormState: FormState;

    age: number;

    symptoms: SymptomsSummary;

    @AfterLoad()
    updateCalculatedFields() {
        if (!this.formStates || !this.formStates.length) {
            this.score = null;
            return;
        }

        this.newestFormState = _.last(this.formStates.sort((x) => x.id));
        this.score = this.newestFormState.score;
        this.category = mapScoreToCategory(this.score);
        this.age = getPatientAge(this);
        this.symptoms = getPatientSymptoms(this);
    }

}
