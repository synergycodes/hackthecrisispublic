import { EntityRepository, Repository } from 'typeorm';

import { DeviceTraceCrossing } from '../entities/DeviceTraceCrossing';

@EntityRepository(DeviceTraceCrossing)
export class DeviceTraceCrossingRepository
    extends Repository<DeviceTraceCrossing> {

    async saveNewCrossings(crossings: DeviceTraceCrossing[]) {
        const newCrossings = crossings.map(
            (crossing) => this.create(crossing)
        );
        await this.save(newCrossings);
        return newCrossings;
    }

    async getForDevice(deviceId: string) {
        if (deviceId) {
            return this.find({
                where: {
                    deviceId
                }
            });
        }

        return this.find();
    }

}
