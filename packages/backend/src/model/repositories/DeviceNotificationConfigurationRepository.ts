import { EntityRepository, Repository } from 'typeorm';

import { DeviceNotificationConfiguration } from '../entities/DeviceNotificationConfiguration';
import { DeviceConfigurationInputType } from '../../modules/deviceConfiguration/deviceConfiguration.type';

@EntityRepository(DeviceNotificationConfiguration)
export class DeviceNotificationConfigurationRepository
    extends Repository<DeviceNotificationConfiguration> {

    async setDeviceConfiguration(
        configuration: DeviceConfigurationInputType
    ): Promise<boolean> {
        const entity = this.create(configuration);
        await this.save(entity);

        return true;
    }

    async getPushTokenForDevice(deviceId: string) {
        return this.findOne({
            where: {
                deviceId
            }
        });
    }

}
