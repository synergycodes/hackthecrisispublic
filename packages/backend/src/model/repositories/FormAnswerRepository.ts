import { Repository, EntityRepository } from 'typeorm';

import { FormAnswer } from '../entities/FormAnswer';
import { SubmitFormAnswerInputType } from '../../modules/formAnswer/formAnswer.type';

const DESCRIPTION_QUESTION_ID = 8;

@EntityRepository(FormAnswer)
export class FormAnswerRepository extends Repository<FormAnswer> {

    async submitAnswers(
        formStateId: number,
        answers: SubmitFormAnswerInputType[]
    ): Promise<FormAnswer[]> {
        const entities = answers
            .filter(({ questionId }) => questionId !== DESCRIPTION_QUESTION_ID)
            .map(
                (answer) => this.create({
                    ...answer,
                    formStateId
                })
            );

        return this.save(entities);
    }

}
