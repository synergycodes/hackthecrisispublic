/* eslint-disable indent */
/* eslint-disable @typescript-eslint/indent */
/* eslint-disable max-len */
import { EntityRepository, Repository } from 'typeorm';

import { DeviceLocationTrace } from '../entities/DeviceLocationTrace';
import { DeviceTraceCrossing } from '../entities/DeviceTraceCrossing';

@EntityRepository(DeviceLocationTrace)
export class DeviceLocationTraceRepository
    extends Repository<DeviceLocationTrace> {

    async getDeviceTraceCrossings(
        deviceId: string
    ): Promise<DeviceTraceCrossing[]> {
        return this.manager.transaction(async (manager) => {
            await manager.queryRunner.query(`
            CREATE TEMPORARY TABLE sick_device_trace (
                "id" SERIAL PRIMARY KEY,
                "deviceId" character varying NOT NULL,
                "location" geography NOT NULL,
                "timestamp" TIMESTAMP NOT NULL
            );
            `);
            await manager.queryRunner.query(
                `
                INSERT INTO sick_device_trace
                    SELECT *
                    FROM public."device_location_trace" t
                    WHERE t."deviceId" = $1;
                `,
                [deviceId]
            );
            await manager.queryRunner.query(`
                CREATE TEMPORARY TABLE other_device_trace (
                    "id" SERIAL PRIMARY KEY,
                    "deviceId" character varying NOT NULL,
                    "location" geography NOT NULL,
                    "timestamp" TIMESTAMP NOT NULL
                );
            `);
            await manager.queryRunner.query(
                `
                INSERT INTO other_device_trace
                    SELECT *
                    FROM public."device_location_trace" t
                    WHERE t."deviceId" <> $1;
                `,
                [deviceId]
            );
            const result = await manager.queryRunner.query(`
                SELECT * 
                FROM (
                    SELECT
                        ST_Distance(product."sickLocation", product."otherLocation") AS distance,
                        product."otherDeviceId" AS "deviceId",
                        ST_AsText(product."otherLocation") AS "location",
                        product."timestamp" AS "timestamp"
                    FROM (
                        SELECT
                            sick."location" as "sickLocation",
                            other."location" as "otherLocation",
                            other."deviceId" as "otherDeviceId",
                            other."timestamp" as "timestamp",
                            ABS((EXTRACT(EPOCH FROM sick."timestamp") - EXTRACT(EPOCH FROM other."timestamp")) / 3600) AS "timeDiff"
                        FROM sick_device_trace sick,
                            other_device_trace other
                    ) product
                    WHERE product."timeDiff" <= 0.25
                ) result
                WHERE result."distance" <= 200
                GROUP BY
                    result."distance",
                    result."deviceId",
                    result."location",
                    result."timestamp"
                ;
            `);
            await manager.queryRunner.query(`
                DROP TABLE sick_device_trace;
            `);
            await manager.queryRunner.query(`
                DROP TABLE other_device_trace;
            `);

            return result.map(
                ({
                    distance,
                    deviceId: crossDeviceID,
                    location,
                    timestamp,
                }) => {
                    const [lat, lon] = (location as string).toUpperCase()
                        .split('POINT(')
                        .join('')
                        .split(')')
                        .join('')
                        .split(' ')
                        .map((x) => parseFloat(x));

                    return {
                        distance,
                        deviceId: crossDeviceID,
                        location: {
                            type: 'Point',
                            coordinates: [lon, lat]
                        },
                        timestamp,
                        withDevice: deviceId
                    } as DeviceTraceCrossing;
                }
            );
        });
    }

}
