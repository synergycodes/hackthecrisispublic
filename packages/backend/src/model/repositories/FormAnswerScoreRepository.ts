import { Repository, EntityRepository, In } from 'typeorm';

import { FormAnswerScore } from '../entities/FormAnswerScore';
import { SubmitFormAnswerInputType } from '../../modules/formAnswer/formAnswer.type';

@EntityRepository(FormAnswerScore)
export class FormAnswerScoreRepository extends Repository<FormAnswerScore> {

    async getTotalScoreForAnswers(
        answers: SubmitFormAnswerInputType[]
    ): Promise<number> {
        const questionIds = answers.map(({ questionId }) => questionId);

        const allScores = await this.find({
            where: {
                questionId: In(questionIds)
            }
        });

        const filteredScores = answers.map(
            (answer) => {
                const score = allScores.find(
                    (score) => score.questionId === answer.questionId
                        && score.answer === answer.value
                );

                return score;
            }
        );

        return filteredScores.filter((x) => x)
            .map(({ score }) => score)
            .reduce((a, b) => a + b, 0);
    }

}
