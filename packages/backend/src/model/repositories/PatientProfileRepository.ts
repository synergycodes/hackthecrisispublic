import * as _ from 'lodash/fp';
import { EntityRepository, Repository } from 'typeorm';
import { PatientCategory, PatientStatus, PatientAgeRange, RequestTimeRange } from '@hackthecrisis/common';

import { PatientProfile } from '../entities/PatientProfile';
import { PatientProfileInputType } from '../../modules/patientProfile/patientProfile.type';
import { PatientProfileFilterInput } from '../../modules/patientProfile/patientProfileFilter.type';
import { getAgeRange } from '../../utils/getAgeRange';
import { PatientsSummary } from '../../modules/patientsSummary/patientsSummary.type';
import { patientStatusSubscriber } from '../../subscriptions/patientStatus';
import { isInTimeRange } from '../../utils/isInTimeRange';

@EntityRepository(PatientProfile)
export class PatientProfileRepository extends Repository<PatientProfile>{

    async getSummary(): Promise<PatientsSummary> {
        const patients = await this.getWithScore();

        return {
            newApplications: patients.filter(
                ({ status }) => status === PatientStatus.NewApplication
            ).length,
            infected: patients.filter(
                ({ status }) => status === PatientStatus.Infected
            ).length,
            quarantine: patients.filter(
                ({ status }) => status === PatientStatus.InQuarantine
            ).length,
            recovered: patients.filter(
                ({ status }) => status === PatientStatus.Healthy
            ).length
        };
    }

    async getById(id: number): Promise<PatientProfile> {
        return this.findOne({
            relations: [
                'formStates',
                'formStates.answers',
                'formStates.answers.score'
            ],
            where: {
                id
            }
        });
    }

    async getWithScore(): Promise<PatientProfile[]> {
        return this.find({
            relations: [
                'formStates',
                'formStates.answers',
                'formStates.answers.score'
            ]
        });
    }

    filterBy(
        filter: PatientProfileFilterInput,
        patientProfiles: PatientProfile[]
    ): PatientProfile[] {
        if (!filter) {
            return patientProfiles;
        }

        return _.flowRight(
            this.filterByRequestTime(filter.timeRange),
            this.filterByAgeRange(filter.ageRanges),
            this.filterByCategory(filter.categories),
            this.filterByStatus(filter.statuses)
        )(patientProfiles);
    }

    async createOrUpdate(
        profile: PatientProfileInputType
    ): Promise<PatientProfile> {
        const { id } = profile;

        if (id) {
            const existing = await this.findOne({
                where: {
                    id
                }
            });

            const changed = _.pickBy(_.identity)(profile);
            const updated = _.merge(
                _.merge(
                    { },
                    existing,
                ),
                changed
            );

            if (profile.status && existing.status !== profile.status) {
                patientStatusSubscriber.onPatientStatusChange({
                    patientProfileId: id,
                    status: updated.status,
                    deviceId: updated.deviceId
                });
            }

            this.update(
                { id },
                _.omit(['score', 'category'], updated)
            );

            return updated as PatientProfile;
        }

        const {
            name, surname, pesel, phone, email, isMainDeviceUser, deviceId
        } = profile;
        const patientProfile = this.create({
            name, surname, pesel, phone, email,
            isMainDeviceUser, deviceId
        });

        return this.save(patientProfile);
    }

    private filterByCategory = _.curry(
        (categories: PatientCategory[], patientProfiles: PatientProfile[]) => {
            if (!categories || !categories.length) {
                return patientProfiles;
            }

            return patientProfiles.filter(
                ({ category }) => categories.includes(category)
            );
        }
    );

    private filterByStatus = _.curry(
        (statuses: PatientStatus[], patientProfiles: PatientProfile[]) => {
            if (!statuses || !statuses.length) {
                return patientProfiles;
            }

            return patientProfiles.filter(
                ({ status }) => statuses.includes(status)
            );
        }
    );

    private filterByAgeRange = _.curry(
        (ageRanges: PatientAgeRange[], patientProfiles: PatientProfile[]) => {
            if (!ageRanges || !ageRanges.length) {
                return patientProfiles;
            }

            return patientProfiles.filter(
                (profile) => ageRanges.includes(getAgeRange(profile.age))
            );
        }
    )

    private filterByRequestTime = _.curry(
        (timeRange: RequestTimeRange, patientProfiles: PatientProfile[]) => {
            if (!timeRange) {
                return patientProfiles;
            }

            return patientProfiles.filter((profile) => {
                if (!profile.newestFormState) {
                    return false;
                }

                return isInTimeRange(
                    timeRange,
                    profile.newestFormState.createDate
                );
            });
        }
    )

}
