import { Repository, EntityRepository, getCustomRepository } from 'typeorm';

import { FormState } from '../entities/FormState';
import { FormAnswerRepository } from './FormAnswerRepository';
import { SubmitFormAnswerInputType } from '../../modules/formAnswer/formAnswer.type';
import { FormAnswerScoreRepository } from './FormAnswerScoreRepository';
import { SubmitFormReturnType } from '../../modules/formState/formState.type';
import { mapScoreToCategory } from '../../utils/mapScoreToCategory';

@EntityRepository(FormState)
export class FormStateRepository extends Repository<FormState> {

    async submitFormStateWithAnswers(
        userId: number,
        answers: SubmitFormAnswerInputType[]
    ): Promise<SubmitFormReturnType> {
        const formState = this.create({
            userId,
            createDate: new Date()
        });
        const saved = await this.save(formState);

        getCustomRepository(FormAnswerRepository)
            .submitAnswers(saved.id, answers);

        const score = await getCustomRepository(FormAnswerScoreRepository)
            .getTotalScoreForAnswers(answers);
        saved.score = score;
        saved.category = mapScoreToCategory(score);

        return saved;
    }

    async getWithAnswers() {
        return this.find({
            relations: ['answers', 'answers.score']
        });
    }

}
