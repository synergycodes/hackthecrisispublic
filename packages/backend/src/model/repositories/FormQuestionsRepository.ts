import { EntityRepository, Repository, IsNull } from 'typeorm';

import { FormQuestion } from '../entities/FormQuestion';

@EntityRepository(FormQuestion)
export class FormQuestionsRepository extends Repository<FormQuestion> {

    getWithChildren() {
        return this.find({
            where: {
                parent: IsNull()
            },
            relations: ['children'],
            order: {
                order: 'ASC'
            }
        });
    }

}
