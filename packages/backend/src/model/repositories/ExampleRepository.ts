import { EntityRepository, Repository } from 'typeorm';

import { Example } from '../entities/Example';

@EntityRepository(Example)
export class ExampleRepository extends Repository<Example> {

}
