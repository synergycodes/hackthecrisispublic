import { PubSub } from 'apollo-server';
import { getCustomRepository } from 'typeorm';
import { PatientStatus } from '@hackthecrisis/common';

import { PATIENT_STATUS_CHANGE_TOPIC } from '../../subscriptions/patientStatus';
import { DeviceLocationTraceRepository } from '../repositories/DeviceLocationTraceRepository';
import { BaseSubscriber } from './BaseSubscriber';
import { PatientStatusChange } from './PatientStatusSubscriber';
import { DeviceTraceCrossingRepository } from '../repositories/DeviceTraceCrossingRepository';

export class InfectionCrossingSubscriber extends BaseSubscriber {

    async onInfectionFound(payload: PatientStatusChange) {
        const { status, deviceId } = payload;
        if (status !== PatientStatus.Infected || !deviceId) {
            return;
        }

        const traceCrossings = await getCustomRepository(
            DeviceLocationTraceRepository
        )
            .getDeviceTraceCrossings(payload.deviceId);
        const newCrossings = await getCustomRepository(
            DeviceTraceCrossingRepository
        )
            .saveNewCrossings(traceCrossings);

        newCrossings.forEach(
            (newCrossing) => {
                this.publish(newCrossing);
            }
        );
    }

    register(topic: string, pubSub: PubSub) {
        super.register(topic, pubSub);
        pubSub.subscribe(
            PATIENT_STATUS_CHANGE_TOPIC,
            this.onInfectionFound.bind(this)
        );
    }
}
