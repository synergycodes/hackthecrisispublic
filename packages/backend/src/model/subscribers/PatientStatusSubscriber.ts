import { PatientStatus } from '@hackthecrisis/common';

import { BaseSubscriber } from './BaseSubscriber';

export type PatientStatusChange = {
    patientProfileId: number;
    deviceId?: string;
    status: PatientStatus;
}

export class PatientStatusSubscriber extends BaseSubscriber {

    onPatientStatusChange = (patientStatusChange: PatientStatusChange) => {
        this.publish(patientStatusChange);
    }

}
