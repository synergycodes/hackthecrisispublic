import { PubSub } from 'apollo-server';
import { Expo } from 'expo-server-sdk';
import { getCustomRepository } from 'typeorm';

import { BaseSubscriber } from './BaseSubscriber';
import { DeviceTraceCrossing } from '../entities/DeviceTraceCrossing';
import { INFECTION_CROSSING_TOPIC } from '../../subscriptions/infectionCrossing';
import { DeviceNotificationConfigurationRepository } from '../repositories/DeviceNotificationConfigurationRepository';


export class PushNotificationSubscriber extends BaseSubscriber {

    private expo: Expo = new Expo();

    async onDeviceInfected(crossing: DeviceTraceCrossing) {
        const pushToken = await this.getPushToken(crossing);

        if (!pushToken) {
            return;
        }

        this.expo
            .chunkPushNotifications([
                {
                    to: pushToken,
                    sound: 'default',
                    body: 'You\'ve been close to an infected person',
                    data: { ...crossing }
                }
            ])
            .forEach(async (chunk) => {
                await this.expo.sendPushNotificationsAsync(chunk);
            });

    }

    register(topic: string, pubSub: PubSub) {
        super.register(topic, pubSub);
        pubSub.subscribe(
            INFECTION_CROSSING_TOPIC,
            this.onDeviceInfected.bind(this)
        );
    };

    private async getPushToken(crossing: DeviceTraceCrossing) {
        const configuration = await getCustomRepository(
            DeviceNotificationConfigurationRepository
        ).getPushTokenForDevice(crossing.deviceId);

        if (!configuration) {
            return null;
        }

        return Expo.isExpoPushToken(configuration.pushToken)
            ? configuration.pushToken
            : null;
    }

}
