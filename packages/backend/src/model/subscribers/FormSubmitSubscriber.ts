import { BaseSubscriber } from './BaseSubscriber';
import { SubmitFormReturnType } from '../../modules/formState/formState.type';

export class FormSubmitSubscriber extends BaseSubscriber{

    onFormSubmit = (formState: SubmitFormReturnType) => {
        this.publish(formState);
    }

}
