import { PubSub } from 'apollo-server';

export abstract class BaseSubscriber {

    protected topic: string;
    protected pubSub: PubSub;

    register(topic: string, pubSub: PubSub) {
        this.topic = topic;
        this.pubSub = pubSub;
    };

    protected publish = (payload: any) => {
        if (!this.pubSub) {
            return;
        }

        this.pubSub.publish(this.topic, payload);
    }

}
