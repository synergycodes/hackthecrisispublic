import { ApolloServer } from 'apollo-server';

import { CONFIG } from '../config';

type MinServerConfiguration = {
    port: number;
}

const getApolloConfiguration = (configuration = {}): MinServerConfiguration =>
    ({
        port: CONFIG.APOLLO_PORT,
        ...configuration
    });

export const startApolloServer = (apolloConfiguration = {}) => {
    const { port, ...configuration } = getApolloConfiguration(
        apolloConfiguration
    );

    return new ApolloServer(configuration)
        .listen(port);
};
