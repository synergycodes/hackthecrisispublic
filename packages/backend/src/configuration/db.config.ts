import { createConnection } from 'typeorm';

import config from '../../ormconfig';

export const connectDatabase = () => createConnection(config);
