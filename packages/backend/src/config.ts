const {
    DB_TYPE,
    DB_HOST,
    DB_PORT,
    DB_USERNAME,
    DB_PASSWORD,
    DB_NAME,
    APOLLO_PORT,
    LOG_LEVEL
} = process.env;

export const CONFIG = {
    DB_TYPE,
    DB_HOST,
    DB_PORT: +DB_PORT,
    DB_USERNAME,
    DB_PASSWORD,
    DB_NAME,
    APOLLO_PORT: Number.parseInt(APOLLO_PORT),
    LOG_LEVEL
};
