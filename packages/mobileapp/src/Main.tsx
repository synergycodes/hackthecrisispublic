import React, { useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import Constants from 'expo-constants';
import { useDispatch } from 'react-redux';

import { RootNavigator } from './Navigators/RootNavigator/RootNavigator';
import { addDeviceId } from './store/authentication/actions';
import { StartScreen } from './Screens/StartScreen';
import { startPositionListener } from './Services/PositionWatcher';
import { sendDeviceInfo } from './utils/sendDeviceInfo';
import { loadFonts } from './utils/loadFonts';

export const Main: React.FC = () => {

    const deviceId = Constants.installationId;
    const dispatch = useDispatch();
    dispatch(addDeviceId(deviceId));

    const [startScreenShown, setStartScreenShown] = React.useState(true);
    const [fontLoaded, setFontLoaded] = React.useState(false);
    const closeStartScreen = () => setStartScreenShown(false);

    useEffect(() => {
        loadFonts(setFontLoaded);
        startPositionListener();
    }, []);

    useEffect(() => {
        sendDeviceInfo(deviceId);
    });

    if (!fontLoaded){
        return null;
    }

    return (
        <NavigationContainer>
            {startScreenShown
                ? <StartScreen onPress={closeStartScreen}/>
                : <RootNavigator/>}
        </NavigationContainer>
    );
};
