export const CONFIG = {
    APOLLO_URL: process.env.APOLLO_URL || 'http://localhost:4000/graphql',
};
