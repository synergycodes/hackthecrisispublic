import styled from 'styled-components/native';
import React from 'react';
import { Dimensions } from 'react-native';
import { AntDesign } from '@expo/vector-icons';

import { Background } from '../../assets/StartScreen/Background';
import { Heart } from '../../assets/StartScreen/Heart';

const { width, height } = Dimensions.get('window');

const MainView = styled.View`
    flex: 1;
    background-color: #F1F9FF;
`;

const TextView = styled.View`
    margin-left: ${width/15}px;
    margin-right: ${width/15}px;
    top: ${-height/1.85}px;
    align-items: flex-end;
    align-self: flex-start;
`;

const LabelTop = styled.Text`
    color: ${({ theme }) => theme.colors.white};
    font-size: ${width/8}px;
    font-family: NunitoBold;
`;

const LabelBottom = styled.Text`
    color: ${({ theme }) => theme.colors.white};
    font-size: ${width/13}px;
    font-family: NunitoBold;
    margin-top: -15px;
`;

const DescriptionView = styled.View`
    top: ${-height/2.5}px;
    margin-left: ${width/15}px;
    margin-right: ${width/15}px;
`;

const Description = styled.Text`
    color: ${({ theme }) => theme.colors.white};
    font-size: ${width/28}px;
    text-align: justify;
    font-family: NunitoRegular;
`;

const Button = styled.TouchableOpacity`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    position: absolute;
    bottom: 0;
    right: 0;
    width: ${width/2.2}px;
    height: ${height/12}px;
    background-color: ${({ theme }) => theme.colors.salmon};
    border-top-left-radius: ${width/7}px;
`;

const ButtonText = styled.Text`
    font-size: ${width/18}px;
    color: white;
    font-family: NunitoBold;
    margin-right: ${width/25}px;
    margin-top: auto;
    margin-bottom: auto;
    margin-left: ${width/20}px;
`;

const HeartView = styled.View`
    position: absolute;
    right: ${width/25}px;
    top: ${height/2.6}px;
`;

const ButtonArrow = styled(AntDesign)`
    margin-top: 3px;
`;

const ratioFix = width/height >= 0.58 ? height * 0.08 : 0;

const BackgroundView = styled.View`
    z-index: -99999;
    align-items: center;
    justify-content: center;
    width: ${width}px;
    height: ${height}px;
`;

type Props = {
    onPress: () => void;
};

const description = 'The EpidemicApp is created to help people'
    +' discover the potential and actual infection risk in their areas.';

export const StartScreen: React.FC<Props> = ({ onPress }) => (
    <MainView>
        <BackgroundView>
            <Background
                width={width+ratioFix+100}
                height={height+ratioFix}
            />
        </BackgroundView>
        <HeartView>
            <Heart width={width/3.1} height={width/3.1}/>
        </ HeartView>
        <TextView>
            <LabelTop>Epidemic</LabelTop>
            <LabelBottom>App</LabelBottom>
        </TextView>
        <DescriptionView>
            <Description>{description}</Description>
        </DescriptionView>
        <Button onPress={onPress}>
            <ButtonText>Start</ButtonText>
            <ButtonArrow name='arrowright' color='white' size={width/18}/>
        </Button>
    </MainView>
);
