import React from 'react';
import styled from 'styled-components/native';

import { homeScreenBoxShadow } from '../../../utils/shadow';

const Box = styled.View`
    height: 59px;
    width: 69px;
    border-radius: 15px;
    display: flex;
    justify-content: center;
    align-items: center;
    ${homeScreenBoxShadow()}
`;

type Props = {
    color: string;
    icon;
}

export const IconBox: React.FC<Props> = ({ color, icon }) => (
    <Box style={{ backgroundColor: color }}>
        {icon}
    </Box>
);
