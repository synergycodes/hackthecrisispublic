import React, { useEffect, useState } from 'react';
import styled from 'styled-components/native';

import { fetchNewsList, News } from '../../../Services/NewsList';
import { HeaderText } from '../ActionBox/ActionBox';
import { NewsBox } from './NewsBox/NewsBox';

const NewsListBox = styled.View``;

const NewsTitle = styled(HeaderText)`
    margin-left: 20px;
    margin-bottom: 10px;
    font-family: NunitoBold;
`;

export const NewsList = () => {

    const [newsList, setNewsList] = useState<News[]>([]);

    useEffect(() => {
        fetchNewsList().then(setNewsList);
    }, []);


    return (
        <NewsListBox>
            <NewsTitle>News</NewsTitle>
            {newsList.map((n) => <NewsBox key={n.id} {...n}/>)}
        </NewsListBox>
    );
};
