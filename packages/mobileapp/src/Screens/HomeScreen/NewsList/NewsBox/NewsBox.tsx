import React from 'react';
import styled from 'styled-components/native';

import { Box } from '../../ActionBox/ActionBox';
import { News } from '../../../../Services/NewsList';

const NewsWrapper = styled(Box)`
    margin-bottom: 30px;
`;

const DateText = styled.Text`
    font-size: 10px;
    font-weight: 300;
    opacity: .6;
    margin-bottom: 5px;
`;

const NewsTitle = styled.Text`
    font-size: 14px;
    font-weight: bold;
    margin-bottom: 5px
`;

const NewsText = styled.Text`
    font-size: 12px;
    text-align: justify;
`;


export const NewsBox = ({ date, text, title }: News) => (
    <NewsWrapper>
        <DateText>{date}</DateText>
        <NewsTitle>{title}</NewsTitle>
        <NewsText>{text}</NewsText>
    </NewsWrapper>
);
