import React from 'react';
import styled from 'styled-components/native';

import { homeScreenBoxShadow } from '../../../utils/shadow';

export const Box = styled.TouchableOpacity`
    display: flex;
    justify-content: center;
    height: auto;
    background: white;
    padding: 20px;
    border-radius: 15px;
    margin-bottom: 55px;
    position: relative;
    ${homeScreenBoxShadow()}
`;

export const HeaderText = styled.Text`
    color: ${({ theme }) => theme.colors.calmBlue};
    font-size: 18px;
    font-family: NunitoBold;
`;

const SubText = styled.Text`
    color: ${({ theme }) => theme.colors.dark};
    font-family: NunitoRegular;
    font-size: 12px;
    margin-top: 10px;
    margin-bottom: 10px;
`;

type Props = {
    headerText: string;
    subText?: string;
    icon?;
    onPress: () => void;
}
export const ActionBox: React.FC<Props> = ({
    headerText,
    subText,
    icon,
    onPress
}) => (
    <Box onPress={onPress}>
        <HeaderText>{headerText}</HeaderText>
        {subText && <SubText>{subText}</SubText>}
        {icon}
    </Box>
);
