import React from 'react';
import styled from 'styled-components/native';

import { IconBox } from '../IconBox/IconBox';
import { StarIcon } from '../../../../assets/StartScreenIcons/StarIcon';
import { ActionBox } from '../ActionBox/ActionBox';


const IconWrapper = styled.View`
    display: flex;
    align-self: flex-end;
    position: absolute;
    top: -20px;
`;

const iconWrapper = (
    <IconWrapper>
        <IconBox
            icon={<StarIcon/>}
            color={'#73C2CF'}/>
    </IconWrapper>
);

export const Recommendations = () => (
    <ActionBox
        headerText={'Recommendations'}
        icon={iconWrapper}
    />
);
