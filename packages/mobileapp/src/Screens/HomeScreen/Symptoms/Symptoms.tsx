import React from 'react';
import styled from 'styled-components/native';
import { AntDesign } from '@expo/vector-icons';

import { theme } from '../../../providers/styles';
import { ActionBox } from '../ActionBox/ActionBox';

const ArrowIconBox = styled.View`
    display: flex;
    align-self: flex-end;
    position: absolute;
    right: 35px;
    color: ${({ theme }) => theme.colors.calmBlue};
`;

type Props = {
    navigateToInterview: () => void;
};

export const Symptoms: React.FC<Props> = ({
    navigateToInterview
}) => (
    <ActionBox
        headerText={'Check symptoms'}
        subText={'Make sure you provide actual data'}
        icon={<ArrowIconBox>
            <AntDesign
                name='arrowright'
                color={theme.colors.calmBlue}
                size={24}/>
        </ArrowIconBox>}
        onPress={navigateToInterview}
    />
);
