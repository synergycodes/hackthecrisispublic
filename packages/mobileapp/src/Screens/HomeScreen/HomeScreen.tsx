import styled, { useTheme } from 'styled-components/native';
import React from 'react';
import { Dimensions, StatusBar, } from 'react-native';

import { HomeNavProps } from '../../Navigators/HomeNavigator';
import { ScreenContainer } from '../shared/ScreenContainer';
import { HomeRoute } from '../../Navigators/types/HomeRoutes';
import { ActionsBoxesRow } from './ActionsBoxesRow/ActionsBoxesRow';
import { NewsList } from './NewsList/NewsList';
import { Recommendations } from './Recommendations/rsc';
import { Symptoms } from './Symptoms/Symptoms';
import { BackgroundHomeScreen } from '../../../assets/HomeScreen/Background';

const { width, height } = Dimensions.get('window');
const MainView = styled.View`
    padding-left: 10%;
    padding-right: 10%;
    padding-top: 20px;
    background-color: ${(props) => props.theme.colors.backgroundColor};
    margin-bottom: 65px;
`;

const HeaderBox = styled.View`
    display: flex;
    flex-direction: column;
    align-self: flex-start;
    margin-bottom: 40px;
`;

const WelcomeText = styled.Text`
    font-size: 40px;
    font-family: NunitoBold;
    color: ${({ theme }) => theme.colors.calmBlue};
`;

const WelcomeSubText = styled.Text`
    color: ${({ theme }) => theme.colors.calmBlue};
    font-family: NunitoRegular;
    font-size: 14px;
`;
const Background = styled(BackgroundHomeScreen)`
    width: ${width}px;
    position: absolute;
`;
const Wrapper = styled.ScrollView`
    width: ${width}px;
    height: ${height}px;
`;

export const HomeScreen: React.FC<HomeNavProps<HomeRoute.Home>> = (
    {
        navigation
    }
) => {
    const { colors } = useTheme();

    return (<ScreenContainer>
        <Wrapper>
            <Background/>
            <StatusBar
                backgroundColor={colors.backgroundColor}
                barStyle="dark-content"
            />
            <MainView>
                <HeaderBox>
                    <WelcomeText>Hello,</WelcomeText>
                    <WelcomeSubText>Start your medical
                            review,</WelcomeSubText>
                </HeaderBox>
                <Symptoms
                    navigateToInterview={() =>
                        navigation.navigate(HomeRoute.Interview)
                    }
                />
                <Recommendations/>
                <ActionsBoxesRow
                    navigateToMap={() => navigation.navigate(HomeRoute.Map)}/>
                <NewsList/>
            </MainView>
        </Wrapper>

    </ScreenContainer>
    );
};
