import React from 'react';
import styled from 'styled-components/native';

import { Box, HeaderText } from '../ActionBox/ActionBox';
import { IconBox } from '../IconBox/IconBox';
import { PieChartIcon } from '../../../../assets/StartScreenIcons/PieChartIcon';
import { HeartIcon } from '../../../../assets/StartScreenIcons/HeartIcon';
import { theme } from '../../../providers/styles';

const Row = styled.View`
    display: flex;
    flex-direction: row;
    width: 100%;
    justify-content: space-between
`;

const BoxWrapper = styled(Box)`
    width: 130px;
    height: 100px;
`;

const IconWrapper = styled.View`
    position: absolute;
    top: -20px;
    align-self: center;
`;

const BottomText = styled(HeaderText)`
    font-size: 14px;
    margin-top: 30px;
    align-self: center;
    font-family: NunitoRegular;
`;

export const ActionsBoxesRow = ({ navigateToMap }) => (
    <Row>
        <BoxWrapper onPress={navigateToMap}>
            <IconWrapper>
                <IconBox icon={<HeartIcon/>} color={theme.colors.salmon}/>
            </IconWrapper>
            <BottomText>Med facilities</BottomText>
        </BoxWrapper>
        <BoxWrapper>
            <IconWrapper>
                <IconBox icon={<PieChartIcon/>} color={theme.colors.darkBlue}/>
            </IconWrapper>
            <BottomText>Statistics</BottomText>
        </BoxWrapper>
    </Row>
);
