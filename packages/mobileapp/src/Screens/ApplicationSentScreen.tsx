/* eslint-disable react-native/no-single-element-style-arrays */
import styled, { useTheme } from 'styled-components/native';
import React from 'react';
import { Dimensions, GestureResponderEvent } from 'react-native';
import Animated, { Easing } from 'react-native-reanimated';

import { ScreenTemplate } from './shared/ScreenTemplate';
import { Virus } from '../../assets/ApplicationSentScreen/Virus';
import { Shield } from '../../assets/ApplicationSentScreen/Shield';
import { Circles } from '../../assets/ApplicationSentScreen/Circles';
import { HomeNavProps } from '../Navigators/HomeNavigator';
import { HomeRoute } from '../Navigators/types/HomeRoutes';

const { width, height } = Dimensions.get('window');

const CriteriaText = styled.Text`
    color: white;
    font-family: NunitoBold;
    font-size: ${width/14}px;
    margin-top: ${height/7}px;
`;

const InfoText = styled.Text`
    color: ${(props) => props.theme.colors.darkBlue};
    font-family: NunitoRegular;
    font-size: ${width*0.048}px;
    text-align: center;
    margin-top: ${height*0.06}px;
`;

const CirclesStyled = styled(Animated.View)`
    position: absolute;
    top: -${height/20}px;
`;

export enum Criterium {
    A = 'A',
    B = 'B',
    C = 'C',
    NO = 'NO'
}

const TopScreen = styled(Animated.View)`
    top: ${height/9}px;
    justify-content: center;
    align-items: center;
`;
const { set } = Animated;

type Props = {
    buttonOnPress: (event: GestureResponderEvent) => void,
}

export const ApplicationSentScreen:
React.FC<HomeNavProps<HomeRoute.ApplicationSent>> = ({
    navigation
}) => {

    const theme = useTheme();
    const [Image, setImage] = React.useState(() => Virus);
    const animationValueScale = React.useRef(new Animated.Value(1));
    const animationValueRotation = React.useRef(new Animated.Value(-0.5));

    const hideVirusConfig = {
        duration: 1000,
        toValue: 0,
        easing: Easing.inOut(Easing.ease),
    };

    const showShieldConfig = {
        duration: 1000,
        toValue: 1,
        easing: Easing.inOut(Easing.ease),
    };

    const rotateShieldConfig = {
        duration: 1000,
        toValue: 0,
        easing: Easing.inOut(Easing.ease),
    };

    const hideVirus
        = Animated.timing(animationValueScale.current, hideVirusConfig);
    const showShield
        = Animated.timing(animationValueScale.current, showShieldConfig);
    const rotateShield
        = Animated.timing(animationValueRotation.current, rotateShieldConfig);

    React.useEffect(() => {
        hideVirus.start((result) => {
            if (result.finished) {
                setImage(() => Shield);
                set(animationValueScale.current, 0);
                set(animationValueRotation.current, -0.5);
                showShield.start();
                rotateShield.start();
            }
        });
    }, []);

    const topScreen = (
        <>
            {Image === Shield && <CirclesStyled style={[{
                transform: [{
                    scale: animationValueScale.current,
                }]
            }]}>
                <Circles width={550} height={550} />
            </CirclesStyled>}
            <TopScreen style={[{
                transform: [{
                    scale: animationValueScale.current,
                    rotateZ: animationValueRotation.current
                }]
            }]}>
                <Image width={200} height={200}/>
            </TopScreen>
            <CriteriaText>Thank you</CriteriaText>
        </>
    );

    const description
        = 'Your application has been sent \nto the SANEPID facility.\n\n'
        + 'You will be notified \nabout further actions.';

    const descriptionBox = (
        <InfoText>
            {description}
        </InfoText>
    );


    return (
        <ScreenTemplate
            descriptionBox={descriptionBox}
            topScreen={topScreen}
            buttonLabel='OK'
            buttonColor={theme.colors.salmon}
            buttonOnPress={() => navigation.navigate(HomeRoute.Home)}
        />
    );
};
