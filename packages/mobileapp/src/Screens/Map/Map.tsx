import React from 'react';
import { Dimensions } from 'react-native';
import styled from 'styled-components/native';
import { AntDesign } from '@expo/vector-icons';

import { Map } from '../../../assets/Map/Map';
import { theme } from '../../providers/styles';

const { width, height } = Dimensions.get('window');

const Box = styled.View`
    flex: 1;
`;

const Header = styled.View`
    height: 100px;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: row;
    background: ${(props) => props.theme.colors.calmBlue};
`;

const HeaderText = styled.Text`
    color: ${(props) => props.theme.colors.white};
    font-size: 24px;
    font-family: NunitoBold;
`;

const ActionBox = styled.View`
    background: ${(props) => props.theme.colors.darkBlue};
    border-radius: 15px;
    position: absolute;
    bottom: 40px;
    height: 110px;
    width: 80%;
    display: flex;
    left: 10%;
    padding: 17px;
    padding-left: 20px;
    padding-right:20px;
`;

const ActionBoxHeader = styled.Text`
    color: ${(props) => props.theme.colors.white};
    font-size: 14px;
    font-family: NunitoBold;
    margin-bottom: 5px;
`;

const ActionBoxAddress = styled.Text`
    color: ${(props) => props.theme.colors.white};
    font-size: 14px;
    font-family: NunitoRegular;
    margin-bottom: 5px;
`;

const ArrowIconBox = styled.View`
    position: absolute;
    right: 25px;
    top: 35px;
    color: ${(props) => props.theme.colors.darkBlue};
    background: ${(props) => props.theme.colors.white};
    height: 50px;
    width: 50px;
    border-radius: 25px;
    display: flex;
    justify-content: center;
    align-items: center;
`;


const ratioFix = width / height >= 0.58 ? height * 0.08 : 0;

const BackgroundView = styled.View`
    z-index: -99999;
    display: flex;
    align-items: center;
    justify-content: center;
    width: ${width}px;
    height: ${height}px;
`;

const BackButton = styled.View`
    position: absolute;
    left: 5%;
`;

export const MapScreen = ({navigation}) => (
    <Box>
        <Header>
            <BackButton >
                <AntDesign
                    onPress={navigation.goBack}
                    name='arrowleft'
                    color={theme.colors.white}
                    size={35}/>
            </BackButton>
            <HeaderText>Med facilities</HeaderText>
        </Header>
        <BackgroundView><Map
            width={width + ratioFix}
            height={height + ratioFix}/></BackgroundView>
        <ActionBox>
            <ActionBoxHeader>Wojewódzki szpital Specjalisty...</ActionBoxHeader>
            <ActionBoxAddress>ul. Koszarowa 5, Wrocław</ActionBoxAddress>
            <ActionBoxAddress>Arrival time: 9:56PM</ActionBoxAddress>
            <ArrowIconBox>
                <AntDesign
                    name='arrowright'
                    color={theme.colors.darkBlue}
                    size={35}/>
            </ArrowIconBox>
        </ActionBox>

    </Box>
);
