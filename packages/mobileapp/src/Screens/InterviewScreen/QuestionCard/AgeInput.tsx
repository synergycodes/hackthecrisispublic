import * as React from 'react';
import styled from 'styled-components/native';

import { CallbackType } from './QuestionCard';

const Container = styled.View`
    display: flex;
    align-items: center;
`;

const StyledInput = styled.TextInput`
    width: 180px;
    height: 50px;
    border: 1px solid ${({ theme }) => theme.colors.darkBlue};
    border-radius: 10px;
    padding: 10px;
    font-size: 23px;
    text-align: center;
    font-weight: bold;
`;

type Props = {
    callback: CallbackType;
};

export const AgeInput: React.FC<Props> = ({
    callback
}) => (
    <Container>
        <StyledInput
            placeholder="e.g. 45"
            keyboardType="number-pad"
            onChangeText={callback}
        />
    </Container>
);
