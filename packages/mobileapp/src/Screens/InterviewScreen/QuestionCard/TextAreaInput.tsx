import * as React from 'react';
import styled from 'styled-components/native';

import { CallbackType } from './QuestionCard';

const Container = styled.View`
    display: flex;
    align-items: center;
    width: 260px;
`;

const StyledInput = styled.TextInput`
    width: 100%;
    height: 150px;
    border: 1px solid ${({ theme }) => theme.colors.darkBlue};
    border-radius: 10px;
    padding: 20px;
    font-size: 14px;
    font-weight: bold;
`;

const InfoText = styled.Text`
    width: 100%;
    text-align: right;
    color: ${({ theme }) => theme.colors.darkBlue};
    font-size: 14px;
    opacity: 0.5;
`;

type Props = {
    callback: CallbackType;
};

export const TextAreaInput: React.FC<Props> = ({
    callback
}) => (
    <Container>
        <StyledInput
            placeholder="Detailed description of the course of the disease..."
            onChangeText={callback}
            multiline={true}
            numberOfLines={6}
            textAlignVertical="top"
        />
        <InfoText>* not required</InfoText>
    </Container>
);
