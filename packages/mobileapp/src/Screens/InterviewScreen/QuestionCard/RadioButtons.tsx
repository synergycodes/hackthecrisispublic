import * as React from 'react';
import styled, { css, useTheme } from 'styled-components/native';
import RadioForm, {
    RadioButton,
    RadioButtonInput,
    RadioButtonLabel
} from 'react-native-simple-radio-button';

import { CallbackType } from './QuestionCard';

const StyledRadioButton = styled(RadioButton)<{ isFirst: boolean }>(
    ({ isFirst }) => css`
        margin-top: ${isFirst ? '0px' : '40px'};
    `
);

const options = [
    { label: 'Yes', value: 0 },
    { label: 'No', value: 1 }
];

type Props = {
    callback: CallbackType;
};

export const RadioButtons: React.FC<Props> = ({
    callback
}) => {
    const { colors } = useTheme();
    const [selectedOption, setSelectedOption] = React.useState<number>();

    const selectOption = (option: number) => {
        setSelectedOption(option);
        callback(option === 0 ? true : false);
    };

    const labelStyles = {
        fontSize: 20,
        color: colors.darkBlue,
        marginLeft: 10
    };

    return (
        <RadioForm animation={true}>
            {options.map((option, index) => (
                <StyledRadioButton
                    labelHorizontal={true}
                    key={index}
                    isFirst={index === 0}
                >
                    <RadioButtonInput
                        obj={option}
                        index={index}
                        isSelected={index === selectedOption}
                        onPress={selectOption}
                        borderWidth={1}
                        buttonInnerColor={colors.darkBlue}
                        buttonOuterColor={colors.darkBlue}
                        buttonSize={25}
                        buttonOuterSize={40}
                    />
                    <RadioButtonLabel
                        obj={option}
                        index={index}
                        labelHorizontal={true}
                        onPress={selectOption}
                        labelStyle={labelStyles}
                    />
                </StyledRadioButton>
            ))}
        </RadioForm>
    );
};
