import * as React from 'react';
import styled from 'styled-components/native';
import { AntDesign } from '@expo/vector-icons';
import { ActivityIndicator } from 'react-native';

import { RadioButtons } from './RadioButtons';
import { CheckBox } from './CheckBox';
import { AgeInput } from './AgeInput';
import { TemperatureInput } from './TemperatureInput';
import { TextAreaInput } from './TextAreaInput';
import { DataInputType } from '../types/DataInputType';
import { Question } from '../types/Question';

const Container = styled.View`
    height: 420px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    background: ${({ theme }) => theme.colors.white};
    margin: 0 30px;
    border-radius: 10px;
    margin-top: 100px;
`;

const QuestionText = styled.Text`
    font-size: 18px;
    color: ${({ theme }) => theme.colors.darkBlue};
    margin: 40px;
    text-align: center; 
`;

const ButtonsRow = styled.View`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    width: 100%;
    margin-bottom: 30px;
    padding: 0 30px;
`;

const NextButtonArea = styled.View`
    flex: 1;
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
`;

const PreviousButtonArea = styled.View`
    flex: 1;
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
`;

const InputArea = styled.View`
    display: flex;
    align-items: center;
    width: 100%;
`;

const NextButton = styled.TouchableOpacity`
    width: 88px;
    height: 46px;
    display: flex;
    justify-content: center;
    align-items: center;
    background: ${({ theme }) => theme.colors.darkBlue};
    border-radius: 23px;
`;

const StyledIcon = styled(AntDesign)`
    color: white;
`;

const PreviousButton = styled.TouchableOpacity`
    width: 50px;
    height: 46px;
    display: flex;
    justify-content: center;
    align-items: center;
    background: #73C2CF;
    border-radius: 23px;
`;

const SymptomeText = styled.Text`
    color: ${({ theme }) => theme.colors.darkBlue};
    font-size: 20px;
    margin: 20px 0;
`;

const dataInputTypeToComponentMap = {
    [DataInputType.AgeInput]: AgeInput,
    [DataInputType.TemperatureInput]: TemperatureInput,
    [DataInputType.CheckBox]: CheckBox,
    [DataInputType.RadioButtons]: RadioButtons,
    [DataInputType.TextAreaInput]: TextAreaInput
};

export type CallbackType = (value: string | boolean) => void;

type Props = {
    next: () => void;
    previous: () => void;
    question: Question;
    callback: (id: number, value: boolean | string) => void;
    loading: boolean;
    showNextButton?: boolean;
    showPreviousButton?: boolean;
};

export const QuestionCard: React.FC<Props> = ({
    next,
    previous,
    question,
    callback,
    loading,
    showNextButton = true,
    showPreviousButton = true
}) => (
    <Container>
        <QuestionText>
            {question.text}
        </QuestionText>
        <InputArea>
            {React.createElement(
                dataInputTypeToComponentMap[question.inputType],
                {
                    callback: (value) => callback(question.id, value)
                }
            )}
            {question.symptome && (
                <SymptomeText>
                    {question.symptome}
                </SymptomeText>
            )}
        </InputArea>
        <ButtonsRow>
            <PreviousButtonArea>
                {showPreviousButton && (
                    <PreviousButton onPress={previous}>
                        <StyledIcon
                            name="arrowleft"
                            size={20}
                        />
                    </PreviousButton>
                )}
            </PreviousButtonArea>
            <NextButtonArea>
                {showNextButton && (
                    <NextButton onPress={next}>
                        {loading ? (
                            <ActivityIndicator size="small" color="white" />
                        ): (
                            <StyledIcon
                                name="arrowright"
                                size={20}
                            />
                        )}
                    </NextButton>
                )}
            </NextButtonArea>
        </ButtonsRow>
    </Container>
);
