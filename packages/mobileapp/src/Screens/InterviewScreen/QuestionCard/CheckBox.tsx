import * as React from 'react';
import styled from 'styled-components/native';
import { MaterialCommunityIcons } from '@expo/vector-icons';

import { CallbackType } from './QuestionCard';

const Container = styled.View`
    display: flex;
    align-items: center;
`;

const StyledIcon = styled(MaterialCommunityIcons)`
    color: ${({ theme }) => theme.colors.darkBlue};
`;

const StyledOpacity = styled.TouchableOpacity``;

type Props = {
    callback: CallbackType;
};

export const CheckBox: React.FC<Props> = ({
    callback
}) => {
    const [checked, setChecked] = React.useState(false);

    const toggleCheckded = () => setChecked((prev) => {
        callback(!prev);
        return !prev;
    });

    return (
        <Container>
            {checked ? (
                <StyledOpacity onPress={toggleCheckded}>
                    <StyledIcon name="checkbox-marked" size={60} />
                </StyledOpacity>
            ) : (
                <StyledOpacity onPress={toggleCheckded}>
                    <StyledIcon name="checkbox-blank-outline" size={60}/>
                </StyledOpacity>
            )}
        </Container>
    );
};
