import * as React from 'react';
import styled, { useTheme } from 'styled-components/native';

import { CallbackType } from './QuestionCard';

const Container = styled.View`
    display: flex;
    align-items: center;
`;

const StyledInput = styled.TextInput<{ appearanceColor: string }>`
    width: 180px;
    height: 50px;
    /* color: ${({ appearanceColor }) => appearanceColor}; */
    border: 1px solid ${({ appearanceColor }) => appearanceColor};
    border-radius: 10px;
    padding: 10px 63px 10px 10px;
    font-size: 23px;
    text-align: right;
    font-weight: bold;
`;

type FloaintUnitProps = {
    appearanceColor: string;
    unitBackgroundColor: string;
};
const FloatingUnit = styled.View<FloaintUnitProps>`
    position: absolute;
    right: 0;
    width: 53px;
    height: 50px;
    display: flex;
    justify-content: center;
    align-items: center;
    background: ${({ unitBackgroundColor }) => unitBackgroundColor};
    border: 1px solid ${({ appearanceColor }) => appearanceColor};
    border-top-right-radius: 10px;
    border-bottom-right-radius: 10px;
`;

const FloatingUnitText = styled.Text<{ appearanceColor: string }>`
    color: ${({ appearanceColor }) => appearanceColor};
    font-size: 23px;
    font-weight: bold;
`;

type Props = {
    callback: CallbackType;
};

export const TemperatureInput: React.FC<Props> = ({
    callback
}) => {
    const { colors } = useTheme();
    const [temp, setTemp] = React.useState<number | undefined>();

    const handleTempChange = (text: string) => {
        callback(text);
        if (text === '') {
            setTemp(undefined);
        } else {
            setTemp(Number.parseFloat(text));
        }
    };

    const color = temp
        ? temp >= 37.0
            ? colors.darkRed
            : colors.green
        : colors.darkBlue;
    const unitBackgroundColor = temp
        ? temp >= 37.0
            ? '#BB04461A'
            : '#24A5791A'
        : '#28519c1a';

    return (
        <Container>
            <StyledInput
                appearanceColor={color}
                placeholder="e.g. 36.6"
                keyboardType="number-pad"
                onChangeText={handleTempChange}
            />
            <FloatingUnit
                appearanceColor={color}
                unitBackgroundColor={unitBackgroundColor}
            >
                <FloatingUnitText appearanceColor={color}>°C</FloatingUnitText>
            </FloatingUnit>
        </Container>
    );
};
