import * as React from 'react';
import * as _ from 'lodash/fp';
import styled, { useTheme } from 'styled-components/native';
import SwipeableViews from 'react-swipeable-views-native';
import { Keyboard, StatusBar } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import gql from 'graphql-tag';
import { useMutation } from '@apollo/react-hooks';

import { ScreenContainer } from '../shared/ScreenContainer';
import { HomeNavProps } from '../../Navigators/HomeNavigator';
import { HomeRoute } from '../../Navigators/types/HomeRoutes';
import { QuestionCard } from './QuestionCard/QuestionCard';
import { DataInputType } from './types/DataInputType';
import { Question } from './types/Question';
import { mapScoreToCriterium } from './mapScoreToCriterium';

const Container = styled.View`
    flex: 1;
    z-index:10;
`;

const BlueBlob = styled.View`
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    border-bottom-left-radius: 30px;
    border-bottom-right-radius: 30px;
    flex: 1;
    height: 200px;
    background: ${({ theme }) => theme.colors.calmBlue};
    display: flex;
    flex-direction: row;
    justify-content: center;
    z-index: 9;
`;

const TouchableOpacity = styled.TouchableOpacity`
    position: absolute;
    left: 40px;
    top: 9px;
    height: 25px;
    width: 25px;
`;

const StyledIcon = styled(Ionicons)`
    color: white;
`;

const QuestionNumber = styled.Text`
    color: ${({ theme }) => theme.colors.white};
    font-size: 24px;
    font-weight: bold;
    align-self: flex-start;
`;

const Wrapper = styled.View`
   flex:1;
`;
const Header = styled.View`
   position: absolute;
   z-index: 100;
   display: flex;
   flex-direction: row;
   align-items: center;
   justify-content: center;
   width: 100%;
   margin-top: 30px;
`;


/* eslint-disable max-len*/
const questions: Question[] = [
    {
        id: 1,
        text: 'Have you been in the coronavirus transmission areas during the last 14 days?',
        inputType: DataInputType.RadioButtons
    },
    {
        id: 2,
        text: 'Have you had contact with a person who has been confirmed to have SARS CoV-2 infection during the last 14 days?',
        inputType: DataInputType.RadioButtons
    },
    {
        id: 3,
        text: 'How old are you?',
        inputType: DataInputType.AgeInput
    },
    {
        id: 4,
        text: 'What is your body temperature?',
        inputType: DataInputType.TemperatureInput
    },
    {
        id: 6,
        text: 'Do you have this symptom?',
        inputType: DataInputType.CheckBox,
        symptome: 'Cough'
    },
    {
        id: 7,
        text: 'Do you have this symptom?',
        inputType: DataInputType.CheckBox,
        symptome: 'Shortness of breath'
    },
    {
        id: 8,
        text: 'Would you like to tell something more?',
        inputType: DataInputType.TextAreaInput
    }
];
/* eslint-enable */
const numberofQuestions = questions.length - 1;

type Answer = { questionId: number; value: string };

const GET_SCORE = gql`
    mutation ($state: SubmitFormInputType!){
        submitForm(state: $state) {
            id
            userId
            createDate
            score
        }
    }
`;

export const InterviewScreen:
React.FC<HomeNavProps<HomeRoute.Interview>> = ({
    navigation
}) => {
    const { colors } = useTheme();
    const [activeQuestion, setActiveQuestion] = React.useState(0);
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [answers, setAnswers] = React.useState({});
    const [canNavigate, setCanNavigate] = React.useState(true);
    const [getScore, { data, loading }] = useMutation(GET_SCORE);

    if (data && canNavigate) {
        navigation.navigate(HomeRoute.Criteria, {
            criterium: mapScoreToCriterium(data.submitForm.score),
            formId: data.submitForm.id
        });
        setCanNavigate(false);
    }

    const next = () => {
        Keyboard.dismiss();
        setActiveQuestion((prev) =>
            _.min([numberofQuestions, prev + 1]));
    };

    const navigateToCriteria = () => {
        getScore({
            variables:
            {
                state: {
                    answers: _.map<string, Answer>(
                        (key) => ({
                            questionId: Number.parseInt(key),
                            value: answers[key].toString()
                        })
                    )(_.keys(answers))
                }
            }
        });
        setCanNavigate(true);
    };

    const previous = () => {
        Keyboard.dismiss();
        setActiveQuestion((prev) => _.max([0, prev - 1]));
    };

    const updateAnswers = (questionId: number, value: string | boolean) => {
        setAnswers((prev) => ({
            ...prev,
            [questionId]: value
        }));
    };

    return (
        <ScreenContainer>
            <StatusBar
                backgroundColor={colors.calmBlue}
                barStyle="light-content"
            />
            <Wrapper>
                <BlueBlob/>
                <Header>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <StyledIcon name="ios-arrow-back" size={20}/>
                    </TouchableOpacity>
                    <QuestionNumber>
                        Question {activeQuestion + 1}
                    </QuestionNumber>
                </Header>
                <Container>
                    <SwipeableViews
                        index={activeQuestion}
                        onChangeIndex={(index) => setActiveQuestion(index)}
                    >
                        {questions.map((question, i) => (
                            <QuestionCard
                                key={question.id}
                                next={i === numberofQuestions
                                    ? navigateToCriteria
                                    : next
                                }
                                previous={previous}
                                question={question}
                                callback={updateAnswers}
                                loading={loading}
                                showPreviousButton={i !== 0}
                            />))
                        }
                    </SwipeableViews>
                </Container>
            </Wrapper>
        </ScreenContainer>
    );
};
