export enum DataInputType {
    RadioButtons = 'RadioButtons',
    CheckBox = 'CheckBox',
    AgeInput = 'AgeInput',
    TemperatureInput = 'TemperatureInput',
    TextAreaInput = 'TextAreaInput'
};
