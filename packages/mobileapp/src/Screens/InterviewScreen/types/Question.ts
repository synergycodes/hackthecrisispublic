import { DataInputType } from './DataInputType';

export type Question = {
    id: number;
    text: string;
    inputType: DataInputType;
    symptome?: string;
};
