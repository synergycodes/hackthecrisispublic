import { Criterium } from '../CriteriaScreen';

export const mapScoreToCriterium = (score: number): Criterium => {
    if (score >= 40) {
        return Criterium.C;
    }

    if (score >= 20) {
        return Criterium.B;
    }

    return Criterium.A;
};
