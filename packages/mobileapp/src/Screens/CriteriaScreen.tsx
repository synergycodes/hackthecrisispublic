import styled, { useTheme } from 'styled-components/native';
import React from 'react';
import { Dimensions, GestureResponderEvent } from 'react-native';
import Svg, { Circle, Ellipse } from 'react-native-svg';
import { Feather } from '@expo/vector-icons';

import { ScreenTemplate } from './shared/ScreenTemplate';
import { HomeNavProps } from '../Navigators/HomeNavigator';
import { HomeRoute } from '../Navigators/types/HomeRoutes';

const { width, height } = Dimensions.get('window');

const Blob = styled.View`
    position: absolute;
    width: ${width}px;
    height: ${height}px;
`;

const CriteriaText = styled.Text`
    color: white;
    font-family: NunitoRegular;
    font-size: ${width/10}px;
    margin-top: ${height/8}px;
`;

const Value = styled.Text`
    color: white;
    font-family: NunitoBold;
    font-size: ${width/3}px;
    margin-top: ${-height/30}px;
`;

const InfoBox = styled.View`
    justify-content: center;
    margin-top: ${height*0.06}px;
    align-items: center;
    padding-left: ${width*0.08}px;
    padding-right: ${width*0.08}px;
    text-align: center;
`;

const InfoText = styled.Text`
    color: ${(props) => props.theme.colors.darkBlue};
    font-family: NunitoRegular;
    font-size: ${width*0.048}px;
    text-align: center;
`;

const CircleWithCriteria = styled.View`
    justify-content: center;
    flex-direction: row;
    align-items: center;
    margin-top: ${height*0.008}px;
    margin-bottom: ${height*0.008}px;
`;

const CriteriaLabel = styled.Text`
    color: ${(props) => props.theme.colors.darkBlue};
    font-family: NunitoBold;
    font-size: ${width*0.058}px;
    margin-left: ${width*0.032}px;
`;

export enum Criterium {
    A = 'A',
    B = 'B',
    C = 'C',
    NO = 'NO'
}

/* eslint-disable max-len */
const criteriumToDescriptionMap = {
    [Criterium.A]: 'without disease symptoms.',
    [Criterium.B]: 'with disease symptoms (runny nose, subfebrile condition, common cold symptoms.)',
    [Criterium.C]: 'with signs of acute respiratory infection (fever > 38 ◦C with coughing & shortness of breath.)',
    [Criterium.NO]: 'Your current symptoms don\'t show up about infection, but measure the temperature, wash your hands properly and follow the instructions.'
};
/* eslint-enable */

export const CriteriaScreen: React.FC<HomeNavProps<HomeRoute.Criteria>>= ({
    navigation,
    route
}) => {
    const theme = useTheme();
    const { criterium, formId } = route.params;

    const onPress = () => {
        navigation.navigate(HomeRoute.PersonalData, { formId });
    };

    const color = ((criterium: Criterium) => {
        const { gold, orange, green, darkRed } = theme.colors;
        const map = {
            A: gold,
            B: orange,
            C: darkRed,
            NO: green
        };
        return map[criterium];
    })(criterium);


    const CircleSVG = (fill: string) => (
        <Svg height={width*0.059} width={width*0.059}>
            <Circle
                r={width*0.059/2}
                cx={width*0.059/2}
                cy={width*0.059/2}
                fill={fill}
            />
        </Svg>
    );

    const LabelElementTop = criterium !== Criterium.NO
        ? <CriteriaText>Criteria</CriteriaText>
        : <CriteriaText>No symptoms</CriteriaText>;

    const LabelElementBottom = criterium === Criterium.NO
        ? <Feather name='check' size={width/2.8} color='white'/>
        : <Value>{criterium}</Value>;

    const BlobElement = (
        <Blob>
            <Svg height={height} width={width}>
                <Ellipse
                    cx={width+105}
                    cy='-15'
                    rx="290"
                    ry="129"
                    fill={color}
                />
            </Svg>
        </Blob>
    );

    const InfoBoxElement = (
        <InfoBox>
            {(criterium !== Criterium.NO) && (
                <>
                    <InfoText>
                    You have been classified as
                    </InfoText>
                    <CircleWithCriteria>
                        {CircleSVG(color)}
                        <CriteriaLabel>
                            Criteria {criterium}
                        </CriteriaLabel>
                    </CircleWithCriteria>
                </>)}
            <InfoText>
                {criteriumToDescriptionMap[criterium]}
            </InfoText>
        </InfoBox>
    );

    const screen = (
        <>
            {BlobElement}
            {LabelElementTop}
            {LabelElementBottom}
        </>
    );

    return (
        <ScreenTemplate
            descriptionBox={InfoBoxElement}
            topScreen={screen}
            buttonLabel={criterium === Criterium.NO ? 'Save' : 'Send'}
            buttonColor={theme.colors.darkBlue}
            buttonOnPress={onPress}
        />
    );
};
