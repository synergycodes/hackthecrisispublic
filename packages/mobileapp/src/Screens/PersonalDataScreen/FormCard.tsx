import * as React from 'react';
import styled from 'styled-components/native';
import { AntDesign } from '@expo/vector-icons';
import { ActivityIndicator } from 'react-native';

const Container = styled.View`
    height: 580px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    background: ${({ theme }) => theme.colors.white};
    margin: 0 30px;
    border-radius: 10px;
    margin-top: 100px;
`;

const MainText = styled.Text`
    font-size: 18px;
    color: ${({ theme }) => theme.colors.darkBlue};
    margin: 25px;
    text-align: center; 
`;

const ButtonsRow = styled.View`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    width: 100%;
    margin-bottom: 30px;
    padding: 0 30px;
`;

const NextButtonArea = styled.View`
    flex: 1;
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
`;

const PreviousButtonArea = styled.View`
    flex: 1;
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
`;

const InputArea = styled.View`
    display: flex;
    align-items: center;
    width: 100%;
`;

const NextButton = styled.TouchableOpacity`
    width: 88px;
    height: 46px;
    display: flex;
    justify-content: center;
    align-items: center;
    background: ${({ theme }) => theme.colors.darkBlue};
    border-radius: 23px;
`;

const StyledIcon = styled(AntDesign)`
    color: white;
`;

const PreviousButton = styled.TouchableOpacity`
    width: 50px;
    height: 46px;
    display: flex;
    justify-content: center;
    align-items: center;
    background: #73C2CF;
    border-radius: 23px;
`;

const SendText = styled.Text`
    color: ${({ theme }) => theme.colors.white};
    font-size: 18px;
`;

export type CallbackType = (value: string | boolean) => void;

type Props = {
    next: () => void;
    previous: () => void;
    loading?: boolean;
    toSend?: boolean;
    showNextButton?: boolean;
    showPreviousButton?: boolean;
};

export const FormCard: React.FC<Props> = ({
    next,
    previous,
    loading,
    toSend = false,
    showNextButton = true,
    showPreviousButton = true,
    children
}) => (
    <Container>
        <MainText>
            Provide personal details
        </MainText>
        <InputArea>
            {children}
        </InputArea>
        <ButtonsRow>
            <PreviousButtonArea>
                {showPreviousButton && (
                    <PreviousButton onPress={previous}>
                        <StyledIcon
                            name="arrowleft"
                            size={20}
                        />
                    </PreviousButton>
                )}
            </PreviousButtonArea>
            <NextButtonArea>
                {showNextButton && (
                    <NextButton onPress={next}>
                        {loading ? (
                            <ActivityIndicator size="small" color="white" />
                        ): toSend ? (
                            <SendText>
                                Send
                            </SendText>
                        ) : (
                            <StyledIcon
                                name="arrowright"
                                size={20}
                            />
                        )}
                    </NextButton>
                )}
            </NextButtonArea>
        </ButtonsRow>
    </Container>
);
