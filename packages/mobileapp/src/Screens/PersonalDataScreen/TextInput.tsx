import * as React from 'react';
import styled from 'styled-components/native';

const Container = styled.View`
    display: flex;
    align-items: center;
    width: 230px;
    margin: 10px 0 ; 
`;

const StyledInput = styled.TextInput<{ fontSize: number; }>`
    width: 100%;
    height: 50px;
    border: 1px solid ${({ theme }) => theme.colors.darkBlue};
    border-radius: 10px;
    padding: 10px;
    font-size: ${({ fontSize }) => fontSize ?? 23}px;
    font-weight: bold;
    text-align: center;
    color: ${({ theme }) => theme.colors.darkBlue};
`;

type Props = {
    fontSize?: number;
    placeholder: string;
    name: string;
    keyboardType?: string;
    callback: (name: string, value: string) => void;
};

export const TextInput: React.FC<Props> = ({
    fontSize,
    placeholder,
    name,
    keyboardType,
    callback
}) => (
    <Container>
        <StyledInput
            fontSize={fontSize}
            placeholder={placeholder}
            keyboardType={keyboardType as any}
            onChangeText={(value) => callback(name, value)}
        />
    </Container>
);
