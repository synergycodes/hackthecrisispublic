import * as React from 'react';
import * as _ from 'lodash/fp';
import styled, { useTheme } from 'styled-components/native';
import SwipeableViews from 'react-swipeable-views-native';
import { Keyboard, StatusBar } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { useMutation } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import { useSelector } from 'react-redux';

import { ScreenContainer } from '../shared/ScreenContainer';
import { HomeNavProps } from '../../Navigators/HomeNavigator';
import { HomeRoute } from '../../Navigators/types/HomeRoutes';
import { FormCard } from './FormCard';
import { TextInput } from './TextInput';
import { CheckBox } from '../InterviewScreen/QuestionCard/CheckBox';
import { RootState } from '../../store/types';

const Container = styled.View`
    flex: 1;
    background-color: ${(props) => props.theme.colors.lightMint};
`;

const BlueBlob = styled.View`
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    border-bottom-left-radius: 30px;
    border-bottom-right-radius: 30px;
    flex: 1;
    height: 200px;
    background: ${({ theme }) => theme.colors.calmBlue};
    display: flex;
    flex-direction: row;
    justify-content: center;
`;

const TouchableOpacity = styled.TouchableOpacity`
    position: absolute;
    top: 38px;
    left: 38px;
`;

const StyledIcon = styled(Ionicons)`
    color: white;
`;

const HeaderTitle = styled.Text`
    color: ${({ theme }) => theme.colors.white};
    font-size: 24px;
    font-weight: bold;
    margin-top: 30px;
`;

const ApplyMyselfRow = styled.View`
    display: flex;
    flex-direction: row;
    align-items: center;
`;

const ApplyMyselfText = styled.Text`
    color: ${({ theme }) => theme.colors.darkBlue};
    font-size: 20px;
    font-weight: bold;
    margin-left: 16px;
`;

const InputsRow = styled.View`
    margin-top: -50px;
`;

const Info = styled.Text`
    width: 200px;
    color: ${({ theme }) => theme.colors.darkRed};
    font-size: 12px;
    text-align: center;
    align-self: center;
`;

const infoText = 'Your personal details will'
    + ' be directed and proceed by SANEPID.';

const numberofScreens = 2;

const SEND_USER_PROFILE = gql`
    mutation createUserProfile ($profile: PatientProfileInputType!) {
        createOrUpdatePatientProfile(profile: $profile) {
            id
            status
            category
	    }
    }
`;

const CONNECT_FORM_WITH_USER = gql`
    mutation connectFormWithUserProfile ($userId: Float!, $stateId: Float!) {
        setUser(userId: $userId, stateId: $stateId)
    }
`;

export const PersonalDataScreen:
React.FC<HomeNavProps<HomeRoute.PersonalData>> = ({
    navigation,
    route
}) => {
    const { colors } = useTheme();
    const [activeQuestion, setActiveQuestion] = React.useState(0);
    const [answers, setAnswers] = React.useState({});
    const deviceId = useSelector<RootState>((state) => state.deviceId);
    const [sendUserProfile, { loading }]
        = useMutation(SEND_USER_PROFILE);
    const [connectProfileWithForm] = useMutation(CONNECT_FORM_WITH_USER);

    const next = () => {
        Keyboard.dismiss();
        setActiveQuestion((prev) =>
            _.min([numberofScreens, prev + 1]));
    };

    const navigateToEndScreen = () => {
        (async () => {
            const { data } = await sendUserProfile({
                variables:
                {
                    profile: {
                        /* eslint-disable dot-notation */
                        name: answers['fullName'].split(' ')[0],
                        surname: answers['fullName'].split(' ')[1],
                        pesel: answers['pesel'],
                        phone: answers['phone'],
                        email: answers['email'],
                        isMainDeviceUser: answers['isMainDeviceUser'],
                        deviceId
                        /* eslint-enable*/
                    }
                }
            });

            if (data.createOrUpdatePatientProfile.id) {
                await connectProfileWithForm({
                    variables: {
                        userId: Number.parseFloat(
                            data.createOrUpdatePatientProfile.id
                        ),
                        stateId: Number.parseFloat(
                            route.params.formId.toString()
                        )
                    }
                });

                navigation.navigate(HomeRoute.ApplicationSent);
            }
        })();
    };

    const previous = () => {
        Keyboard.dismiss();
        setActiveQuestion((prev) => _.max([0, prev - 1]));
    };

    const updateAnswers = (propName: string, value: string | boolean) => {
        setAnswers((prev) => ({
            ...prev,
            [propName]: value
        }));
    };

    return (
        <ScreenContainer>
            <StatusBar
                backgroundColor={colors.calmBlue}
                barStyle="light-content"
            />
            <Container>
                <BlueBlob>
                    <TouchableOpacity>
                        <StyledIcon name="ios-arrow-back" size={20} />
                    </TouchableOpacity>
                    <HeaderTitle>
                        {activeQuestion === 0 ? 'Personal data' : 'Address'}
                    </HeaderTitle>
                </BlueBlob>
                <SwipeableViews
                    index={activeQuestion}
                    onChangeIndex={(index) => setActiveQuestion(index)}
                >
                    <FormCard
                        next={next}
                        previous={previous}
                        showPreviousButton={false}
                    >
                        <InputsRow>
                            <ApplyMyselfRow>
                                <CheckBox
                                    callback={(value) =>
                                        updateAnswers('isMainDeviceUser', value)
                                    }
                                />
                                <ApplyMyselfText>
                                    Apply myself
                                </ApplyMyselfText>
                            </ApplyMyselfRow>
                            <TextInput
                                name="fullName"
                                placeholder="Full Name"
                                callback={updateAnswers}
                            />
                            <TextInput
                                name="email"
                                placeholder="Email"
                                keyboardType="email-address"
                                callback={updateAnswers}
                            />
                            <TextInput
                                name="pesel"
                                placeholder="PESEL"
                                keyboardType="number-pad"
                                callback={updateAnswers}
                            />
                            <TextInput
                                name="phone"
                                placeholder="Telephone"
                                keyboardType="number-pad"
                                callback={updateAnswers}
                            />
                        </InputsRow>
                    </FormCard>
                    <FormCard
                        next={navigateToEndScreen}
                        previous={previous}
                        loading={loading}
                        toSend={true}
                    >
                        <InputsRow>
                            <TextInput
                                name="streetName"
                                placeholder="Street Name"
                                callback={updateAnswers}
                            />
                            <TextInput
                                name="unitNumber"
                                placeholder="Unit Number"
                                callback={updateAnswers}
                            />
                            <TextInput
                                name="postalCode"
                                placeholder="Postal Code"
                                callback={updateAnswers}
                            />
                            <TextInput
                                name="city"
                                placeholder="City"
                                callback={updateAnswers}
                            />
                            <TextInput
                                fontSize={14}
                                name="additionalInformation"
                                placeholder="Additional information"
                                callback={updateAnswers}
                            />
                            <Info>
                                {infoText}
                            </Info>
                        </InputsRow>
                    </FormCard>
                </SwipeableViews>
            </Container>
        </ScreenContainer>
    );
};
