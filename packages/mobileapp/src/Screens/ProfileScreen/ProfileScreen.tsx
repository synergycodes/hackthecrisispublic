import styled from 'styled-components/native';
import React from 'react';

import { ProfileNavProps } from '../../Navigators/ProfileNavigator';
import { ScreenContainer } from '../shared/ScreenContainer';
import { ProfileRoute } from '../../Navigators/types/ProfileRoutes';

const MainView = styled.View`
    flex: 1;
    background-color: ${(props) => props.theme.colors.white};
`;

const WelcomeText = styled.Text`
    margin-left: 15px;
    margin-top: 5px;
    font-size: 28px;
    font-weight: bold;
`;

export const ProfileScreen:
React.FC<ProfileNavProps<ProfileRoute.Profile>> = () => (
    <ScreenContainer>
        <MainView>
            <WelcomeText>Profile!</WelcomeText>
        </MainView>
    </ScreenContainer>
);
