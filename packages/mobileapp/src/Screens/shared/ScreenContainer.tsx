import * as React from 'react';
import styled from 'styled-components/native';

const ScreenContainerView = styled.View<{ addMargin: boolean }>`
    flex: 1;
    margin-top: ${({ theme, addMargin }) =>
        addMargin ? theme.sizes.headerHeight : 0}px;
`;

type Props = {
    addMargin?: boolean;
};

export const ScreenContainer: React.FC<Props> = ({
    children,
    addMargin = false
}) => (
    <ScreenContainerView addMargin={addMargin}>
        {children}
    </ScreenContainerView>
);
