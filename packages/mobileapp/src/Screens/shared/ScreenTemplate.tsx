import styled from 'styled-components/native';
import React, { ReactChild } from 'react';
import { Dimensions, GestureResponderEvent } from 'react-native';

const { width, height } = Dimensions.get('window');

const MainView = styled.View`
    flex: 1;
    background-color: ${(props) => props.theme.colors.lightGrey};
    align-items: center;
`;

const Background = styled.View`
    position: absolute;
    align-self: center;
    width: ${width*1.064}px;
    height: ${height*0.72}px;
    background-color: ${(props) => props.theme.colors.calmBlue};
    border-bottom-left-radius: 35px;
    border-bottom-right-radius: 35px;
`;

const InfoContainer = styled.View`
    background-color: white;
    width: ${width*0.87}px;
    height: ${height*0.465}px;
    margin-bottom: 0;
    margin-top: auto;
    border-top-left-radius: ${width*0.027}px;
    border-top-right-radius: ${width*0.027}px;
    align-items: center;
`;

type ButtonProps = {
    color: string
}

const Button = styled.TouchableOpacity<ButtonProps>`
    background-color: ${({ color }) => color};
    border-radius: ${width*0.61}px;
    width: ${width*0.57}px;
    height: ${height*0.07}px;
    justify-content: center;
    align-items: center;
    margin-bottom: ${height*0.07}px;
    margin-top: auto;
`;

const ButtonText = styled.Text`
    color: white;
    font-family: NunitoBold;
    font-size: ${width*0.058}px;
`;

type Props = {
    descriptionBox: ReactChild,
    topScreen: ReactChild,
    buttonOnPress: (event: GestureResponderEvent) => void,
    buttonLabel: string,
    buttonColor: string
}

export const ScreenTemplate: React.FC<Props> = ({
    descriptionBox,
    topScreen,
    buttonOnPress,
    buttonLabel,
    buttonColor
}) => {

    const InfoContainerElement = (
        <>
            {descriptionBox}
            <Button color={buttonColor} onPress={buttonOnPress}>
                <ButtonText>
                    {buttonLabel}
                </ButtonText>
            </Button>
        </>
    );

    return (
        <MainView>
            <Background />
            {topScreen}
            <InfoContainer>
                {InfoContainerElement}
            </InfoContainer>
        </MainView>
    );
};
