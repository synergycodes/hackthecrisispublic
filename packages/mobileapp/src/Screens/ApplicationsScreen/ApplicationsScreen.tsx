import styled, { useTheme } from 'styled-components/native';
import React from 'react';
import { StatusBar, Dimensions } from 'react-native';
import { MaterialCommunityIcons, AntDesign } from '@expo/vector-icons';

import { ApplicationsNavProps } from '../../Navigators/ApplicationsNavigator';
import { ScreenContainer } from '../shared/ScreenContainer';
import { ApplicationsRoute } from '../../Navigators/types/ApplicationsRoutes';

const { height } = Dimensions.get('window');

const Container = styled.View`
    flex: 1;
    background-color: ${(props) => props.theme.colors.lightMint};
    margin-bottom: 65px;
`;

const BlueBlob = styled.View`
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    border-bottom-left-radius: 30px;
    border-bottom-right-radius: 30px;
    flex: 1;
    height: 200px;
    background: ${({ theme }) => theme.colors.calmBlue};
    display: flex;
    flex-direction: row;
    justify-content: center;
`;

const TileView = styled.ScrollView`
    margin-top: ${height/8.5}px;
`;

const Tile = styled.TouchableOpacity`
    width: 80%;
    padding-left: 20px;
    padding-right: 20px;
    padding-top: 25px;
    padding-bottom: 25px;
    background-color: white;
    margin: 15px;
    border-radius: 30px;
    align-self: center;
`;

const PersonInfoContainer = styled.View`
    flex-direction: row;
    align-items: flex-end;
    margin-bottom: 8px;
`;

const Name = styled.Text`
    font-family: NunitoBold;
    font-size: 21px;
    color: ${({ theme }) => theme.colors.calmBlue};
`;

const Age = styled.Text`
    font-family: NunitoBold;
    font-size: 13px;
    margin-bottom: 4px;
    color: ${({ theme }) => theme.colors.calmBlue};
`;

const StyledPersonalIcon = styled(MaterialCommunityIcons)`
    color: ${({ theme }) => theme.colors.salmon};
    margin-right: 8px;
`;

const PersonalView = styled.View`
    margin-bottom: 8px;
    flex-direction: row;
    align-items: center;
`;

const PersonalText = styled.Text`
    font-family: NunitoRegular;
    font-size: 15px;
`;

const StatusView = styled.View`
    flex-direction: row;
`;

const StatusLabel = styled.Text`
    font-family: NunitoBold;
    font-size: 15px;
`;

const StatusInfo = styled.Text`
    font-family: NunitoRegular;
    font-size: 15px;
`;

const Arrow = styled.View`
    position: absolute;
    top: 50%;
    right: 10%;
`;

const AddButton = styled.TouchableOpacity`
    background-color: ${({ theme }) => theme.colors.calmBlue}90;
    width: 60px;
    height: 60px;
    align-items: center;
    justify-content: center;
    border-radius: 100px;
    position: absolute;
    bottom: 10px;
    right: 30px;
`;

type SARSInfoBoxProps = {
    bgColor: string
}

const SARSInfoBox = styled.View<SARSInfoBoxProps>`
    width: 90px;
    height: 50px;
    background-color: ${({ bgColor }) => bgColor};
    border-radius: 15px;
    justify-content: center;
    align-items: center;
    padding-bottom: 10px;
    padding-top: 10px;
    position: absolute;
    top: -25px;
    right: -15px;
`;

const SARSInfoTop = styled.Text`
    font-family: NunitoRegular;
    font-size: 11px;
    color: white;
`;

const SARSInfoBottom = styled.Text`
    font-family: NunitoBold;
    font-size: 13px;
    color: white;
`;

const Title = styled.Text`
    color: ${({ theme }) => theme.colors.white};
    font-size: 30px;
    margin-top: ${height/30}px;
    font-family: NunitoBold;
`;

enum ApplicationStatus {
    Waiting = 'Waiting for response',
    Quarantine = 'Quarantine'
}

enum HealthStatus {
    Positive = 'Positive',
    Negative = 'Negative',
    NoData = 'NoData'
}

type ApplicationInfo = {
    name: string,
    age: number,
    isPersonal: boolean,
    applicationStatus: ApplicationStatus,
    healthStatus: HealthStatus
}

const mock_data: ApplicationInfo[] = [
    {
        name: 'Anna Kowalska',
        age: 46,
        isPersonal: true,
        applicationStatus: ApplicationStatus.Waiting,
        healthStatus: HealthStatus.Positive
    },
    {
        name: 'Tomasz Kowalski',
        age: 16,
        isPersonal: false,
        applicationStatus: ApplicationStatus.Waiting,
        healthStatus: HealthStatus.NoData
    },
    {
        name: 'Halina Nowak',
        age: 80,
        isPersonal: false,
        applicationStatus: ApplicationStatus.Quarantine,
        healthStatus: HealthStatus.Negative
    },
    {
        name: 'Halina Nowak',
        age: 80,
        isPersonal: true,
        applicationStatus: ApplicationStatus.Quarantine,
        healthStatus: HealthStatus.Positive
    },
    {
        name: 'Halina Nowak',
        age: 80,
        isPersonal: false,
        applicationStatus: ApplicationStatus.Quarantine,
        healthStatus: HealthStatus.Negative
    },
];

export const ApplicationsScreen:
React.FC<ApplicationsNavProps<ApplicationsRoute.Applications>> = () => {
    const { colors } = useTheme();

    const tile = (info: ApplicationInfo, key: number) => (
        <Tile key={key.toString(10)}>
            <PersonInfoContainer>
                <Name>{info.name}</Name>
                <Age> ({info.age})</Age>
            </PersonInfoContainer>
            {info.isPersonal
                && <PersonalView>
                    <StyledPersonalIcon name="checkbox-marked" size={30} />
                    <PersonalText>Personal application</PersonalText>
                </PersonalView>}
            <StatusView>
                <StatusLabel>Status: </StatusLabel>
                <StatusInfo>{info.applicationStatus}</StatusInfo>
            </StatusView>
            <Arrow>
                <AntDesign
                    name='arrowright'
                    size={30}
                    color={colors.calmBlue}
                />
            </Arrow>
            {info.healthStatus !== HealthStatus.NoData
                && <SARSInfoBox
                    bgColor={info.healthStatus === HealthStatus.Positive
                        ? colors.darkRed
                        : colors.lawnGreen}
                >
                    <SARSInfoTop>SARS CoV-2</SARSInfoTop>
                    <SARSInfoBottom>{info.healthStatus}</SARSInfoBottom>
                </SARSInfoBox>}
        </Tile>
    );

    const contentContainerStyle = {
        paddingTop: 15
    };

    return (
        <ScreenContainer>
            <StatusBar
                backgroundColor={colors.calmBlue}
                barStyle="light-content"
            />
            <Container>
                <BlueBlob>
                    <Title>
                        My applications
                    </Title>
                </BlueBlob>
                <TileView contentContainerStyle={contentContainerStyle}>
                    {mock_data.map((info, key) => tile(info, key))}
                </TileView>
                <AddButton>
                    <AntDesign name="plus" size={30} color={'white'}/>
                </AddButton>
            </Container>
        </ScreenContainer>
    );
};
