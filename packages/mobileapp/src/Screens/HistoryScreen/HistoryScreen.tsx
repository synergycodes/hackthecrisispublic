import styled from 'styled-components/native';
import React from 'react';

import { HistoryNavProps } from '../../Navigators/HistoryNavigator';
import { ScreenContainer } from '../shared/ScreenContainer';
import { HistoryRoute } from '../../Navigators/types/HistoryRoutes';

const MainView = styled.View`
    flex: 1;
    background-color: ${(props) => props.theme.colors.white};
`;

const WelcomeText = styled.Text`
    margin-left: 15px;
    margin-top: 5px;
    font-size: 28px;
    font-weight: bold;
`;

export const HistoryScreen:
React.FC<HistoryNavProps<HistoryRoute.History>> = () => (
    <ScreenContainer>
        <MainView>
            <WelcomeText>History!</WelcomeText>
        </MainView>
    </ScreenContainer>
);
