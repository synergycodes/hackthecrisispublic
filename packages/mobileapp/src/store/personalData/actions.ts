import { PersonalData, PersonalDataAction, SET_PERSONAL_DATA } from './types';

export const setPersonalData
= (personalData: PersonalData): PersonalDataAction => ({
    type: SET_PERSONAL_DATA,
    payload: personalData
});
