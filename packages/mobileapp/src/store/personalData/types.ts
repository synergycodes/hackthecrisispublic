export type PersonalData = {
    fullName: string;
    phone: string;
    email: string;
    pesel: string;
};

export const SET_PERSONAL_DATA = '[PERSONAL DATA] SET_PERSONAL_DATA';
export type SetPersonalDataAction = {
    type: typeof SET_PERSONAL_DATA;
    payload: PersonalData;
};

export type PersonalDataAction =
    | SetPersonalDataAction;
