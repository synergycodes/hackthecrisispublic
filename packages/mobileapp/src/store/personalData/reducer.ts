import { PersonalData, PersonalDataAction, SET_PERSONAL_DATA } from './types';

export type PersonalDataState = PersonalData;

const defaultState: PersonalData = {
    fullName: '',
    pesel: '',
    phone: '',
    email: ''
};

export const personalDataReducer = (
    state: PersonalDataState = defaultState,
    action: PersonalDataAction
): PersonalDataState => {
    switch (action.type) {
        case SET_PERSONAL_DATA:
            return action.payload;
        default:
            return state;
    }
};
