import {
    createStore,
    compose,
    applyMiddleware,
    Middleware
} from 'redux';
import { logger } from 'redux-logger';

import { epicMiddleware, rootEpic } from './epic';
import { rootReducer } from './reducer';

export const middlewares: Middleware[] = [epicMiddleware];
if (process.env.NODE_ENV === 'development') {
    middlewares.push(logger);
}

export const store = createStore(
    rootReducer,
    compose(
        applyMiddleware(...middlewares)
    )
);

epicMiddleware.run(rootEpic);
