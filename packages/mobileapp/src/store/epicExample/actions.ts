import { PING, PONG, EpicExampleAction } from './types';

export const ping = (): EpicExampleAction => ({
    type: PING
});

export const pong = (): EpicExampleAction => ({
    type: PONG
});
