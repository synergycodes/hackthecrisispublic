import { Epic, ofType } from 'redux-observable';
import { delay, map } from 'rxjs/operators';

import {
    EpicExampleAction,
    PingAction,
    PING
} from './types';
import { pong } from './actions';

export const exampleEpic:
Epic<EpicExampleAction, EpicExampleAction>
    = (action$) => action$.pipe(
        ofType<EpicExampleAction, PingAction>(PING),
        delay(1000),
        map(() => pong())
    );
