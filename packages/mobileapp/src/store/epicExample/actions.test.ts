import { mockStore } from '../../test/redux';
import { ping, pong } from './actions';
import { PING, PONG } from './types';

describe('epicExample actions', () => {
    const store = mockStore({});
    afterEach(() => store.clearActions());

    it('should create an action of type PING', () => {
        store.dispatch(ping());

        expect(store.getActions())
            .toContainEqual({
                type: PING,
            });
    });

    it('should create an action of type PONG', () => {
        store.dispatch(pong());

        expect(store.getActions())
            .toContainEqual({
                type: PONG,
            });
    });
});
