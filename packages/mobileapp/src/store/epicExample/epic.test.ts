/* eslint-disable jest/expect-expect */
import { rxTest } from '../../test/rxTest';
import { ping } from './actions';
import { exampleEpic } from './epic';
import { PONG } from './types';

describe('exampleEpic()', () => {
    it('should return PONG after 1s if it receives PING', () => {
        rxTest().run(({ hot, expectObservable }) => {
            const action$ = hot('-a', {
                a: ping(),
            }) as any;
            const state$ = null;

            const output$ = exampleEpic(action$, state$, {});

            expectObservable(output$).toBe('1s -a', {
                a: {
                    type: PONG
                }
            });
        });
    });
});
