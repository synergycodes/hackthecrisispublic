export const PING = '[EPIC EXAMPLE] PING';
export type PingAction = {
    type: typeof PING;
}

export const PONG = '[EPIC EXAMPLE] PONG';
export type PongAction = {
    type: typeof PONG;
}

export type EpicExampleAction =
    | PingAction
    | PongAction;
