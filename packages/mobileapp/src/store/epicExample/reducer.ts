import { EpicExampleAction, PING, PONG } from './types';

export type EpicExampleState = string;

const initialState = '';

export const epicExampleReducer = (
    state: EpicExampleState = initialState,
    action: EpicExampleAction
) => {
    switch (action.type) {
        case PING:
            return PING;
        case PONG:
            return PONG;
        default:
            return state;
    }
};
