import { EpicExampleState, epicExampleReducer } from './reducer';
import { ping, pong } from './actions';
import { PING, PONG } from './types';

describe('epicEcampleReducer()', () => {
    it('should set state to PING', () => {
        const state: EpicExampleState = '';
        const action = ping();

        const actualResult = epicExampleReducer(state, action);

        const expectedResult = PING;
        expect(actualResult).toEqual(expectedResult);
    });

    it('should set state to PONG', () => {
        const state: EpicExampleState = '';
        const action = pong();

        const actualResult = epicExampleReducer(state, action);

        const expectedResult = PONG;
        expect(actualResult).toEqual(expectedResult);
    });
});
