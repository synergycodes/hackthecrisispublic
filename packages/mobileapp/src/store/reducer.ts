import { combineReducers } from 'redux';

import { deviceIdReducer } from './authentication/reducer';
import { personalDataReducer } from './personalData/reducer';

export const rootReducer = combineReducers({
    deviceId: deviceIdReducer,
    personalData: personalDataReducer,
});
