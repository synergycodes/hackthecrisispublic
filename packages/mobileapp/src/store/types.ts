import { rootReducer } from './reducer';
import { AddDeviceIdAction } from './authentication/types';
import { EpicExampleAction } from './epicExample/types';
import { PersonalDataAction } from './personalData/types';

export type RootState = ReturnType<typeof rootReducer>;

export type RootAction =
    | EpicExampleAction
    | AddDeviceIdAction
    | PersonalDataAction;
