import {
    createEpicMiddleware,
    combineEpics,
    Epic,
    ActionsObservable,
    StateObservable
} from 'redux-observable';
import { BehaviorSubject } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { RootAction, RootState } from './types';
import { exampleEpic } from './epicExample/epic';

const rootEpics = [
    exampleEpic
];

const epic$ = new BehaviorSubject(
    combineEpics(...rootEpics) as Epic<RootAction, RootAction>
);

export const rootEpic = (
    action$: ActionsObservable<RootAction>,
    state$: StateObservable<RootState>
) =>
    epic$
        .pipe(
            mergeMap((epic: Epic) => epic(action$, state$, {}))
        );

export const epicMiddleware
    = createEpicMiddleware<RootAction, RootAction, RootState>();
