export const ADD_DEVICE_ID = '[DEVICE_ID] ADD_DEVICE_ID';

export type DeviceId = string;

export type AddDeviceIdAction = {
    type: typeof ADD_DEVICE_ID;
    payload: DeviceId;
};
