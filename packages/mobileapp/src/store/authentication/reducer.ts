import { DeviceId, AddDeviceIdAction, ADD_DEVICE_ID } from './types';

export type AddDeviceIdState = string;

const defaultState: AddDeviceIdState = '';

export const deviceIdReducer = (
    state: AddDeviceIdState = defaultState,
    action: AddDeviceIdAction
): DeviceId => {
    switch (action.type) {
        case ADD_DEVICE_ID:
            return action.payload;
        default:
            return state;
    }
};
