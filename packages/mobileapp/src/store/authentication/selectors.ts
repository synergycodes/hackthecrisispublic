import { RootState } from '../types';
import { DeviceId } from './types';

export const newReservationIdSelector = (state: RootState): DeviceId =>
    state.deviceId;
