import { DeviceId, ADD_DEVICE_ID, AddDeviceIdAction } from './types';

export const addDeviceId
= (deviceId: DeviceId): AddDeviceIdAction => ({
    type: ADD_DEVICE_ID,
    payload: deviceId
});
