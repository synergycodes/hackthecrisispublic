/* eslint-disable no-console */
import { Notifications } from 'expo';
import gql from 'graphql-tag';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';

import { client } from '../providers/ApolloProvider';

export const sendDeviceInfo = async (deviceId) => {

    if (!Constants.isDevice){
        return;
    }
    const { status: existingStatus }
        = await Permissions.getAsync(Permissions.NOTIFICATIONS);
    let finalStatus = existingStatus;

    if (existingStatus !== 'granted') {
        const { status }
            = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        finalStatus = status;
    }

    if (finalStatus !== 'granted') {
        return;
    }

    console.log(finalStatus);
    console.log(
        'Notification Token: ',
        await Notifications.getExpoPushTokenAsync()
    );
    const token = await Notifications.getExpoPushTokenAsync();
    const ADD_DEVICE_TOKEN = gql`
        mutation {
        setDevicePushToken(conf: {
          deviceId: "${deviceId}",
          pushToken: "${token}"
        })
      }
    `;

    const resp = await client.mutate({
        mutation: ADD_DEVICE_TOKEN
    });

    console.log(resp);
};
