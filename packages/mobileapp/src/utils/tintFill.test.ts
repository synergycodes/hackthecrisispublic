import { tintFill } from './tintFill';

describe.each([
    ['#ffffff', 100, 0, '#ffffff'],
    ['#2137ff', 42, 0.2, '#8894ff'],
])('should return expected results', (baseFill, value, offset, expected) => {
    it('should work', () => {
        expect(tintFill(baseFill, value, offset)).toEqual(expected);
    });
});
