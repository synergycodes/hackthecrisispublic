import { shadow } from './shadow';

describe.each`
    elevation | expectedElevation | expectedBoxShadow
    ${10}     | ${'10'}           | ${'0 5px 8px #000000AD'}
    ${15}     | ${'15'}           | ${'0 7.5px 12px #000000AD'}
    ${20}     | ${'20'}           | ${'0 10px 16px #000000AD'}
`('shadow', ({ elevation, expectedElevation, expectedBoxShadow }) => {

    it('should return proper elevation', () => {
        const css = shadow(elevation).join('');
        expect(css).toContain(`elevation: ${expectedElevation}`);
    });

    it('should return proper box-shadow', () => {
        const css = shadow(elevation).join('');
        expect(css).toContain(`box-shadow: ${expectedBoxShadow}`);
    });

});
