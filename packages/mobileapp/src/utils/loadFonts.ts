import * as Font from 'expo-font';

export const loadFonts = async (setFontLoaded) => {
    const getFont = async () => {
        await Font.loadAsync({
            NunitoBold: require('../../assets/shared/fonts/Nunito-Bold.ttf'),
            NunitoRegular:
                require('../../assets/shared/fonts/Nunito-Regular.ttf')
        });
        setFontLoaded(true);
    };
    getFont();
};
