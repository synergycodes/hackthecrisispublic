export const tintFill = (baseFill: string, value: number, offset: number) => {
    const r = parseInt(baseFill.substr(1, 2), 16);
    const g = parseInt(baseFill.substr(3, 2), 16);
    const b = parseInt(baseFill.substr(5, 2), 16);

    return '#'
    +Math.floor(r + (255-r) * (1-value/100) * (1-offset) + offset).toString(16)
    +Math.floor(g + (255-g) * (1-value/100) * (1-offset) + offset).toString(16)
    +Math.floor(b + (255-b) * (1-value/100) * (1-offset) + offset).toString(16);
};
