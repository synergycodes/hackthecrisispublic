import { css } from 'styled-components/native';

// Based on https://stenbeck.io/styling-shadows-in-react-native-ios-and-android

export const shadow = (elevation: number = 15) =>
    css`
        elevation: ${elevation};
        box-shadow: 0 ${elevation * 0.5}px ${elevation * 0.8}px #000000AD;
    `;


export const homeScreenBoxShadow = () =>
    css`
      shadow-color: #000;
      shadow-offset: 1px 1px;
      shadow-opacity: 0.5;
      shadow-radius: 2px;
      elevation: 2;
`;
