import * as React from 'react';

import { StackedScreenProps } from './types/StackedScreenProps';
import { ProfileScreen } from '../Screens/ProfileScreen/ProfileScreen';
import {
    ProfileNavigatorParamList,
    ProfileRoute
} from './types/ProfileRoutes';
import {
    createCommonStackNavigator
} from './shared/createCommonStackNavigator';

export type ProfileNavProps<T extends keyof ProfileNavigatorParamList>
    = StackedScreenProps<ProfileNavigatorParamList, T>;

const StackNavigator
= createCommonStackNavigator<ProfileNavigatorParamList>();

export const ProfileNavigator: React.FC = () => (
    <StackNavigator.Navigator>
        <StackNavigator.Screen
            name={ProfileRoute.Profile}
            component={ProfileScreen}
        />
    </StackNavigator.Navigator>
);
