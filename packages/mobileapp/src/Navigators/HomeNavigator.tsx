import * as React from 'react';

import { StackedScreenProps } from './types/StackedScreenProps';
import { HomeScreen } from '../Screens/HomeScreen/HomeScreen';
import {
    PersonalDataScreen
} from '../Screens/PersonalDataScreen/PersonalDataScreen';
import { InterviewScreen } from '../Screens/InterviewScreen/InterviewScreen';
import { HomeNavigatorParamList, HomeRoute } from './types/HomeRoutes';
import {
    createCommonStackNavigator
} from './shared/createCommonStackNavigator';
import { CriteriaScreen } from '../Screens/CriteriaScreen';
import { ApplicationSentScreen } from '../Screens/ApplicationSentScreen';
import { MapScreen } from '../Screens/Map/Map';

export type HomeNavProps<T extends keyof HomeNavigatorParamList>
    = StackedScreenProps<HomeNavigatorParamList, T>;

const StackNavigator = createCommonStackNavigator<HomeNavigatorParamList>();

export const HomeNavigator: React.FC = () => (
    <StackNavigator.Navigator initialRouteName={HomeRoute.Home}>
        <StackNavigator.Screen
            name={HomeRoute.PersonalData}
            component={PersonalDataScreen}
        />
        <StackNavigator.Screen
            name={HomeRoute.Home}
            component={HomeScreen}
        />
        <StackNavigator.Screen
            name={HomeRoute.Interview}
            component={InterviewScreen}
        />
        <StackNavigator.Screen
            name={HomeRoute.Criteria}
            component={CriteriaScreen}
        />
        <StackNavigator.Screen
            name={HomeRoute.ApplicationSent}
            component={ApplicationSentScreen}
        />
        <StackNavigator.Screen
            name={HomeRoute.Map}
            component={MapScreen}
        />
    </StackNavigator.Navigator>
);
