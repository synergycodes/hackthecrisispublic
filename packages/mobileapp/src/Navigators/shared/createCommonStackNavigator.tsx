import * as React from 'react';
import {
    createStackNavigator,
    StackNavigationOptions,
    TransitionPresets
} from '@react-navigation/stack';
import {
    ParamListBase,
    DefaultNavigatorOptions,
    StackRouterOptions
} from '@react-navigation/native';

import { Header } from '../RootNavigator/Header/Header';

type NavigatorProps
    = DefaultNavigatorOptions<StackNavigationOptions> & StackRouterOptions;

export const createCommonStackNavigator
= <T extends ParamListBase>() => {
    const StackNavigator = createStackNavigator<T>();

    return {
        Navigator: ({
            children,
            initialRouteName,
            screenOptions
        }: NavigatorProps) => <StackNavigator.Navigator
            initialRouteName={initialRouteName}
            screenOptions={{
                header: Header,
                ...TransitionPresets.SlideFromRightIOS,
                ...screenOptions
            }}
            headerMode="screen"
        >
            {children}
        </StackNavigator.Navigator>,
        Screen: StackNavigator.Screen
    };
};
