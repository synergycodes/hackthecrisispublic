import React from 'react';
import * as _ from 'lodash/fp';
import { createBottomTabNavigator, BottomTabBar } from '@react-navigation/bottom-tabs';
import { NavigationState } from '@react-navigation/native';
import { useTheme } from 'styled-components';
import { Dimensions } from 'react-native';

import { HomeNavigator } from '../HomeNavigator';
import { RootParamList, RootRoute } from '../types/RootRoutes';
import { HistoryNavigator } from '../HistoryNavigator';
import { TabBarIcon } from './TabBarIcon';
import { ApplicationsNavigator } from '../ApplicationsNavigator';
import { ProfileNavigator } from '../ProfileNavigator';

const { width } = Dimensions.get('window');

type NavigationRoute = { state: NavigationState };
const isInNestedRoute = (routes: NavigationRoute[]) => _.flowRight(
    _.lte(1),
    _.max,
    _.map<NavigationRoute, number>((x: NavigationRoute) => x.state?.index)
)(routes);

const Tab = createBottomTabNavigator<RootParamList>();

export const RootNavigator: React.FC = () => {
    const { colors } = useTheme();

    const tabBarOptions = {
        showLabel: false,
        style: {
            position: 'absolute',
            height: 65,
            backgroundColor: colors.calmBlue,
            borderTopLeftRadius: width*0.069,
            borderTopRightRadius: width*0.069,
        }
    };

    const tabColors = [
        colors.calmBlue,
        colors.mint,
        colors.veryDarkBlue,
        colors.veryDarkMint
    ];

    return (
        <Tab.Navigator
            screenOptions={({ navigation, route }) => ({
                tabBarIcon: ({ focused }) => (
                    <TabBarIcon route={route} focused={focused} />
                ),
                tabBarVisible: !isInNestedRoute(
                    navigation.dangerouslyGetState().routes
                )
            })}
            tabBarOptions={tabBarOptions as any}
            tabBar={
                (props) => {
                    const backgroundColor = tabColors[props.state.index];
                    return (
                        <BottomTabBar
                            {...props}
                            style={{
                                ...tabBarOptions.style,
                                backgroundColor } as any
                            }
                        />
                    );
                }
            }
        >
            <Tab.Screen
                name={RootRoute.Home}
                component={HomeNavigator}
            />
            <Tab.Screen
                name={RootRoute.Applications}
                component={ApplicationsNavigator}
            />
            <Tab.Screen
                name={RootRoute.History}
                component={HistoryNavigator}
            />
            <Tab.Screen
                name={RootRoute.Profile}
                component={ProfileNavigator}
            />
        </Tab.Navigator>
    );
};
