import * as React from 'react';
import { StackHeaderProps } from '@react-navigation/stack';
import styled from 'styled-components/native';
import { Animated } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

import { shadow } from '../../../utils/shadow';
import { Bar } from './Bar/Bar';

const Container = styled(SafeAreaView)`
    flex: 1;
    flex-direction: row;
`;

const Content = styled(Animated.View)`
    flex: 1;
    height: ${({ theme }) => theme.sizes.headerHeight}px;
    display: flex;
    align-items: flex-start;
    justify-content: flex-start;
    background: ${({ theme }) => theme.colors.white};
    overflow: hidden;
    ${shadow()}
`;

export const Header: React.FC<StackHeaderProps> = ({
    scene: { route },
    navigation: { dangerouslyGetState, goBack },
}) => {

    if (true) {
        return null;
    }

    return (
        <Container>
            <Content>
                <Bar
                    title={route.name}
                    showBackButton={dangerouslyGetState().index > 0}
                    navigateBack={goBack}
                />
            </Content>
        </Container>
    );
};
