import * as React from 'react';
import styled, { css, useTheme } from 'styled-components/native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Ionicons } from '@expo/vector-icons';

const BarContainer = styled.View`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    width: 100%;
    height: ${({ theme }) => theme.sizes.headerHeight}px;
`;

const HeaderTitle = styled.View`
    position: absolute;
    display: flex;
    flex-direction: row;
    justify-content: center;
    width: 100%;
    font-size: 20px;
`;

export const TitleText = styled.Text`
    font-size: 20px;
`;

const HeaderLeft = styled.View`
    display: flex;
    flex-direction: row;
    align-items: center;
`;

export const BackButton = styled.View(({ theme }) => css`
    margin-left: 16px;
    width: ${theme.sizes.headerIcon}px;
    height: ${theme.sizes.headerIcon}px;
    display: flex;
    align-items: center;
`);

type Props = {
    title: string;
    showBackButton: boolean;
    navigateBack: () => void;
};

export const Bar: React.FC<Props> = ({
    title,
    showBackButton,
    navigateBack,
}) => {
    const theme = useTheme();

    return (
        <BarContainer>
            <HeaderTitle>
                <TitleText>{title}</TitleText>
            </HeaderTitle>
            <HeaderLeft>
                {showBackButton
                    && (
                        <TouchableOpacity onPress={navigateBack}>
                            <BackButton>
                                <Ionicons
                                    name={theme.icons.backArrow}
                                    size={theme.sizes.iconSmall}
                                />
                            </BackButton>
                        </TouchableOpacity>
                    )
                }
            </HeaderLeft>
        </BarContainer>
    );
};
