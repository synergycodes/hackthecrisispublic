import * as React from 'react';
import styled, { useTheme, css } from 'styled-components/native';

import { RootParamList, RootRoute } from '../types/RootRoutes';
import { nameof } from '../../utils/nameof';
import { HomeIcon } from '../../../assets/Main/HomeIcon';
import { HistoryIcon } from '../../../assets/Main/HistoryIcon';
import { ApplicationsIcon } from '../../../assets/Main/ApplicationsIcon';
import { ProfileIcon } from '../../../assets/Main/ProfileIcon';

type BottomBarIconProps = {
    focused: boolean;
};

const BottomBarIcon = styled.View<BottomBarIconProps>(({
    focused
}) => css`
    background-color: transparent;
    opacity: ${focused ? 1 : 0.5};
    width: 50px;
    height: 50px;
    display: flex;
    justify-content: center;
    align-items: center;
    overflow: hidden;
`);

type Props = {
    route: any;
    focused: boolean;
};

export const TabBarIcon: React.FC<Props> = ({ route, focused }) => {
    const { colors, sizes } = useTheme();

    const rootRouteToIconMap: { [key: string]: [typeof Icon] } = {
        [nameof<RootParamList>(RootRoute.Home)]: [HomeIcon],
        [nameof<RootParamList>(RootRoute.History)]: [HistoryIcon],
        [nameof<RootParamList>(RootRoute.Applications)]: [ApplicationsIcon],
        [nameof<RootParamList>(RootRoute.Profile)]: [ProfileIcon]
    };

    const [Icon] = rootRouteToIconMap[route.name];

    return (
        <BottomBarIcon focused={focused}>
            <Icon
                size={sizes.headerIcon}
                color={colors.white}
            />
        </BottomBarIcon>
    );
};
