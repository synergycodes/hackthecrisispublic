import * as React from 'react';

import { StackedScreenProps } from './types/StackedScreenProps';
import { ApplicationsScreen } from '../Screens/ApplicationsScreen/ApplicationsScreen';
import {
    ApplicationsNavigatorParamList,
    ApplicationsRoute
} from './types/ApplicationsRoutes';
import {
    createCommonStackNavigator
} from './shared/createCommonStackNavigator';

export type ApplicationsNavProps<T extends keyof ApplicationsNavigatorParamList>
    = StackedScreenProps<ApplicationsNavigatorParamList, T>;

const StackNavigator
= createCommonStackNavigator<ApplicationsNavigatorParamList>();

export const ApplicationsNavigator: React.FC = () => (
    <StackNavigator.Navigator>
        <StackNavigator.Screen
            name={ApplicationsRoute.Applications}
            component={ApplicationsScreen}
        />
    </StackNavigator.Navigator>
);
