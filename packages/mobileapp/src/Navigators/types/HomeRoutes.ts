import { Criterium } from '../../Screens/CriteriaScreen';

export enum HomeRoute {
    PersonalData = 'Personal data',
    Home = 'Home',
    Interview = 'Interview',
    Criteria = 'Criteria',
    ApplicationSent = 'ApplicationSent',
    Map = 'Map'
}

export type HomeNavigatorParamList = {
    [HomeRoute.PersonalData]: {
        formId: number;
    };
    [HomeRoute.Home]: undefined;
    [HomeRoute.Interview]: undefined;
    [HomeRoute.Criteria]: {
        criterium: Criterium;
        formId: number;
    },
    [HomeRoute.ApplicationSent]: undefined;
    [HomeRoute.Map]: undefined;
};
