export enum RootRoute {
    Home = 'Home',
    History = 'History',
    Applications = 'Applications',
    Profile = 'Profile',
    Map = 'Map'
}

export type RootParamList = {
    [RootRoute.Home]: undefined;
    [RootRoute.History]: undefined;
    [RootRoute.Applications]: undefined;
    [RootRoute.Profile]: undefined;
    [RootRoute.Map]: undefined;
};
