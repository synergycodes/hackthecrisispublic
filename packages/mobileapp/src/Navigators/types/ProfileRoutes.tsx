export enum ProfileRoute {
    Profile = 'Profile',
};

export type ProfileNavigatorParamList = {
    [ProfileRoute.Profile]: undefined;
};
