export enum HistoryRoute {
    History = 'History',
};

export type HistoryNavigatorParamList = {
    [HistoryRoute.History]: undefined;
};
