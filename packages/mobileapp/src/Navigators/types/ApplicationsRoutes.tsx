export enum ApplicationsRoute {
    Applications = 'Applications',
};

export type ApplicationsNavigatorParamList = {
    [ApplicationsRoute.Applications]: undefined;
};
