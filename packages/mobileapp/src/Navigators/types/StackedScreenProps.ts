import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';

export type StackedScreenProps<
    ParamsList extends Record<string, object>,
    K extends keyof ParamsList
> = {
    navigation?: StackNavigationProp<ParamsList, K>;
    route?: RouteProp<ParamsList, K>;
};
