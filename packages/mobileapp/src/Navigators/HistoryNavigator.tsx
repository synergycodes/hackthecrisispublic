import * as React from 'react';

import { StackedScreenProps } from './types/StackedScreenProps';
import { HistoryScreen } from '../Screens/HistoryScreen/HistoryScreen';
import { HistoryNavigatorParamList, HistoryRoute } from './types/HistoryRoutes';
import {
    createCommonStackNavigator
} from './shared/createCommonStackNavigator';

export type HistoryNavProps<T extends keyof HistoryNavigatorParamList>
    = StackedScreenProps<HistoryNavigatorParamList, T>;

const StackNavigator = createCommonStackNavigator<HistoryNavigatorParamList>();

export const HistoryNavigator: React.FC = () => (
    <StackNavigator.Navigator>
        <StackNavigator.Screen
            name={HistoryRoute.History}
            component={HistoryScreen}
        />
    </StackNavigator.Navigator>
);
