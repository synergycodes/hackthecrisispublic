import * as Location from 'expo-location';
import { LocationData, Accuracy } from 'expo-location';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';

import { client } from '../providers/ApolloProvider';
import { ADD_DEVICE_LOCATION } from './queries/sendPositions';

const timeInterval = 10 * 1000;
const distanceInterval = 10;
/**
 * @property {number} timeInterval - Minimum time to wait between each update
 * in milliseconds.
 * @property {number} distanceInterval - Receive updates only when the location
 * has changed by at least this distance in meters.
 */
const options = {
    enableHighAccuracy: true,
    timeInterval,
    distanceInterval
};

const watchPositionCallback = (
    { coords: { latitude, longitude, accuracy }, timestamp, }: LocationData
) => {
    accuracy > Accuracy.High && client.mutate({
        mutation: ADD_DEVICE_LOCATION,
        variables: {
            data: {
                deviceId: Constants.installationId,
                location: {
                    lat: latitude,
                    lon: longitude
                },
                timestamp
            }
        }
    }).catch((err) => {
        // TODO: add retry mechanism
        // eslint-disable-next-line no-console
        console.log(err);
    }).then((resp) => {
        // eslint-disable-next-line no-console
        console.log(resp);
    });
};

export const startPositionListener = () => {
    let removeWatch;
    Permissions
        .askAsync(Permissions.LOCATION)
        .then((response) => {
            if (response.granted) {
                removeWatch = Location
                    .watchPositionAsync(options, watchPositionCallback);
            }
        })
        .catch((e) => {
            // eslint-disable-next-line no-console
            console.log(e);
        });

    return removeWatch?.remove;
};
