import { newsList } from '../../assets/Mocks/NewsList';

export type News = {
    title: string;
    text: string;
    date: string;
    id: string;
}

export const fetchNewsList = (): Promise<News[]> => Promise.resolve(newsList);
