import gql from 'graphql-tag';

export const ADD_DEVICE_LOCATION = gql`
  mutation DeviceTraceMutation($data: DeviceLocationInput!) {
  addDeviceTrace(data: $data) {
    id
  }
}
`;
