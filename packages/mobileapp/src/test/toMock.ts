// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const toMock = <T extends (...args: any[]) => any>(fn: T) =>
    fn as jest.MockedFunction<T>;
