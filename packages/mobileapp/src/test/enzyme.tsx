import * as React from 'react';
import { mount as _mount, shallow as _shallow } from 'enzyme';

import { MockThemeProvider } from './MockThemeProvider';

const MockProvider: React.FC = ({ children }) => (
    <MockThemeProvider>
        { children }
    </MockThemeProvider>
);

const mockOptions = {
    wrappingComponent: MockProvider,
};

export const mount: typeof _mount = (node: React.ReactElement) =>
    _mount(node, mockOptions);

export const shallow: typeof _shallow = (node: React.ReactElement) =>
    _shallow(node, mockOptions);
