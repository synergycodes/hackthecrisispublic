import configureStore from 'redux-mock-store';
import { createEpicMiddleware } from 'redux-observable';

import { RootAction, RootState } from '../store/types';

export const getFakeState
    = (extendingObject: RootState): RootState => ({
        ...extendingObject
    });

export type DispatchExtensions = {
    (action: RootAction): RootAction;
}

const middlewares = [
    createEpicMiddleware<RootAction, RootAction, RootState>()
];

export const mockStore = configureStore<Partial<RootState>, DispatchExtensions>(
    middlewares
);
