import * as React from 'react';
import { ThemeProvider } from 'styled-components/native';

export const mockTheme: Theme = {
    colors: {
        white: 'white',
        blue: 'blue',
        lightBlue: 'lightBlue'
    },
    icons: {
        backArrow: 'ios-arrow-back',
        cross: 'cross'
    },
    sizes: {
        headerIcon: {
            width: 25,
            height: 25
        },
        headerHeight: 60,
        icon: 45,
        iconSmall: 30,
        iconVerySmall: 25,
        iconBig: 60,
        expandedHeaderHeight: 210
    },
};

export const MockThemeProvider: React.FC = ({ children }) => (
    <ThemeProvider theme={mockTheme}>
        {children}
    </ThemeProvider>
);
