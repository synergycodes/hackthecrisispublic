import { ShallowWrapper, ReactWrapper } from 'enzyme';

export const simulateOnPress = (el: ShallowWrapper | ReactWrapper) => {
    el.prop<() => void>('onPress')();
};
