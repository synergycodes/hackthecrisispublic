import { TestScheduler } from 'rxjs/testing';

export const rxTest = () => new TestScheduler(
    (actual: unknown, expected: unknown) =>
        expect(actual).toEqual(expected)
);
