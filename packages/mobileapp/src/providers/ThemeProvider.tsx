import * as React from 'react';
import {
    ThemeProvider as BaseThemeProvider
} from 'styled-components/native';

import { theme } from './styles';

type ThemeProviderProps = {
    children: React.ReactChild | React.ReactChild[];
}

export const ThemeProvider: React.FC<ThemeProviderProps> = ({
    children
}) => (
    <BaseThemeProvider theme={theme}>
        { children }
    </BaseThemeProvider>
);
