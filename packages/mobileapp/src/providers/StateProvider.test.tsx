import * as React from 'react';

import { mount } from '../test/enzyme';
import { StateProvider } from './StateProvider';

describe('<StateProvider />', () => {
    it('should render its children', () => {
        const children = <div>I am a child</div>;

        const wrapper = mount(<StateProvider>
            {children}
        </StateProvider>);

        expect(wrapper).toContainReact(children);
    });
});
