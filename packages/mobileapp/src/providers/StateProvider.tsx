import * as React from 'react';
import { Provider } from 'react-redux';

import { store } from '../store/store';

export const StateProvider: React.FC = ({
    children
}) => (
    <Provider store={store}>
        {children}
    </Provider>
);
