import * as React from 'react';
import { ApolloProvider as BaseApolloProvider } from '@apollo/react-hooks';
import { BatchHttpLink } from 'apollo-link-batch-http';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-boost';

import { CONFIG } from '../config';

const { APOLLO_URL } = CONFIG;

export const client = new ApolloClient({
    link: new BatchHttpLink({ uri: APOLLO_URL }),
    cache: new InMemoryCache()
});

export const ApolloProvider: React.FC = ({
    children
}) => (
    <BaseApolloProvider client={client}>
        {children}
    </BaseApolloProvider>
);
