declare global {
    type Theme = typeof theme;
}

export const theme = {
    colors: {
        white: 'white',
        lightBlue: '#2699FB',
        blue: '#2699FB',
        salmon: '#FEA79C',
        lightGrey: '#EAEFF1',
        calmBlue: '#04A2BB',
        darkBlue: '#28519C',
        darkRed: '#BB0446',
        gold: '#F2C530',
        orange: '#FF9A2C',
        green: '#24A579',
        dark: '#0C101E',
        backgroundColor: '#CEEAEE59',
        lightMint: '#EAEFF1',
        mint: '#04A2BB',
        veryDarkMint: '#067192',
        veryDarkBlue: '#28519C',
        lawnGreen: '#24A579'
    },
    sizes: {
        headerIcon: 25,
        headerHeight: 60,
        expandedHeaderHeight: 210,
        icon: 45,
        iconSmall: 30,
        iconVerySmall: 25,
        iconBig: 60
    },
    icons: {
        backArrow: 'ios-arrow-back',
        cross: 'cross'
    }
};
