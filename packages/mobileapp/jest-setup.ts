// eslint-disable-next-line import/no-extraneous-dependencies
import 'jest-enzyme';
// eslint-disable-next-line import/no-extraneous-dependencies
import 'jest-styled-components';
// @ts-ignore
// eslint-disable-next-line import/no-extraneous-dependencies
import jestFetchMock from 'jest-fetch-mock';

(jestFetchMock as any).enableMocks();

const suppressDomErrors = () => {
    // eslint-disable-next-line max-len
    const suppressedErrors = /(React does not recognize the.*prop on a DOM element|Unknown event handler property|is using uppercase HTML|Received `%s` for a non-boolean attribute `%s`|The tag.*is unrecognized in this browser|PascalCase)/;
    // eslint-disable-next-line no-console
    const realConsoleError = console.error;
    // eslint-disable-next-line no-console
    console.error = (message, ...rest) => {
        if (message.match(suppressedErrors)) {
            return;
        }
        realConsoleError(message, ...rest);
    };
};

suppressDomErrors();
