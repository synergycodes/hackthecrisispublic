/* eslint-disable import/no-default-export */
/* Expo needs a default export in App.tsx in order to work */

import * as React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { YellowBox } from 'react-native';

import { Main } from './src/Main';
import { ThemeProvider } from './src/providers/ThemeProvider';
import { StateProvider } from './src/providers/StateProvider';
import { ApolloProvider } from './src/providers/ApolloProvider';

export default function App() {
    YellowBox.ignoreWarnings([
        'componentWillReceiveProps',
        'componentWillMount'
    ]);
    return (
        <ThemeProvider>
            <StateProvider>
                <SafeAreaProvider>
                    <ApolloProvider>
                        <Main />
                    </ApolloProvider>
                </SafeAreaProvider>
            </StateProvider>
        </ThemeProvider>
    );
}
