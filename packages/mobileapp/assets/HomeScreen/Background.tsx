import * as React from 'react'
import Svg, {
    Defs,
    LinearGradient,
    Stop,
    ClipPath,
    Path,
    G
} from 'react-native-svg'

export const BackgroundHomeScreen = (props) => {
    return (
        <Svg width={375} height={118} viewBox="0 0 375 118" {...props}>
            <Defs>
                <LinearGradient
                    id="prefix__b"
                    x1={0.5}
                    x2={0.836}
                    y2={0.698}
                    gradientUnits="objectBoundingBox"
                >
                    <Stop offset={0} stopColor="#73c2cf"/>
                    <Stop offset={0} stopColor="#83c9d4" stopOpacity={0.886}/>
                    <Stop offset={0.509} stopColor="#a3d7df"
                          stopOpacity={0.655}/>
                    <Stop offset={1} stopColor="#ceeaee" stopOpacity={0.349}/>
                    <Stop offset={1} stopColor="#fff" stopOpacity={0}/>
                </LinearGradient>
                <ClipPath id="prefix__a">
                    <Path fill="#fff" stroke="#707070" d="M0 0h375v118H0z"/>
                </ClipPath>
            </Defs>
            <G clipPath="url(#prefix__a)">
                <Path
                    d="M-3827.895 9455.445s14.5 30.78 65.264 56.4c11.663 5.888 62.718 12.822 104.445 32.006 34.91 16.05 57.528 44.625 57.528 44.625s30.781 54.891 75.487 77.026 95.415 26.953 96.254 27.6c.476.368 8.939-14 9.589-13.514 52.478 39.1 110.025 23.92 137.939 44.8 28.51 21.325-22.808 147.817-22.808 147.817l-9.649-38.184s32.457-15.436 32.457-33.308-22.811-376.953-22.811-376.953-148.249-59.306-157.021-59.306-324.57 30.872-324.57 30.872z"
                    transform="rotate(-16 -35633.953 -8690.843)"
                    opacity={0.53}
                    fill="url(#prefix__b)"
                />
            </G>
        </Svg>
    )
};
