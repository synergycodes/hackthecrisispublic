import * as React from 'react'
import Svg, {
    Defs,
    LinearGradient,
    Stop,
    ClipPath,
    Path,
    G,
    Circle,
    SvgProps
} from 'react-native-svg'

export const Background = (props: SvgProps) => {
    return (
        <Svg
            data-name="Group 908"
            viewBox="0 0 340 667"
            {...props}
        >
            <Defs>
                <LinearGradient
                    id="prefix__b"
                    x1={0.5}
                    x2={0.836}
                    y2={0.698}
                    gradientUnits="objectBoundingBox"
                >
                    <Stop offset={0} stopColor="#73c2cf"/>
                    <Stop offset={0} stopColor="#83c9d4" stopOpacity={0.886}/>
                    <Stop offset={0.541} stopColor="#ceeaee"
                          stopOpacity={0.349}/>
                    <Stop offset={1} stopColor="#fff" stopOpacity={0}/>
                </LinearGradient>
                <ClipPath id="prefix__a">
                    <Path data-name="Rectangle 1545" fill="#04a2bb"
                          d="M0 0h375v667H0z"/>
                </ClipPath>
            </Defs>
            <G data-name="Mask Group 14" clipPath="url(#prefix__a)">
                <Path data-name="Rectangle 1544" fill="#04a2bb"
                      d="M0 0h375v667H0z"/>
                <Path
                    data-name="Path 932"
                    d="M-3827.895 9461.017s14.253 32.665 64.144 59.858c21.188 11.549 81.644 24.144 103.962 42.7 24.69 20.528 39.934 36.324 48.268 55.694.556 1.292 33.824 53.17 65.9 82.612 49.79 45.694 226.828 21.923 254.848 44.555s-22.416 156.869-22.416 156.869l-9.484-40.521s31.9-16.381 31.9-35.348-22.416-400.043-22.416-400.043-145.705-62.937-154.327-62.937-323.048-37.786-323.048-37.786z"
                    transform="translate(3730.297 -9425)"
                    fill="url(#prefix__b)"
                />
                <Path
                    data-name="Path 931"
                    d="M419.167 445.191s-17.851 8.033-39.986-5.712-26.457-82.518-63.051-118.754c-19.98-19.785-37.666-31.7-82.827-54.266s-60-20.922-97.819-35.993c-23.853-9.506-56.116-60.263-92.823-95.607C4.201 97.825-38.095 76.38-38.095 76.38"
                    fill="none"
                    stroke="#fff"
                    strokeLinecap="round"
                    opacity={0.47}
                />
                <Path
                    data-name="Path 928"
                    d="M-3827.9 9444.426s22.272 7.1 52.756 22.167c24.3 12.015 54.033 29.271 81.481 52.092 61.406 51.053 53.016 79.257 94.251 117.1s101.749 49.8 124.955 68.546 72.831 106.391 72.831 106.391l-7.854-33.56s26.419-13.567 26.419-29.275-18.565-331.308-18.565-331.308-120.67-52.124-127.811-52.124-266.956-16.07-266.956-16.07"
                    transform="translate(3790 -9444)"
                    fill="url(#prefix__b)"
                />
                <Path
                    data-name="Path 930"
                    d="M-33.742 57.966s3.941-17.863 44.819-1.263c18.559 7.537 48.734 28.595 68.715 48.38 36.594 36.236 74.255 97.651 96.39 111.4s39.986 5.712 39.986 5.712"
                    fill="none"
                    stroke="#ddf1f3"
                    strokeLinecap="round"
                    opacity={0.86}
                />
                <Circle
                    data-name="Ellipse 357"
                    cx={4.5}
                    cy={4.5}
                    r={4.5}
                    transform="translate(24 80)"
                    fill="#fff"
                />
                <Circle
                    data-name="Ellipse 358"
                    cx={3}
                    cy={3}
                    r={3}
                    transform="translate(69 81)"
                    fill="#fff"
                />
                <Circle
                    data-name="Ellipse 359"
                    cx={4.5}
                    cy={4.5}
                    r={4.5}
                    transform="translate(211 218)"
                    fill="#fff"
                />
                <Circle
                    data-name="Ellipse 360"
                    cx={12}
                    cy={12}
                    r={12}
                    transform="translate(358 418)"
                    fill="#fff"
                />
                <G transform="translate(-302 -110)" opacity={0.33}>
                    <Circle
                        data-name="Ellipse 56"
                        cx={32}
                        cy={32}
                        r={32}
                        transform="translate(287 579)"
                        fill="#b0e1e9"
                    />
                </G>
                <Circle
                    data-name="Ellipse 363"
                    cx={4.5}
                    cy={4.5}
                    r={4.5}
                    transform="translate(54 462)"
                    fill="#3cb6ca"
                />
            </G>
        </Svg>
    )
};
