import * as React from 'react'
import Svg, { Path } from 'react-native-svg'

export const PieChartIcon = (props) => {
    return (
        <Svg width={24.712} height={24} viewBox='0 0 24.712 24' fill='#fff' {...props}>
            <Path
                d='M11 24a11 11 0 010-22v11h11a11.012 11.012 0 01-11 11z'
            />
            <Path
                d='M24.712 11.232A11.232 11.232 0 0013.48 0v11.232z'
            />
        </Svg>
    )
};
