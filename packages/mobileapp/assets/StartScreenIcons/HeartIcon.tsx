import * as React from 'react'
import Svg, { Path } from 'react-native-svg'

export const HeartIcon = (props) => {
    return (
        <Svg width={28.903} height={25.209}
             viewBox='0 0 28.903 25.209' {...props}>
            <Path
                d='M26.674 2.229a7.6 7.6 0 00-10.757 0l-1.466 1.466-1.466-1.466A7.607 7.607 0 002.228 12.986l1.465 1.466 10.758 10.757 10.757-10.757 1.466-1.466a7.6 7.6 0 000-10.757z'
                fill='#fff'
            />
        </Svg>
    )
};
