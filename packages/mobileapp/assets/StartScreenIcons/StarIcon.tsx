import * as React from 'react'
import Svg, { Path } from 'react-native-svg'

export const StarIcon = (props) => {
    return (
        <Svg width={26.508} height={25.209}
             viewBox='0 0 26.508 25.209' {...props}>
            <Path
                d='M13.254 0l4.1 8.3 9.158 1.339-6.631 6.451 1.564 9.119-8.191-4.309-8.191 4.309 1.564-9.119L0 9.636 9.158 8.3z'
                fill='#fff'
            />
        </Svg>
    )
};
