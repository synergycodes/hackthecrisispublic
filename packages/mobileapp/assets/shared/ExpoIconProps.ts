export type ExpoIconProps = {
    size: number,
    color: string
};
