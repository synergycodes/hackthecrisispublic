import * as React from 'react';
import { Feather } from '@expo/vector-icons';
import { ExpoIconProps } from '../shared/ExpoIconProps';

export const HomeIcon: React.FC<ExpoIconProps> = ({ size, color }) => {
    return (
        <Feather name='home'
            size={size}
            color={color}
        />
    )
};
