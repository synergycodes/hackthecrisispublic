import * as React from 'react';
import { Feather } from '@expo/vector-icons';
import { ExpoIconProps } from '../shared/ExpoIconProps';

export const HistoryIcon: React.FC<ExpoIconProps> = ({ size, color }) => {
    return (
        <Feather name='thermometer'
            size={size}
            color={color}
        />
    )
};
