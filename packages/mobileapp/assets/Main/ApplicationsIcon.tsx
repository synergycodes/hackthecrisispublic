import * as React from 'react';
import { Feather } from '@expo/vector-icons';
import { ExpoIconProps } from '../shared/ExpoIconProps';

export const ApplicationsIcon: React.FC<ExpoIconProps> = ({ size, color }) => {
    return (
        <Feather name='file-text'
            size={size}
            color={color}
        />
    )
};
