import * as React from 'react';
import { Feather } from '@expo/vector-icons';
import { ExpoIconProps } from '../shared/ExpoIconProps';

export const ProfileIcon: React.FC<ExpoIconProps> = ({ size, color }) => {
    return (
        <Feather name='user'
            size={size}
            color={color}
        />
    )
};
