export const newsList = [
    {
        title: 'COVID-19',
        text: 'The coronavirus COVID-19 is affecting 186 countries and territories around the world and 1 international conveyance (the Diamond Princess cruise ship harbored in Yokohama, Japan). The day is reset after midnight GMT+0',
        date: '21/03/2020',
        id: '21'
    },
    {
        title: 'Stricter stay-at-home orders',
        text: 'Illinois plans to require residents to stay home as much as possible, aside from meeting their basic needs, starting Saturday evening. New York plans to ban all nonessential travel beginning Sunday evening, following California\'s lead, which began Friday. Connecticut and Oregon were preparing to do the same.',
        date: '31/02/2068',
        id: '47'
    },
];
