module.exports = {
    extends: [
        'plugin:react/recommended',
        '../common/.eslintrc.js'
    ],
    plugins: ['react-native'],
    rules: {
        'react-native/no-unused-styles': 2,
        'react-native/split-platform-components': 2,
        'react-native/no-inline-styles': 2,
        'react-native/no-raw-text': 0,
        'react-native/no-single-element-style-arrays': 2,
        'react-native/no-color-literals': 0,
        'react-native/sort-styles': 0,
        'react/prop-types': 0,
        'react/display-name': 0,
        'react-native/no-missing-generic-family-keyword': 0
    },
    settings: {
        react: {
            version: 'detect'
        }
    }
};
