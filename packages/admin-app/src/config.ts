export const CONFIG = {
    APOLLO_URL: process.env.APOLLO_URL || 'http://localhost:4000/graphql',
    APOLLO_SUBSCRIPTIONS_URL: process.env.APOLLO_SUBSCRIPTIONS_URL || 'wss://localhost:4000/graphql'
};
