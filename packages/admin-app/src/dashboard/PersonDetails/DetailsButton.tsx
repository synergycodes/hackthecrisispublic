import * as React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import { getColor, shadow } from '../../shared/theme';

type Props = {
    id: string;
}

const StyledDetailsButton = styled(Link)`
    display: flex;
    height: 40px;
    width: 179px;
    justify-content: center;
    align-items: center;
    font-size: 25px;
    color: #fff;
    background: ${getColor('tertiary')};
    border-radius: 20px;
    ${shadow()}
    cursor: pointer;
    text-decoration: none;
`;
export const DetailsButton: React.FC<Props> = ({ id }) => (
    <StyledDetailsButton to={`/entries/${id}`}>
        Details
    </StyledDetailsButton>
);
