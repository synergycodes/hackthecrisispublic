import * as React from 'react';
import styled from 'styled-components';

import { getColor, shadow } from '../../shared/theme';

const StyledButtonsSection = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    padding: 10px;
`;

const Button = styled.div`
    display: flex;
    width: 150px;
    height: 40px;
    justify-content: center;
    align-items: center;
    font-size: 18px;
    border-radius: 24px;
    color: ${getColor('main')};
    ${shadow()}
    cursor: pointer;
`;

export const ButtonsSection: React.FC = () => (
    <StyledButtonsSection>
        <Button>Send message</Button>
        <Button>Call</Button>
        <Button>Change status</Button>
    </StyledButtonsSection>
);
