import * as React from 'react';
import styled from 'styled-components';
import { ChevronDown } from 'react-feather';

import { getColor } from '../../shared/theme';

const StyledNavigation = styled.div`
    display: flex;
    padding-left: 40px;
    flex-direction: column;
    justify-content: space-between;
`;

const ArrowUp = styled.div`
    transform: rotate(180deg);
    color: ${getColor('mainLight')};
    cursor: pointer;
`;

const ArrowDown = styled.div`
    color: ${getColor('main')};
    cursor: pointer;
`;

type Props = {
    scrollUp: () => void;
    scrollDown: () => void;
}

export const Navigation: React.FC<Props> = ({ scrollUp, scrollDown }) => (
    <StyledNavigation>
        <ArrowUp onClick={scrollUp}><ChevronDown size={35} /></ArrowUp>
        <ArrowDown onClick={scrollDown}><ChevronDown size={35} /></ArrowDown>
    </StyledNavigation>
);
