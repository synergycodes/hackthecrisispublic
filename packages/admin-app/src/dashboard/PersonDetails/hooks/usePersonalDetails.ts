import * as _ from 'lodash/fp';

import {
    PatientExtended,
    useApplicationQuery,
    useApplicationsQuery
} from '../../../entries/dal/useAplicationsQuery';
import { Symptoms } from '../../../shared/types/diseaseTypes';
import { Application } from '../../../shared/types/application';

export const getSymptoms = (app: PatientExtended): Symptoms[] =>
    _.filter(_.identity, [
        app.symptoms.coughing && Symptoms.COUGHING,
        app.symptoms.dyspoena && Symptoms.DYSPNOEA,
    ]) as Symptoms[];

const mapApplications = (data: PatientExtended[]): Application[] =>
    data.map((person) => ({
        name: `${person.name} ${person.surname}`,
        id: String(person.id),
        date: new Date(person.newestFormState.createDate),
        status: person.status,
        temperature: Number(person.symptoms.lastBodyTemperature),
        category: person.category,
        age: person.age,
        city: 'Wroclaw',
        symptoms: getSymptoms(person),
        phone: person.phone,
        email: person.email
    }));

export const usePersonalDetails = (): Application[] => {
    const { data } = useApplicationsQuery();

    if (!data)
        return null;

    const applications = mapApplications(data);

    return _.orderBy<Application>((x) => x.date.getTime())
    ('desc')(applications);
};

// todo: remove any
const mapApplication = (data: PatientExtended): Application | any => ({
    name: `${data.name} ${data.surname}`,
    id: String(data.id),
    date: new Date(data.newestFormState.createDate),
    status: data.status,
    temperature: Number(data.symptoms.lastBodyTemperature),
    category: data.category,
    age: data.age,
    city: 'Wroclaw',
    symptoms: getSymptoms(data),
    phone: data.phone || '+48 123 456 789',
    email: data.email || 'patient@mail.com',
    isolationTime: '5 days',
    firstSymptoms: '18.03.2020',
    lastActivity: '21.03.2020',
    additionalInformation: 'Extended description of the symptoms '
            + 'added by the patient',
});

export const usePersonDetails = (id: string): Application => {
    const { data } = useApplicationQuery(parseInt(id, 10));

    if (!data)
        return null;

    return mapApplication(data);
};
