import * as React from 'react';
import { Card } from '@material-ui/core';
import styled from 'styled-components';

import { getSize } from '../../shared/theme';
import { DetailsCardContent } from './DetailsCardContent';
import { Navigation } from './Navigation';
import { usePersonalDetails } from './hooks/usePersonalDetails';

const Container = styled.div`
    margin-top: ${getSize('cardsMargin')}px;
    flex: 1.85;
    display: flex;
    justify-content: space-between;
    height: 100%;

    .MuiPaper-root {
        border-radius: ${getSize('cardBorderRadius')}px;
    }
`;

const DetailsCard = styled(Card)`
    display: flex;
    width: 100%;
    padding: 35px 40px 37px 50px;
    justify-content: space-between;
    height: 230px;
`;

export const PersonDetails: React.FC = () => {
    const [slideIndex, setSlideIndex] = React.useState(0);
    const personalDetails = usePersonalDetails();

    if (!personalDetails) {
        return (
            <Container>
                <DetailsCard square={true}>
                    There are no new applications
                </DetailsCard>
            </Container>
        );
    }

    const scrollUp = () =>
        setSlideIndex(Math.max(slideIndex - 1, 0));

    const scrollDown = () =>
        setSlideIndex(Math.min(slideIndex + 1, personalDetails.length - 1));

    return (
        <Container>
            <DetailsCard square={true}>
                <DetailsCardContent
                    personalDetails={personalDetails}
                    slideIndex={slideIndex}
                    onChangeIndex={setSlideIndex}
                />
                <Navigation
                    scrollUp={scrollUp}
                    scrollDown={scrollDown}
                />
            </DetailsCard>
        </Container>
    );
};
