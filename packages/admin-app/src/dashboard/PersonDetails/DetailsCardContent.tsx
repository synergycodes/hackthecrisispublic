import * as React from 'react';
import styled from 'styled-components';
import SwipeableViews from 'react-swipeable-views';

import { BasicPersonDetails } from '../../shared/components/BasicPersonDetails/BasicPersonDetails';
import { PersonDetailsEntry } from '../../shared/components/PersonDetailsEntry/PersonDetailsEntry';
import { SymptomsTable } from '../../shared/components/SymptomsTable/SymptomsTable';
import { DetailsButton } from './DetailsButton';
import { Application } from '../../shared/types/application';
import { ButtonsSection } from './ButtonsSection';

type Props = {
    personalDetails: Application[];
    slideIndex: number;
    onChangeIndex: (index: number) => void;
};

const Container = styled.div`
    display: flex;
    width: 100%;
    flex-wrap: wrap;
    justify-content: space-between;
`;

const MiddleColumn = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    padding-left: 30px;
`;

const StyledBasicDetails = styled(BasicPersonDetails)`
    padding-right: 30px;
    div:nth-child(1) {
        font-size: 30px;
    }

    div:nth-child(2) {
        font-size: 25px;
    }
`;

const StyledSymptomsTable = styled(SymptomsTable)`
    margin: 0;
    height: 100%;
`;

export const StyledScroller = styled(SwipeableViews)`
    flex: 1;
`;

const StyledSlide = {
    height: '100%'
};

const StyledSwipeableRoot = {
    height: '100%'
};

const pad = (v: number) => String(v).padStart(2, '0');
const formatTime = (date: Date) =>
    `${pad(date.getHours())}:${pad(date.getMinutes())}`;

export const DetailsCardContent: React.FC<Props> = ({
    personalDetails, slideIndex, onChangeIndex
}) => (
    <StyledScroller
        containerStyle={StyledSlide}
        slideStyle={StyledSwipeableRoot}
        resistance
        axis='y'
        index={slideIndex}
        onChangeIndex={onChangeIndex}
    >
        {personalDetails.map((details) => (
            <Container key={details.id}>
                <StyledBasicDetails
                    name={details.name}
                    age={details.age}
                    email={details.email}
                    phone={details.phone}
                >
                    <DetailsButton id={details.id} />
                </StyledBasicDetails>
                <MiddleColumn>
                    <PersonDetailsEntry
                        label='Status'
                        content={details.status}
                    />
                    <PersonDetailsEntry
                        label='Category'
                        content={details.category}
                    />
                    <PersonDetailsEntry
                        label='Time'
                        content={formatTime(details.date)}
                    />
                </MiddleColumn>
                <StyledSymptomsTable
                    temperature={details.temperature}
                    symptoms={details.symptoms}
                    shouldShowLabels={false}
                />
                <ButtonsSection />
            </Container>
        ))}
    </StyledScroller>
);
