import { PatientsSummary, usePatientsSummaryQuery } from '../../../entries/dal/usePatientsSummaryQuery';

export const usePatientsSummary = (): PatientsSummary => {
    const { data } = usePatientsSummaryQuery();

    if (!data)
        return ({
            infected: 0,
            quarantine: 0,
            recovered: 0
        });

    return data;
};
