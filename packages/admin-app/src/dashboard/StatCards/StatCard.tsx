import * as React from 'react';
import { Paper } from '@material-ui/core';
import styled from 'styled-components';

import { getColor, getSize, shadow } from '../../shared/theme';

const StyledCard = styled(Paper)`
    &.MuiPaper-rounded {
        border-radius: 24px;
        ${shadow()}
    }
    box-sizing: border-box;
    flex: 1;
    :not(:last-of-type) {
        margin-right: ${getSize('cardsMargin')}px;
    }
    display: flex;
    justify-content: flex-start;
    min-width: 160px;
    min-height: fit-content;
    height: 100%;
`;

type IconContainerProps = {
    color: Parameters<typeof getColor>[0]
};

const ICON_CONTAINER_WIDTH = 98;
const ICON_CONTAINER_HEIGHT = 71;

const IconContainer = styled.div<IconContainerProps>`
    transform: translate(-16px, -16px);
    display: flex;
    justify-content: center;
    align-items: center;
    background: ${({ color }) => getColor(color)};
    min-width: ${ICON_CONTAINER_WIDTH}px;
    width: ${ICON_CONTAINER_WIDTH}px;
    min-height: ${ICON_CONTAINER_HEIGHT}px;
    height: ${ICON_CONTAINER_HEIGHT}px;
    border-radius: 16px;
    margin-right: 16px;
`;

const StatsContainer = styled.div`
    display: flex;
    justify-content: flex-start;
    flex-direction: column;
    align-self: center;
`;

const Value = styled.div`
    text-overflow: ellipsis;
    font-size: 25px;
    font-weight: bold;
`;

const Label = styled(Value)`
    margin-bottom: 8px;
    font-size: 25px;
    color: ${getColor('main')};
`;

type Props = {
    icon: string;
    label: string;
    value: number;
} & IconContainerProps;

export const StatCard: React.FC<Props> = ({
    icon,
    label,
    value,
    color
}) => (
    <StyledCard>
        <IconContainer color={color}>
            <img src={icon} alt={'icon'} />
        </IconContainer>
        <StatsContainer>
            <Label>{ label }</Label>
            <Value>{ value }</Value>
        </StatsContainer>
    </StyledCard>
);
