import * as React from 'react';
import styled from 'styled-components';

import PlusIcon from '../../shared/assets/images/PlusIcon.svg';
import HomeIcon from '../../shared/assets/images/HomeIcon.svg';
import VirusIcon from '../../shared/assets/images/VirusIcon.svg';
import { StatCard } from './StatCard';
import { usePatientsSummary } from './hooks/usePatientsSummary';

const Container = styled.div`
    flex: 0.65;
    display: flex;
    justify-content: space-between;
`;

export const StatCards: React.FC = () => {
    const patientsSummary = usePatientsSummary();

    return (
        <Container>
            <StatCard
                icon={VirusIcon}
                label={'Infected'}
                value={patientsSummary.infected}
                color={'secondary'} />
            <StatCard
                icon={HomeIcon}
                label={'Quarantine'}
                value={patientsSummary.quarantine}
                color={'mainLight'} />
            <StatCard
                icon={PlusIcon}
                label={'Recovered'}
                value={patientsSummary.recovered}
                color={'tertiary'} />
        </Container>
    );
};
