import * as React from 'react';
import styled from 'styled-components';
import { Card } from '@material-ui/core';

import { getSize, getColor } from '../../shared/theme';
import { ALERTS } from './mockAlerts';
import { AlertsTable } from './AlertsTable';

const Container = styled.div`
    margin-top: ${getSize('cardsMargin')}px;
    flex: 2;
    display: flex;
    justify-content: space-between;
    height: 100%;

    .MuiPaper-root {
        border-radius: ${getSize('cardBorderRadius')}px;
    }
`;

const Title = styled.div`
    padding-bottom: 5px;
    font-size: 28px;
    color: ${getColor('main')};
`;

const AlertsCard = styled(Card)`
    width: 100%;
    padding: 27px 50px;
    font-size: ${getSize('baseFontSize')}px;
`;

export const Alerts: React.FC = () => (
    <Container>
        <AlertsCard square={true}>
            <Title>Notifications</Title>
            <AlertsTable data={ALERTS.slice(0, 5)} />
        </AlertsCard>
    </Container>
);
