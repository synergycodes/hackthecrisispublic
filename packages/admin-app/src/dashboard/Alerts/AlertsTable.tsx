import * as React from 'react';
import styled from 'styled-components';

import { AlertsData } from './mockAlerts';
import { getColor } from '../../shared/theme';

type Props = {
    data: AlertsData;
}

const Table = styled.table`
    width: 100%;
    height: 80%;
`;

const HeaderEntry = styled.th`
    text-align: left;
    font-weight: 500;
    color: ${getColor('mainLighter')};
`;

const Row = styled.tr`
    font-size: 20px;
`;

const Button = styled.td`
    font-weight: 600;
    color: ${getColor('main')};
    cursor: pointer;
`;

const Name = styled.td`
    font-weight: 600;
`;

export const AlertsTable: React.FC<Props> = ({ data }) => (
    <Table>
        <thead>
            <tr>
                <HeaderEntry>Name</HeaderEntry>
                <HeaderEntry>Time</HeaderEntry>
                <HeaderEntry>Date</HeaderEntry>
                <HeaderEntry>Description</HeaderEntry>
            </tr>
        </thead>
        <tbody>
            {data.map((alert) => (
                <Row key={alert.id}>
                    <Name>{alert.name}</Name>
                    <td>09:34 am</td>
                    <td>{alert.date}</td>
                    <td>{alert.description}</td>
                    <Button>View</Button>
                </Row>
            ))}
        </tbody>
    </Table>
);
