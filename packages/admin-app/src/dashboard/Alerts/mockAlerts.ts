export const ALERTS = [
    {
        id: '1',
        name: 'Anna Kowalska',
        date: '20/03/2020',
        description: 'High temperature'
    },
    {
        id: '2',
        name: 'Anna Kowalska',
        date: '20/03/2020',
        description: 'Category change'
    },
    {
        id: '3',
        name: 'Anna Kowalska',
        date: '20/03/2020',
        description: 'New symptoms'
    },
    {
        id: '4',
        name: 'Anna Kowalska',
        date: '20/03/2020',
        description: 'High temperature'
    },
    {
        id: '5',
        name: 'Anna Kowalska',
        date: '20/03/2020',
        description: 'High temperature'
    },
    {
        id: '6',
        name: 'Anna Kowalska',
        date: '20/03/2020',
        description: 'High temperature'
    }
];

export type AlertsData = typeof ALERTS;
