import * as React from 'react';
import styled from 'styled-components';
import { Paper } from '@material-ui/core';
import { ChevronDown, Minus, Plus } from 'react-feather';

import MapImage from '../../shared/assets/mocks/dashboard-map.png';
import { getColor, shadow, theme } from '../../shared/theme';

const Container = styled(Paper)`
    &.MuiPaper-rounded {
        border-radius: 24px;
    }
    position: relative;
    padding: 16px;
    flex: 3;
    display: flex;
    justify-content: center;
    align-items: center;
    height: 80%;
`;

const Map = styled.img`
    height: 100%;
    width: 100%;
`;

const CityButton = styled.div`
    position: absolute;
    bottom: 50px;
    left: 50px;
    display: flex;
    width: 240px;
    height: 60px;
    padding: 0 30px;
    justify-content: space-between;
    align-items: center;
    background: #fff;
    border-radius: 19px;
    ${shadow()}
    font-size: 30px;
    color: ${getColor('main')};
    font-weight: 700;
    cursor: pointer
`;

const commonZoomButtonStyles = `
    display: flex;
    width: 56px;
    height: 56px;
    justify-content: center;
    align-items: center;
    background: #fff;
    border-radius: 40px;
    ${shadow()}
`;

const ZoomButtons = styled.div`
    position: absolute;
    bottom: 50px;
    right: 50px;
    cursor: pointer;
`;

const ZoomInButton = styled.div`
    ${commonZoomButtonStyles}
    margin-bottom: 20px;
`;

const ZoomOutButton = styled.div`
    ${commonZoomButtonStyles}
    line-height: 9px;
`;

export const RegionMap: React.FC = () => (
    <Container>
        <Map src={MapImage} />
        <CityButton>Wrocław<ChevronDown size={35} /></CityButton>
        <ZoomButtons>
            <ZoomInButton>
                <Plus size={30} color={theme.colors.main} />
            </ZoomInButton>
            <ZoomOutButton>
                <Minus size={30} color={theme.colors.main} />
            </ZoomOutButton>
        </ZoomButtons>
    </Container>
);
