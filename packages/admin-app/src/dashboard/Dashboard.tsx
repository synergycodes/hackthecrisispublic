import * as React from 'react';
import styled from 'styled-components';

import { StatCards } from './StatCards/StatCards';
import { PersonDetails } from './PersonDetails/PersonDetails';
import { RegionMap } from './RegionMap/RegionMap';
import { Alerts } from './Alerts/Alerts';
import { getSize } from '../shared/theme';

const Container = styled.div`
    width: 100%;
    margin: 64px;
    display: flex;
    justify-content: stretch;
    align-items: stretch;

    @media(max-width: 1024px) {
        flex-direction: column;
    }
`;

const LeftColumn = styled.div`
    flex: 5;
    display: flex;
    flex-direction: column;
    align-items: stretch;
    justify-content: space-between;
`;

const RightColumn = styled(LeftColumn)`
    flex: 3;
    margin-left: ${getSize('cardsMargin')}px;

    @media(max-width: 1024px) {
        margin-left: 0;
        margin-top: ${getSize('cardsMargin')}px;
    }
`;

export const Dashboard: React.FC = () => (
    <Container>
        <LeftColumn>
            <StatCards />
            <PersonDetails />
            <Alerts />
        </LeftColumn>
        <RightColumn>
            <RegionMap />
        </RightColumn>
    </Container>
);
