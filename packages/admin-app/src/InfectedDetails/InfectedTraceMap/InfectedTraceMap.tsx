import * as React from 'react';
import styled from 'styled-components';
import { Paper } from '@material-ui/core';

import MapImage from '../../shared/assets/mocks/map-infected-new.png';
import { getSize } from '../../shared/theme';

const Container = styled(Paper)`
    margin-top: ${getSize('cardsMargin')}px;
    padding: 16px;
    flex: 3;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    height: 100%;

    &.MuiPaper-root {
        border-radius: ${getSize('cardBorderRadius')}px;
    }
`;

const Map = styled.img`
    border-radius: 24px;
    height: 350px;
    width: 436px;
    position: relative;
    top: -20px;
`;

const Address = styled.div`
    width: 90%;
    font-weight: bold;
    font-size: 16pt;
`;

export const InfectedTraceMap: React.FC = () => (
    <Container>
        <Map src={MapImage}/>
        <Address>
            ul. Piłsudskiego 120<br/>
            50-020 Wrocław
        </Address>
    </Container>
);
