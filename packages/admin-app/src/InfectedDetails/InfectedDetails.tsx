import * as React from 'react';
import styled from 'styled-components';
import { useParams } from 'react-router-dom';

import { getSize } from '../shared/theme';
import { MedicalHistory } from './MedicalHistory/MedicalHistory';
import { InfectedTraceMap } from './InfectedTraceMap/InfectedTraceMap';
import { InfectedActions } from './InfectedActions/InfectedActions';
import { PersonDetails } from './PersonDetails/PersonDetails';
import { usePersonDetails } from '../dashboard/PersonDetails/hooks/usePersonalDetails';

// todo: extract components below to shared
const Container = styled.div`
    width: 100%;
    margin: 64px;
    display: flex;
    justify-content: stretch;
    align-items: stretch;

    @media(max-width: 1024px) {
        flex-direction: column;
    }
`;

const LeftColumn = styled.div`
    flex: 6;
    display: flex;
    flex-direction: column;
    align-items: stretch;
    justify-content: space-between;
`;

const RightColumn = styled(LeftColumn)`
    flex: 3;
    margin-left: ${getSize('cardsMargin')}px;

    @media(max-width: 1024px) {
        margin-left: 0;
        margin-top: ${getSize('cardsMargin')}px;
    }
`;

export const InfectedDetails: React.FC = () => {
    const { id } = useParams();
    const personalDetails = usePersonDetails(id);

    if (!personalDetails){
        return null;
    }

    return (
        <Container>
            <LeftColumn>
                <PersonDetails personalDetails={personalDetails}/>
                <MedicalHistory personalDetails={personalDetails}/>
            </LeftColumn>
            <RightColumn>
                <InfectedTraceMap/>
                <InfectedActions/>
            </RightColumn>
        </Container>
    );
};
