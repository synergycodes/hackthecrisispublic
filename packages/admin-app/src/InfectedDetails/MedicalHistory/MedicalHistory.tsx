import * as React from 'react';
import { Card } from '@material-ui/core';
import styled from 'styled-components';

import { getColor, getSize } from '../../shared/theme';
import { SymptomsTable } from '../../shared/components/SymptomsTable/SymptomsTable';
import { Application } from '../../shared/types/application';
import TemperatureGraphImage
    from '../../shared/assets/mocks/temperatureGraph.svg';

// todo: remove any
type Props = {
    personalDetails: Application | any;
};

const Container = styled.div`
    margin-top: ${getSize('cardsMargin')}px;
    flex: 9;
    display: flex;
    justify-content: space-between;
    height: 100%;

    .MuiPaper-root {
        border-radius: ${getSize('cardBorderRadius')}px;
    }
`;

const Title = styled.div`
    padding-bottom: 5px;
    font-size: 30px;
    color: ${getColor('main')};
`;

const Subtitle = styled.div`
    padding-bottom: 5px;
    font-size: 25px;
    color: ${getColor('mainLight')};
`;

const MedicalHistoryCard = styled(Card)`
    width: 100%;
    padding: 20px 50px;
    font-size: ${getSize('baseFontSize')}px;
`;

const DetailsCard = styled(Card)`
    display: flex;
    width: 100%;
    padding: 30px 20px 20px 30px;
    justify-content: space-between;
`;

const MiddleRow = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
`;

const SymptomsTableContainer = styled.div`
    width: 40%;
`;

const AdditionalInformationContainer = styled.div`
    width: 60%;

    div {
        font-size: 20px;
        margin-bottom: 24px;
    }

    textarea {
        width: 90%;
        height: 50%;
        font-size: 18px;
        padding: 16px;
        border-radius: 13px;
        border-color: ${getColor('mainLighter')};
        resize: none;
        outline: none;
        background-color: ${getColor('foreground')};
    }
`;

const MedicalHistorySymptomsTable = styled(SymptomsTable)`
    margin-left: 0;

    tr {
        td:first-child {
            padding-left: 0;
        }
    }
`;

const TemperatureGraph = styled.img`
    padding-top: 15px;
`;


export const MedicalHistory: React.FC<Props> = ({ personalDetails }) => {
    if (!personalDetails) {
        return (
            <Container>
                <DetailsCard square={true}>
                    There is no medical history
                </DetailsCard>
            </Container>
        );
    }

    return (
        <Container>
            <MedicalHistoryCard square={true}>
                <Title>Medical History</Title>
                <Subtitle>18.03.2020</Subtitle>
                <MiddleRow>
                    <SymptomsTableContainer>
                        <MedicalHistorySymptomsTable
                            temperature={personalDetails.temperature}
                            symptoms={personalDetails.symptoms}
                        />
                    </SymptomsTableContainer>
                    <AdditionalInformationContainer>
                        <div>Additional Information:</div>
                        <textarea
                            disabled
                            defaultValue={personalDetails.additionalInformation}
                        ></textarea>
                    </AdditionalInformationContainer>
                </MiddleRow>
                <TemperatureGraph src={TemperatureGraphImage}/>
            </MedicalHistoryCard>
        </Container>
    );
};
