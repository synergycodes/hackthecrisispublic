import * as React from 'react';
import { Card } from '@material-ui/core';
import styled from 'styled-components';

import { getSize } from '../../shared/theme';
import { DetailsCardContent } from './DetailsCardContent';
import { Application } from '../../shared/types/application';

type Props = {
    personalDetails: Application;
};

const Container = styled.div`
    margin-top: ${getSize('cardsMargin')}px;
    flex: 1;
    display: flex;
    justify-content: space-between;
    height: 100%;

    .MuiPaper-root {
        border-radius: ${getSize('cardBorderRadius')}px;
        overflow: inherit;
    }
`;

const DetailsCard = styled(Card)`
    display: flex;
    width: 100%;
    padding: 30px;
    justify-content: space-between;
`;

export const PersonDetails: React.FC<Props> = ({ personalDetails }) => {
    if (!personalDetails) {
        return (
            <Container>
                <DetailsCard square={true}>
                    There is no details to show
                </DetailsCard>
            </Container>
        );
    }

    return (
        <Container>
            <DetailsCard square={true}>
                <DetailsCardContent personalDetails={personalDetails}/>
            </DetailsCard>
        </Container>
    );
};
