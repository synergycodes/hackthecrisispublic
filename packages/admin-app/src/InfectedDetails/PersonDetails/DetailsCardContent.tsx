import * as React from 'react';
import styled from 'styled-components';

import { BasicPersonDetails } from '../../shared/components/BasicPersonDetails/BasicPersonDetails';
import { Application } from '../../shared/types/application';
import { PersonDetailsEntry } from '../../shared/components/PersonDetailsEntry/PersonDetailsEntry';
import { getColor } from '../../shared/theme';

// todo: remove any
type Props = {
    personalDetails: Application | any;
};

const Container = styled.div`
    display: flex;
    position: relative;
    justify-content: space-between;
    flex-wrap: wrap;
    width: 100%;
    padding-right: 25%;
`;

const EntriesColumn = styled.div`
    padding: 0 20px;
`;

const QuarantineButton = styled.div`
    position: absolute;
    bottom: -50px;
    right: -10px;
    height: 48px;
    width: 196px;
    border-radius: 25px;
    justify-content: center;
    text-align: center;
    box-shadow: 0px 3px 12px #00000029;
    padding: 5px 30px 0px 30px;
    margin-right: 10px;
    font-size: 30px;
    color: #fff;
    background: ${getColor('main')};
    cursor: pointer;
`;

export const DetailsCardContent: React.FC<Props> = ({ personalDetails }) => (
    <Container>
        <BasicPersonDetails
            name={personalDetails.name}
            age={personalDetails.age}
            phone={personalDetails.phone}
            email={personalDetails.email}
        />
        <EntriesColumn>
            <PersonDetailsEntry
                label='Status'
                content={personalDetails.status}
            />
            <PersonDetailsEntry
                label='Isolation Time'
                content={personalDetails.isolationTime}
            />
            <PersonDetailsEntry
                label='First Symptoms'
                content={personalDetails.firstSymptoms}
            />
        </EntriesColumn>
        <EntriesColumn>
            <PersonDetailsEntry
                label='Category'
                content={personalDetails.category}
            />
            <PersonDetailsEntry
                label='ID'
                content={personalDetails.id}
            />
            <PersonDetailsEntry
                label='Last Activity'
                content={personalDetails.lastActivity}
            />
        </EntriesColumn>
        <QuarantineButton>Quarantine</QuarantineButton>
    </Container>
);
