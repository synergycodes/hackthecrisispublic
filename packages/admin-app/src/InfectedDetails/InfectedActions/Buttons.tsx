import * as React from 'react';
import styled from 'styled-components';

import { getColor, shadow } from '../../shared/theme';
import mailIcon from '../../shared/assets/images/mailWhite.svg';
import zapIcon from '../../shared/assets/images/zap.svg';
import plusIcon from '../../shared/assets/images/plusWhite.svg';

const StyledButtons = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    height: 100%;
    padding-bottom: 40px;
    justify-content: space-between;
    align-items: center;
`;

const commonStyles = `
    display: flex;
    height: 78px;
    width: 90%;
    font-size: 30px;
    justify-content: center;
    align-items: center;
    border-radius: 19px;
    ${shadow()}
    color: #fff;
    cursor: pointer;
    text-align: left;
    justify-content: space-between;
`;

const SendMessageButton = styled.div`
    ${commonStyles};
    padding: 0 30px;
    background: ${getColor('mainLight')};
`;

const SendActionRequestButton = styled.div`
    ${commonStyles};
    padding: 0 30px;
    background: ${getColor('secondary')};
`;

const SendAmbulanceButton = styled.div`
    ${commonStyles};
    padding: 0 30px;
    background: ${getColor('tertiary')};
`;

export const Buttons: React.FC = () => (
    <StyledButtons>
        <SendMessageButton>
            <div>Send message</div>
            <img src={mailIcon} alt="mail"/>
        </SendMessageButton>
        <SendActionRequestButton>
            <div>Send request for action</div>
            <img src={zapIcon} alt="zap"/>
        </SendActionRequestButton>
        <SendAmbulanceButton>
            <div>Send an ambulance</div>
            <img src={plusIcon} alt="plus"/>
        </SendAmbulanceButton>
    </StyledButtons>
);
