import * as React from 'react';
import styled from 'styled-components';

import { getSize } from '../../shared/theme';
import { Buttons } from './Buttons';

const Container = styled.div`
    margin-top: ${getSize('cardsMargin')}px;
    flex: 1.75;
    display: flex;
    justify-content: space-between;
    height: 100%;
`;

export const InfectedActions: React.FC = () => (
    <Container>
        <Buttons/>
    </Container>
);
