import * as React from 'react';
import _ from 'lodash/fp';
import styled from 'styled-components';
import { RequestTimeRange } from '@hackthecrisis/common/src';

import { Container, Title } from '../entries/Entries';
import { InfectedTable } from './InfectedTable/InfectedTable';
import { ActionBar } from './ActionBar/ActionBar';
import { Search } from './ActionBar/Search';
import { PatientInfo } from './types';
import { Filters } from '../shared/components/Filters/Filters';
import {
    Container as FilterContainer
} from '../shared/components/Filters/FilterSingle';
import { useInfectedDetails } from './InfectedTable/useInfectedDetails';

const filterResults = (data: PatientInfo[], searchString: string) =>
    _.filter<PatientInfo>((x) =>
        _.flowRight(
            _.some((value) => (''+value).toLowerCase()
                .includes(searchString.toLowerCase())),
            _.values
        )(x))(data);

const FiltersContainer = styled.div`
    flex: 1 1;
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    ${FilterContainer} {
        margin-left: 50px;
    }
`;

export const Infected: React.FC = () => {
    const [searchString, setSearch] = React.useState('');

    const infectedData = useInfectedDetails();

    const data = filterResults(infectedData, searchString);

    return (
        <Container>
            <Title>Supported Applications</Title>
            <ActionBar>
                <Search updateSearch={setSearch} />
                <FiltersContainer>
                    <Filters filters={{
                        ageRanges: [],
                        categories: [],
                        statuses: [],
                        timeRange: RequestTimeRange.Today
                    }}/>
                </FiltersContainer>
            </ActionBar>
            <InfectedTable data={data} />
        </Container>
    );
};
