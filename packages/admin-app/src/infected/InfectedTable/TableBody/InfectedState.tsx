import * as React from 'react';
import styled from 'styled-components';

import AlertIcon from '../../assets/alert.svg';
import WarningIcon from '../../assets/warning.svg';
import InfoIcon from '../../assets/info.svg';
import { getColor } from '../../../shared/theme';
import { PatientState } from '../../types';

const Container = styled.div`
    display: flex;
`;

const Text = styled.span<{ state: PatientState }>`
    color: ${({ state }) => {
        if (state === 'Worse') {
            return getColor('accentRed');
        } else if (state === 'No change') {
            return getColor('accentOrange');
        } else {
            return getColor('accentGreen');
        }
    }};
    margin-left: 11px;
`;

type Props = {
    state: PatientState;
};

const stateIconMapping = {
    Worse: AlertIcon,
    'No change': WarningIcon,
    Improved: InfoIcon
};

export const InfectedState: React.FC<Props> = ({ state }) =>
    <Container>
        <img src={stateIconMapping[state]} />
        <Text state={state}>{state}</Text>
    </Container>;
