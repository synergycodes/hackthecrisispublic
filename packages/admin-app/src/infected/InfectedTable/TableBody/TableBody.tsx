import * as React from 'react';
import styled from 'styled-components';
import { TableRow, TableBody as TableBodyBase } from '@material-ui/core';

import { PatientInfo } from '../../types';
import { Circle } from '../../../entries/ApplicationsList/DiseaseCategory';
import { InfectedState } from './InfectedState';
import { getElapsedTimeText } from '../utils/elapsedTime';
import { Link } from '../../../entries/ApplicationsList/DetailsNavigation';
import { Cell } from '../Cell';

const NameCell = styled(Cell)`
    font-size: 23px !important;
    font-weight: bold !important;
`;

export const SmallDiseaseCategory = styled(Circle)`
    font-size: 16px;
    width: 41px;
    height: 41px;
    padding: 0em;
    display: flex;
    align-items: center;
    justify-content: center;
`;

type Props = {
    data: PatientInfo[];
}

export const TableBody: React.FC<Props> = ({ data }) =>
    <TableBodyBase>
        {data.map(({
            category, name, lastActivity, city,
            age, applicationId, status, state
        }) =>
            <TableRow key={applicationId}>
                <Cell>
                    <SmallDiseaseCategory category={category}>
                        {category}
                    </SmallDiseaseCategory>
                </Cell>
                <NameCell>{name}</NameCell>
                <Cell>
                    {getElapsedTimeText(new Date(lastActivity))}
                </Cell>
                <Cell>{city}</Cell>
                <Cell>{age}</Cell>
                <Cell>{applicationId}</Cell>
                <Cell>{status}</Cell>
                <Cell>
                    <InfectedState state={state} />
                </Cell>
                <Cell>
                    <Link to={`/infected/${applicationId}`}>
                        Details
                    </Link>
                </Cell>
            </TableRow>)}
    </TableBodyBase>;
