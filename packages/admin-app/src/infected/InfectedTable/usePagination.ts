import * as React from 'react';
import _ from 'lodash/fp';

import { PatientInfo } from '../types';

export const usePagination = (data: PatientInfo[]) => {
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(7);
    const availableRowsPerPage = [7, 14, 21];

    const pagedData = _.flowRight(
        _.take(rowsPerPage),
        _.drop(page * rowsPerPage)
    )(data);

    return {
        page,
        setPage,
        rowsPerPage,
        setRowsPerPage,
        availableRowsPerPage,
        pagedData
    };
};
