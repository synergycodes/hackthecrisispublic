import { formatDistance } from 'date-fns/fp';
import _ from 'lodash/fp';

const formatElapsedTime = (elapsedText: string) =>
    `${elapsedText} ago`;

export const getElapsedTimeText = _.flowRight(
    formatElapsedTime,
    formatDistance(new Date())
);
