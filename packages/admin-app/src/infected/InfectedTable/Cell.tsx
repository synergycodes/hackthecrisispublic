import styled from 'styled-components';
import { TableCell } from '@material-ui/core';

import { getColor } from '../../shared/theme';

export const Cell = styled(TableCell)`
    font-family: inherit !important;
    font-size: 18px !important;
    color: ${getColor('black')} !important;
`;
