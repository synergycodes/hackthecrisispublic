import * as React from 'react';
import _ from 'lodash/fp';

import { PatientInfo } from '../types';

export const useSorting = (data: PatientInfo[]) => {
    const [
        sortedProperty,
        setSortedProperty] = React.useState<keyof PatientInfo>('lastActivity');
    const [isAscending, setIsAscending] = React.useState(false);

    const sortDirection = isAscending ? 'asc' : 'desc';

    let sortedData = _.orderBy<PatientInfo>(
        (item: PatientInfo) => sortedProperty === 'lastActivity'
            ? new Date(item[sortedProperty]).getTime()
            : item[sortedProperty]
    )(sortDirection)(data);

    if (sortedProperty === 'lastActivity') {
        sortedData = sortedData.reverse();
    }

    const toggleOrder = () =>
        setIsAscending(!isAscending);

    const sortBy = (property: keyof PatientInfo) => {
        property !== sortedProperty && setSortedProperty(property);
        property === sortedProperty && toggleOrder();
    };

    return {
        sortBy,
        sortedProperty,
        isAscending,
        sortedData: sortedProperty ? sortedData : data
    };
};
