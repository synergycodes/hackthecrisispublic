import * as React from 'react';
import styled from 'styled-components';
import { TableContainer as BaseContainer, Table, TablePagination } from '@material-ui/core';

import { SectionContainer as BaseSectionContainer } from '../../shared/components/SectionContainer';
import { getColor } from '../../shared/theme';
import { TableHead } from './TableHead/TableHead';
import { SmallDiseaseCategory, TableBody } from './TableBody/TableBody';
import { usePagination } from './usePagination';
import { Cell } from './Cell';
import { PatientInfo } from '../types';
import { useSorting } from './useSorting';

const TableContainer = styled(BaseContainer)`
    box-sizing: border-box;
    overflow: hidden;
`;

const SectionContainer = styled(BaseSectionContainer)`
    display: flex;
    flex-direction: column;
    padding: 25px 50px 25px 20px;
`;

const StyledTable = styled(Table)`
    box-sizing: border-box;

    ${Cell} {
        border-bottom: 1px solid ${getColor('mainLighter')};
    }

    ${SmallDiseaseCategory} {
        margin: 0 auto;
        flex: none;
    }
`;

type Props = {
    data: PatientInfo[];
};

export const InfectedTable: React.FC<Props> = ({ data }) => {
    const {
        sortedData,
        ...rest
    } = useSorting(data);

    const {
        page,
        setPage,
        rowsPerPage,
        setRowsPerPage,
        availableRowsPerPage,
        pagedData
    } = usePagination(sortedData);

    const onChangeRowsPerPage = (e: React.ChangeEvent<HTMLInputElement>) => {
        setRowsPerPage(parseInt(e.target.value));
        setPage(0);
    };

    return (
        <SectionContainer>
            <TableContainer>
                <StyledTable>
                    <TableHead {...rest}/>
                    <TableBody data={pagedData} />
                </StyledTable>
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={availableRowsPerPage}
                component='div'
                count={data.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onChangePage={(e, newPage) => setPage(newPage)}
                onChangeRowsPerPage={onChangeRowsPerPage}
            />
        </SectionContainer>
    );
};
