import { PatientExtended, useApplicationsQuery } from '../../entries/dal/useAplicationsQuery';
import { PatientInfo } from '../types';

const statusMapping = {
    'New application': 'Waiting for test results',
    Infected: 'Infected',
    Healthy: 'In quarantine'
};

const stateMapping = {
    1: 'Improved',
    2: 'Worse',
    3: 'No change'
};

const getMockedState = (() => {
    const savedState = new Map<number, number>();

    return (id: number) => {
        !savedState.has(id)
            && savedState.set(id, Math.floor(Math.random() * 3) + 1);
        return stateMapping[savedState.get(id)];
    };
})();

const mapApplications = (data: PatientExtended[]): PatientInfo[] =>
    data.map((x: PatientExtended) => ({
        name: `${x.name} ${x.surname}`,
        city: 'Wroclaw',
        age: x.age,
        applicationId: x.id,
        status: statusMapping[x.status],
        lastActivity: x.newestFormState.createDate.toString(),
        category: x.category,
        state: getMockedState(x.id)
    }));

export const useInfectedDetails = (): PatientInfo[] => {
    const { data } = useApplicationsQuery();

    if (!data)
        return null;

    return mapApplications(data);
};
