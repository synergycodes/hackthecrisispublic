import * as React from 'react';
import styled from 'styled-components';
import { TableHead as TableHeadBase, TableRow } from '@material-ui/core';

import { getColor } from '../../../shared/theme';
import { ActionableHeader } from './ActionableHeader';
import { Cell } from '../Cell';
import { PatientInfo } from '../../types';

const HeaderCell = styled(Cell)`
    font-size: 23px !important;
    color: ${getColor('mainLight')} !important;
`;

type Props = {
    sortedProperty: keyof PatientInfo,
    sortBy: (x: keyof PatientInfo) => void,
    isAscending: boolean,
};

export const TableHead: React.FC<Props> = ({
    sortedProperty,
    sortBy,
    isAscending
}) =>
    <TableHeadBase>
        <TableRow>
            <HeaderCell></HeaderCell>
            <HeaderCell>
                <ActionableHeader
                    text='Name'
                    onClick={() => sortBy('name')}
                />
            </HeaderCell>
            <HeaderCell>
                <ActionableHeader
                    text='Last activity'
                    onClick={() => sortBy('lastActivity')}
                />
            </HeaderCell>
            <HeaderCell>
                <ActionableHeader
                    text='City'
                    onClick={() => sortBy('city')}
                />
            </HeaderCell>
            <HeaderCell>
                <ActionableHeader
                    text='Age'
                    onClick={() => sortBy('age')}
                />
            </HeaderCell>
            <HeaderCell>
                <ActionableHeader
                    text='Application ID'
                    onClick={() => sortBy('applicationId')}
                />
            </HeaderCell>
            <HeaderCell>
                <ActionableHeader
                    text='Status'
                    onClick={() => sortBy('status')}
                />
            </HeaderCell>
            <HeaderCell>
                <ActionableHeader
                    text='State'
                    onClick={() => sortBy('state')}
                />
            </HeaderCell>
            <HeaderCell></HeaderCell>
        </TableRow>
    </TableHeadBase>;
