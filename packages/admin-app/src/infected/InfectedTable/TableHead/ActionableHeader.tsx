import * as React from 'react';
import styled from 'styled-components';

import { SortingSymbol } from './SortingSymbol';

const Container = styled.div`
    display: flex;
    align-items: center;
    cursor: pointer;
`;

type Props = {
    text: string;
    onClick: () => void;
}

export const ActionableHeader: React.FC<Props> = ({ text, onClick }) =>
    <Container onClick={onClick}>
        {text}
        <SortingSymbol />
    </Container>;
