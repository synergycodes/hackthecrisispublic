import * as React from 'react';
import styled from 'styled-components';

import TriangleUpIcon from '../../assets/triangleUp.svg';
import TriangleDownIcon from '../../assets/triangleDown.svg';

const Container = styled.div`
    display: flex;
    flex-direction: column;
    margin-left: 11px;
`;

const FirstImage = styled.img`
    margin-bottom: 4px;
`;

export const SortingSymbol: React.FC = () =>
    <Container>
        <FirstImage src={TriangleUpIcon}/>
        <img src={TriangleDownIcon}/>
    </Container>;
