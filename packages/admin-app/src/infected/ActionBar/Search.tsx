import * as React from 'react';
import styled from 'styled-components';
import { TextField, InputAdornment } from '@material-ui/core';
import _ from 'lodash/fp';

import { SectionContainer } from '../../shared/components/SectionContainer';
import SearchIcon from '../assets/search.svg';

const SearchContainer = styled(SectionContainer)`
    padding: 10px;
    width: 30%;
`;

const TextInput = styled(TextField)`
    width: 100%;
    margin-left: 10px !important;
    margin-right: 10px !important;
    font-family: inherit !important;
`;

type Props = {
    updateSearch: (text: string) => void;
}

const debouncedUpdateSearch = _.throttle(500, (text, updateSearch) =>
    updateSearch(text));

export const Search: React.FC<Props> = ({ updateSearch }) => {
    const [text, setText] = React.useState('');

    const invalidateSearch = (text: string) => {
        setText(text);
        debouncedUpdateSearch(text, updateSearch);
    };

    return (
        <SearchContainer>
            <TextInput
                value={text}
                onChange={(e) => invalidateSearch(e.target.value)}
                placeholder='Search'
                InputProps={{
                    startAdornment: (
                        <InputAdornment position='start'>
                            <img src={SearchIcon} />
                        </InputAdornment>
                    ),
                    disableUnderline: true
                }}
            />
        </SearchContainer>
    );
};
