import styled from 'styled-components';

export const ActionBar = styled.div`
    display: flex;
    flex-direction: row;
    margin-bottom: 30px;
`;
