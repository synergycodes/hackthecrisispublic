export type PatientState = 'Worse' | 'Improved' | 'No change';

export type PatientInfo = {
    category: 'A' | 'B' | 'C';
    name: string;
    lastActivity: string;
    city: string;
    age: number;
    applicationId: number;
    status: string;
    state: PatientState;
}
