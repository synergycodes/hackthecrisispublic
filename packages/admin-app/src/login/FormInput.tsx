import * as React from 'react';
import { FormControl, InputLabel, Input } from '@material-ui/core';

type Props = {
    label: string;
    id: string;
    value: string;
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
    type?: string;
}

export const FormInput: React.FC<Props> = ({
    label, id, value, onChange, type
}) =>
    <FormControl>
        <InputLabel htmlFor={id}>{label}</InputLabel>
        <Input
            id={id}
            value={value}
            onChange={onChange}
            type={type || 'text'}
        />
    </FormControl>;
