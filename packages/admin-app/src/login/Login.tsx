import * as React from 'react';
import styled from 'styled-components';

import { LoginForm } from './LoginForm';
import { getColor, shadow } from '../shared/theme';
import { LoginBackground } from '../shared/assets/mocks/LoginBackground';

const Container = styled.div`
    height: 100vh;
    width: 100%;
    background-repeat: no-repeat;
    overflow: hidden;
`;

const Content = styled.div`
    display: flex;
    height: 650px;
    margin-top: 40px;
    padding: 68px 92px 78px 92px;
    flex-direction: column;
    justify-content: center;
    background: #fff;
    box-sizing: border-box;
    ${shadow()}
    border-radius: 41px;
    position: absolute;
    top: 45%;
    left: 57%;
    transform: translate(-50%, -50%);

    .MuiInputBase-root {
        padding-bottom: 7px;
    }

    .MuiInput-underline:before {
        border-bottom: 1px solid ${getColor('mainLight')};
    }

    .MuiInput-underline:hover:before {
        border-bottom: 2px solid ${getColor('main')} !important;
    }

    .MuiSelect-icon {
        color: ${getColor('main')};
    }
`;

const Title = styled.div`
    font-size: 60px;
    color: ${getColor('main')};
`;

const Description = styled.div`
    font-size: 25px;
    color: ${getColor('mainLight')};
`;

export const Login: React.FC = () => (
    <Container>
        <LoginBackground/>
        <Content>
            <Title>Welcome</Title>
            <Description>Please, login to your account</Description>
            <LoginForm />
        </Content>
    </Container>
);
