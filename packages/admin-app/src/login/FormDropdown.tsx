import * as React from 'react';
import { FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';

type Props = {
    label: string;
    id: string;
    value: string;
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
    options: { id: string, name: string }[]
};

export const FormDropdown: React.FC<Props> = ({
    label, id, value, onChange, options
}) =>
    <FormControl>
        <InputLabel id={id}>{label}</InputLabel>
        <Select
            labelId={id}
            value={value}
            onChange={onChange}
        >
            {options.map(({ id: optionValue, name }) =>
                <MenuItem value={optionValue} key={id}>{name}</MenuItem>)
            }
        </Select>
    </FormControl>;
