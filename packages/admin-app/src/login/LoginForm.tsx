import * as React from 'react';
import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

import { FormInput } from './FormInput';
import { FormDropdown } from './FormDropdown';
import { PROVINCES } from './constants';
import { getColor, shadow } from '../shared/theme';
import { DASHBOARD_PATH } from '../shared/consts/navigation';

const Form = styled.form`
    display: flex;
    width: 400px;
    flex-direction: column;
    align-items: center;

    .MuiFormControl-root {
        width: 100%;
        margin-top: 20px;
    }

    .MuiButton-root {
        margin-top: 40px;
        width: 200px;
    }

    .MuiList-root {
        max-width: 400px;
    }
`;

const Button = styled(NavLink)`
    display: flex;
    height: 80px;
    margin-top: 60px;
    width: 300px;
    font-size: 38px;
    justify-content: center;
    align-items: center;
    color: #fff;
    background: ${getColor('secondary')};
    ${shadow()}
    border-radius: 19px;
    cursor: pointer;
    text-decoration: none;
`;

export const LoginForm: React.FC = () => {
    const [name, setName] = React.useState<string>('');
    const [surname, setSurname] = React.useState<string>('');
    const [password, setPassword] = React.useState<string>('');
    const [province, setProvince] = React.useState<string>('');

    return (
        <Form noValidate autoComplete='off'>
            <FormInput
                label='Name'
                id='name'
                value={name}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                    setName(e.target.value)}
            />
            <FormInput
                label='Surname'
                id='Surname'
                value={surname}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                    setSurname(e.target.value)}
            />
            <FormInput
                label='Password'
                id='password'
                value={password}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                    setPassword(e.target.value)}
                type='password'
            />
            <FormDropdown
                label='Province'
                id='province'
                value={province}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                    setProvince(e.target.value)}
                options={PROVINCES}
            />
            <Button to={DASHBOARD_PATH}>
                Log in
            </Button>
        </Form>
    );
};
