import * as React from 'react';

import { RoutedContentProvider } from './shared/providers/RoutedContentProvider';
import { MaterialThemeProvider } from './shared/providers/MaterialThemeProvider';
import { LazyStoreProvider } from './shared/providers/LazyStoreProvider';
import { RouterProvider } from './shared/providers/RouterProvider';
import { ApolloProvider } from './shared/providers/ApolloProvider';
import { StoreProvider } from './shared/providers/StoreProvider';
import { ThemeProvider } from './shared/providers/ThemeProvider';

const Loader: React.FC = () => (
    <div>
        Loading async component ...
    </div>
);

export const App: React.FC = () => (
    <ThemeProvider>
        <MaterialThemeProvider>
            <ApolloProvider>
                <StoreProvider>
                    <RouterProvider loader={<Loader />}>
                        <LazyStoreProvider>
                            <RoutedContentProvider />
                        </LazyStoreProvider>
                    </RouterProvider>
                </StoreProvider>
            </ApolloProvider>
        </MaterialThemeProvider>
    </ThemeProvider>
);
