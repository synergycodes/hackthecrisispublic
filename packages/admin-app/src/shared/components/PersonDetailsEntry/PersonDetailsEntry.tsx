import * as React from 'react';
import styled from 'styled-components';

import { getColor, getSize } from '../../theme';

type Props = {
    label: string;
    content: string;
}

const Entry = styled.div`
    margin-bottom: 10px;
    font-size: ${getSize('baseFontSize')}px;
`;

const Label = styled.div`
    color: ${getColor('mainLight')}
`;

export const PersonDetailsEntry: React.FC<Props> = ({ label, content }) => (
    <Entry>
        <Label>{`${label}:`}</Label>
        <div>{content}</div>
    </Entry>
);
