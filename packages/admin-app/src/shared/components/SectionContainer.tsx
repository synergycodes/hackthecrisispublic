import styled from 'styled-components';

import { getColor, shadow } from '../theme';

export const SectionContainer = styled.div`
    background-color: ${getColor('foreground')};
    ${shadow()}
    border-radius: 30px;
    padding: 30px 50px 30px 20px;
    display: flex;
    flex-direction: row;
`;
