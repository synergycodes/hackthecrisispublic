import * as React from 'react';
import styled from 'styled-components';

import { MenuBar } from '../MenuBar/MenuBar';

const Container = styled.div`
    height: 100vh;
    width: 100vw;
    display: flex;
    justify-content: stretch;
`;

export const Shell: React.FC = ({ children }) => (
    <Container>
        <MenuBar />
        { children }
    </Container>
);
