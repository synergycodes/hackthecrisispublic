import styled from 'styled-components';

import { getColor } from '../../theme';

export const Circle = styled.div`
    border-radius: 50%;
    position: relative;
    width: 55px;
    height: 55px;
    background: ${getColor('mainLight')};
    margin-bottom: 32px;

    :after {
        content: 'E.';
        position: absolute;
        left: calc(50% - 8px);
        top: calc(50% - 6px);
        line-height: 48px;
        margin: 0;
        padding: 0;
        font-size: 48px;
        font-weight: bold;
        color: white;
    }
`;
