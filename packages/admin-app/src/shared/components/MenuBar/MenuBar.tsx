import * as React from 'react';
import styled from 'styled-components';
import { LogOut, Home, Users, Bell, Settings } from 'react-feather';
import { Avatar } from '@material-ui/core';

import adminPhoto from '../../assets/images/admin-photo.png';
import { Item } from './Item';
import {
    DASHBOARD_PATH,
    ENTRIES_PATH,
    INFECTED_PATH,
    LOGIN_PATH
} from '../../consts/navigation';
import { getColor, getSize } from '../../theme';
import { Circle } from './Circle';

const MainContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    background: ${getColor('main')};
    border-radius: 8px;
    margin: 8px;
    padding-top: 32px;
    overflow: hidden;
    min-width: ${getSize('menubarItem')}px;
`;

const UpperSection = styled.div`
    width: 100%;
    flex: 1;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
`;

const BottomSection = styled(UpperSection)`
    justify-content: flex-end;
    > * {
        margin: 32px 0 32px 0;
    }
`;

export const MenuBar: React.FC = () => (
    <MainContainer>
        <UpperSection>
            <Circle />
            <Item icon={Home} path={DASHBOARD_PATH} />
            <Item icon={Bell} path={ENTRIES_PATH} />
            <Item icon={Users} path={INFECTED_PATH} />
            <Item icon={LogOut} path={LOGIN_PATH} />
        </UpperSection>
        <BottomSection>
            <Avatar alt={'Admin'} src={adminPhoto} />
            <Settings color={'#ffffffaa'} size={26} />
        </BottomSection>
    </MainContainer>
);
