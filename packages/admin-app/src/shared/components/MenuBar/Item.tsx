import * as React from 'react';
import { Icon } from 'react-feather';
import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

import MenuItemShape from '../../assets/images/menuItemShape.svg';
import { getColor, getSize } from '../../theme';

type Props = {
    icon: Icon;
    path: string;
};

const activeClassName = 'active';
const Container = styled(NavLink).attrs({
    activeClassName
})`
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    width: ${getSize('menubarItem')}px;
    height: ${getSize('menubarItem')}px;
    z-index: 0;
    border-radius: 50%;
    transition: background-color 500ms ease;
    margin: 16px 0 16px 0;

    > svg {
        transition: 100ms ease-in stroke;
    }

    :hover :not(.${activeClassName}) {
        background: #ffffff17;
    }

    :after {
        z-index: -1;
        mask: url(${MenuItemShape});
        content: '';
        background: ${getColor('background')};
        right: -10px;
        top: -18px;
        position: absolute;
        width: calc(2 * ${getSize('menubarItem')}px - 8px);
        height: calc(${getSize('menubarItem')}px + 2 * 18px);
        transition: transform 300ms ease;
        transform: translate(50%) scale(0);
    }

    &.${activeClassName} {
        > svg {
            stroke: ${getColor('main')};
        }
        :after {
            transform: translate(calc(50% - 10px)) scale(1);
        }
    }
`;

export const Item: React.FC<Props> = ({
    icon: Icon,
    path
}) => (
    <Container to={path}>
        <Icon color={'white'} size={26} />
    </Container>
);
