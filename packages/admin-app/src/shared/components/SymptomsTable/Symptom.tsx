import * as React from 'react';
import styled from 'styled-components';

import tempUrl from '../../assets/images/tempWithCircle.svg';
import coughingUrl from '../../assets/images/coughingWithCircle.svg';
import dyspnoeaUrl from '../../assets/images/lungsWithCircle.svg';
import { Symptoms } from '../../types/diseaseTypes';

const SymptomImage = {
    [Symptoms.TEMPERATURE]: tempUrl,
    [Symptoms.COUGHING]: coughingUrl,
    [Symptoms.DYSPNOEA]: dyspnoeaUrl
};

const Label = {
    [Symptoms.TEMPERATURE]: 'Temperature',
    [Symptoms.COUGHING]: 'Cough',
    [Symptoms.DYSPNOEA]: 'Shortness of breath'
};

type SymptomProps = {
    value: Symptoms;
    children: any;
    shouldShowLabel: boolean;
};

const StyledSymptom = styled.tr`
    font-size: 15px;

    td {
        max-width: 84px;
        padding-left: 15px;
    }
`;

const Icon = styled.td`
    text-align: right;
`;

export const Symptom: React.FC<SymptomProps> = ({
    value, children, shouldShowLabel
}) => (
    <StyledSymptom>
        <Icon><img alt={value} src={SymptomImage[value]} /></Icon>
        {shouldShowLabel && <td>{Label[value]}</td>}
        {children}
    </StyledSymptom>
);
