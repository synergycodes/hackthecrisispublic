import * as React from 'react';
import styled from 'styled-components';

import { Symptoms } from '../../types/diseaseTypes';
import { Symptom } from './Symptom';
import { getColor } from '../../theme';

type Props = {
    temperature: number;
    symptoms: Symptoms[];
    shouldShowLabels?: boolean;
}

const Table = styled.table`
    margin-left: 20px;
    flex-grow: 1;
`;

const PositiveInfo = styled.td`
    font-size: 24px;
    color: ${getColor('accentGreen')};
`;

const NegativeInfo = styled.td`
    font-size: 24px;
    color: ${getColor('accentRed')};
`;

export const SymptomsTable: React.FC<Props> = ({
    temperature,
    symptoms,
    shouldShowLabels = true,
    ...rest
}) => {
    const hasSymptom = (symptom: Symptoms) =>
        symptoms.includes(symptom);

    const tempInfo = <NegativeInfo>{temperature} <sup>o</sup>C</NegativeInfo>;
    const coughingInfo = hasSymptom(Symptoms.COUGHING)
        ? <NegativeInfo>YES</NegativeInfo>
        : <PositiveInfo>No</PositiveInfo>;
    const dyspnoeaInfo = hasSymptom(Symptoms.DYSPNOEA)
        ? <NegativeInfo>YES</NegativeInfo>
        : <PositiveInfo>No</PositiveInfo>;

    return (
        <Table {...rest}>
            <tbody>
                <Symptom
                    value={Symptoms.TEMPERATURE}
                    shouldShowLabel={shouldShowLabels}
                >
                    {tempInfo}
                </Symptom>
                <Symptom
                    value={Symptoms.COUGHING}
                    shouldShowLabel={shouldShowLabels}
                >
                    {coughingInfo}
                </Symptom>
                <Symptom
                    value={Symptoms.DYSPNOEA}
                    shouldShowLabel={shouldShowLabels}
                >
                    {dyspnoeaInfo}
                </Symptom>
            </tbody>
        </Table>
    );
};
