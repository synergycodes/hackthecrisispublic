import * as React from 'react';

import tempUrl from '../../shared/assets/images/tempWithCircle.svg';
import coughingUrl from '../../shared/assets/images/coughingWithCircle.svg';
import dyspnoeaUrl from '../../shared/assets/images/lungsWithCircle.svg';
import { Symptoms } from '../../types/diseaseTypes';

const SymptomImage = {
    [Symptoms.TEMPERATURE]: tempUrl,
    [Symptoms.COUGHING]: coughingUrl,
    [Symptoms.DYSPNOEA]: dyspnoeaUrl
};

type SymptomProps = {
    value: Symptoms
};

export const Symptom: React.FC<SymptomProps> = ({ value }) => (
    <img alt={value} src={SymptomImage[value]} />
);
