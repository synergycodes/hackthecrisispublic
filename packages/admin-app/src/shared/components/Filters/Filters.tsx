import * as React from 'react';
import { useDispatch } from 'react-redux';
import {
    PatientAgeRange,
    PatientCategory,
    PatientStatus
} from '@hackthecrisis/common/src';
import { PatientProfileFilterInput } from '@hackthecrisis/apollo/src/modules/patientProfile/patientProfileFilter.type';

import { FilterSingle } from './FilterSingle';
import {
    setAgeFilter,
    setCategoryFilter,
    setStatusFilter
} from '../../../entries/actions/filters';

type FiltersProps = {
    filters: PatientProfileFilterInput
};

export const Filters: React.FC<FiltersProps> = ({
    filters
}) => {
    const dispatch = useDispatch();

    return (
        <>
            <FilterSingle
                title='Category'
                value={filters.categories[0]}
                onChange={(value) => {
                    if (value === 'All')
                        return dispatch(setCategoryFilter());

                    dispatch(setCategoryFilter(value as PatientCategory));
                }}
                options={[
                    { name: PatientCategory.A, value: PatientCategory.A },
                    { name: PatientCategory.B, value: PatientCategory.B },
                    { name: PatientCategory.C, value: PatientCategory.C }
                ]}/>
            <FilterSingle
                title='Age'
                value={filters.ageRanges[0]}
                onChange={(value) => {
                    if (value === 'All')
                        return dispatch(setAgeFilter());

                    dispatch(setAgeFilter(value as PatientAgeRange));
                }}
                options={[
                    { name: '0 - 10', value: PatientAgeRange.From1To10 },
                    { name: '11 - 20', value: PatientAgeRange.From11To20 },
                    { name: '21 - 30', value: PatientAgeRange.From21To30 },
                    { name: '31 - 40', value: PatientAgeRange.From31To40 },
                    { name: '41 - 50', value: PatientAgeRange.From41To50 },
                    { name: '51 - 60', value: PatientAgeRange.From51To60 },
                    { name: '61 - 70', value: PatientAgeRange.From61To70 },
                    { name: '71 - 80', value: PatientAgeRange.From71To80 },
                    { name: '81 - 90', value: PatientAgeRange.From81To90 },
                    { name: '91 - 100', value: PatientAgeRange.From91To100 },
                ]}/>
            <FilterSingle
                title='Status'
                value={filters.statuses.length > 1
                    ? undefined
                    : filters.statuses[0]
                }
                onChange={(value) => {
                    if (value === 'All')
                        return dispatch(setStatusFilter());

                    dispatch(setStatusFilter(value as PatientStatus));
                }}
                options={[
                    {
                        name: 'New application',
                        value: PatientStatus.NewApplication
                    },
                    { name: 'Healthy', value: PatientStatus.Healthy },
                    {
                        name: 'In quarantine',
                        value: PatientStatus.InQuarantine
                    },
                ]}/>
        </>
    );
};
