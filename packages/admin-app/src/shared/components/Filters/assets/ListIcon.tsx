import * as React from 'react';

export const ListIcon = () => (
    <svg width={20} height={14} viewBox="0 0 20 14">
        <defs>
            <style>
                {
                    // eslint-disable-next-line max-len
                    '.prefix__a{fill:none;stroke:#fff;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px}'
                }
            </style>
        </defs>
        <path
            className="prefix__a"
            d="M6 1h13M6 7h13M6 13h13M1 1h.01M1 7h.01M1 13h.01"
        />
    </svg>
);
