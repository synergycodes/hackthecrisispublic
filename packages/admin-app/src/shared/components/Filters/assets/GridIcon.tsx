import * as React from 'react';

export const GridIcon = () => (
    <svg width={20} height={20} viewBox="0 0 20 20">
        <defs>
            <style>
                {
                    // eslint-disable-next-line max-len
                    '.prefix__a{fill:none;stroke:#04a2bb;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px}'
                }
            </style>
        </defs>
        <path
            className="prefix__a"
            d="M1 1h7v7H1zM12 1h7v7h-7zM12 12h7v7h-7zM1 12h7v7H1z"
        />
    </svg>
);
