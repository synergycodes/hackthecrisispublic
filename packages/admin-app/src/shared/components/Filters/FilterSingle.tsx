import * as React from 'react';
import styled from 'styled-components';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

import { getColor, shadow } from '../../theme';

const Title = styled.div`
    color: ${getColor('main')};
    font-size: 23px;
`;

const Dropdown = styled(Select)`
    width: 200px;
    height: 47px;
    background-color: ${getColor('foreground')};
    border-radius: 40px;
    border: 0;
    ${shadow()}

    &:before {
        border-radius: 40px;
        border: 0 !important;
    }

    &:after {
        display: none;
    }

    .MuiSelect-select {
        border-radius: 40px;
        border: 0;
        font-family: 'Nunito', sans-serif;
        color: ${getColor('main')};
        font-size: 23px;
        padding-left: 1em;
        padding-right: 1em;

        :focus {
            background-color: transparent;
        }
    }

    .MuiSelect-icon {
        color: ${getColor('main')};
    }
`;

const DropdownOption = styled(MenuItem)`
    color: ${getColor('main')} !important;
    font-family: 'Nunito', sans-serif !important;
    font-size: 23px !important;
`;

export const Container = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;

    ${Dropdown} {
        margin-left: 10px;
    }
`;

type Option = {
    name: string;
    value: string;
}

type Props = {
    title: string;
    options: Option[],
    value?: string;
    onChange?: (val: string) => void;
};

const DEFAULT_VALUE = {
    name: 'All',
    value: 'All'
};

export const FilterSingle: React.FC<Props> = ({
    title, options, value, onChange
}) => (
    <Container>
        <Title>{title}:</Title>
        <Dropdown
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={value || DEFAULT_VALUE.value}
            onChange={({ target: { value } }) => onChange(value as string)}
            defaultValue={DEFAULT_VALUE.value}>
            {[DEFAULT_VALUE].concat(options).map(({ name, value }) => (
                <DropdownOption
                    key={value}
                    value={value}>{name}</DropdownOption>
            ))}
        </Dropdown>
    </Container>
);
