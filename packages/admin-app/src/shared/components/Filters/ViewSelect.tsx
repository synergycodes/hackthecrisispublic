import * as React from 'react';
import styled from 'styled-components';

import { getColor } from '../../theme';
import { ListIcon } from './assets/ListIcon';
import { GridIcon } from './assets/GridIcon';

const Container = styled.div`
    flex: 1 1;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: flex-end;
`;

const Label = styled.div`
    color: ${getColor('main')};
    font-size: 23px;
    font-weight: bold;
`;

const ViewPillContainer = styled.div<{ active: boolean }>`
    width: 48px;
    height: 48px;
    margin-left: 16px;
    background-color: ${({ active, ...rest }) =>
        active
            ? getColor('main')(rest)
            : getColor('mainLighter')(rest)
};
    border-radius: 20px;
    display: flex;
    align-items: center;
    justify-content: center;

    > svg .prefix__a {
        stroke: ${({ active, ...rest }) =>
        active
            ? getColor('foreground')(rest)
            : getColor('main')(rest)
};
    }
`;

type ViewPillProps = {
    active?: boolean;
};
const ViewPill: React.FC<ViewPillProps> = ({
    active = false, children
}) => (
    <ViewPillContainer active={active}>
        {children}
    </ViewPillContainer>
);

export const ViewSelect = () => (
    <Container>
        <Label>View:</Label>
        <ViewPill
            active>
            <ListIcon />
        </ViewPill>
        <ViewPill>
            <GridIcon />
        </ViewPill>
    </Container>
);
