import * as React from 'react';
import styled from 'styled-components';

import { getColor, getSize } from '../../theme';
import phoneIcon from '../../assets/images/phone.svg';
import emailIcon from '../../assets/images/mail.svg';

type Props = {
    name: string;
    age: number;
    phone: string;
    email: string;
    children?: any;
};

const StyledBasicDetails = styled.div`
    display: flex;
    flex-direction: column;
    padding-right: 20px;
    border-right: 1px solid ${getColor('mainLighter')};
`;

const MainContent = styled.div`
    flex-grow: 1;
`;

const Name = styled.div`
    font-size: 40px;
    color: ${getColor('main')};
`;

const Age = styled.div`
    margin-bottom: 20px;
    font-size: 30px;
`;

const ContactData = styled.div`
    display: flex;
    margin-bottom: 15px;
    font-size: ${getSize('baseFontSize')}px;

    img {
        margin-right: 10px;
    }
`;

export const BasicPersonDetails: React.FC<Props> = ({
    name, age, phone, email, children, ...rest
}) => (
    <StyledBasicDetails {...rest}>
        <MainContent>
            <Name>{name}</Name>
            <Age>{`${age} years`}</Age>
            {phone && (
                <ContactData>
                    <img src={phoneIcon} alt='phone' />
                    {phone}
                </ContactData>
            )}
            {email && (
                <ContactData>
                    <img src={emailIcon} alt='email' />
                    {email}
                </ContactData>
            )}
        </MainContent>
        {children}
    </StyledBasicDetails>
);
