import * as React from 'react';

export const useEventListener = (
    type: string,
    listener: EventListener,
    target: EventTarget = window
) => {
    React.useEffect(() => {
        target.addEventListener(type, listener);
        return () => target.removeEventListener(type, listener);
    }, [type, listener, target]);
};
