import * as React from 'react';

type ClickOutsideCallback = (event: MouseEvent) => void;

export const useClickOutside = (callback: ClickOutsideCallback) => {
    const ref = React.useRef(null);

    React.useEffect(() => {
        const windowClickListener = (event: MouseEvent) => {
            if (!ref.current) {
                return;
            }

            if (!ref.current?.contains(event.target)) {
                callback(event);
            }
        };
        window.addEventListener('click', windowClickListener);

        return () => window.removeEventListener('click', windowClickListener);
    });

    return {
        ref
    };
};
