import * as React from 'react';

export const useInterval = (callback, interval) => {
    const savedCallback = React.useRef(callback);

    React.useEffect(() => {
        savedCallback.current = callback;
    }, [callback]);

    React.useEffect(() => {
        const tick = () => {
            savedCallback.current();
        };

        const intervalId = setInterval(tick, interval);
        return () => clearInterval(intervalId);
    }, [interval]);
};
