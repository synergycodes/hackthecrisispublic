import * as React from 'react';
import * as _ from 'lodash/fp';

export const useUniqueId = (prefix: string) => React.useMemo(
    () => _.uniqueId(prefix),
    [prefix]
);
