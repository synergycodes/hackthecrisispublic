import * as React from 'react';

export type OnChangeEvent = React.ChangeEvent<{ value: string, name: string }>;

export const useForm = <T extends {}>(initialFormData: T): [
    T,
    (event: OnChangeEvent) => void
] => {
    const [formData, setState] = React.useState<T>(initialFormData);

    const onChange = ({ target }: OnChangeEvent) => {
        const { value, name } = target;
        setState((prev) => ({
            ...prev,
            [name]: value
        }));
    };

    return [
        formData,
        onChange
    ];
};
