import { combineReducers, compose } from 'redux';
import * as _ from 'lodash/fp';

export const rootReducer = {};

export const createReducer = (asyncReducer: {} = {}) => {
    // HACK: This code is to prevent Rx from throwing errors, hence we have
    //       no global store, and Rx Rx doesn't accept empty reducers.
    const nextAsync = _.isEmpty(asyncReducer)
        ? {
            dummyReducer: (state: any = null) => state
        }
        : asyncReducer;

    return combineReducers({
        ...rootReducer,
        ...nextAsync
    });
};

export const composeEnhancers
    = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const reducers = new Map<string, {}>();
export const registerReducer = (key: string, reducer: {}) =>
    reducers.set(key, reducer);

export const getAsyncReducers = (keys: string[]) => {
    const asyncReducer = _.flowRight(
        _.reduce((prev, current: {}) => ({ ...prev, ...current }), {}),
        _.map((key: string) => reducers.get(key)),
        _.filter((key: string) => reducers.has(key))
    )(keys);

    return createReducer(asyncReducer);
};
