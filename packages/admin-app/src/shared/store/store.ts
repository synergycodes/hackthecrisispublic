import { applyMiddleware, createStore } from 'redux';

import { epicMiddleware, rootEpic, getAsyncEpics, epic$ } from './epic';
import { composeEnhancers, createReducer, getAsyncReducers } from './reducer';
import { clearEpics } from './asyncEpics/actions';

export const middleware = [epicMiddleware];

export const store = createStore(
    createReducer(),
    composeEnhancers(
        applyMiddleware(...middleware)
    ),
);

export const loadAsyncStore = (keys: string[]) => {
    loadAsyncReducers(keys);
    loadAsyncEpics(keys);
};

const loadAsyncReducers = (keys: string[]) => {
    const nextReducer = getAsyncReducers(keys);
    store.replaceReducer(nextReducer);
};

const loadAsyncEpics = (keys: string[]) => {
    store.dispatch(clearEpics());

    const nextEpics = getAsyncEpics(keys);
    epic$.next(nextEpics);
};

epicMiddleware.run(rootEpic);
