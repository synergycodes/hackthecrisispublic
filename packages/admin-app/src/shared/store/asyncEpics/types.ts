export const CLEAR_EPICS = '[ASYNC_EPICS] CLEAR_EPICS';
type ClearEpics = {
    type: typeof CLEAR_EPICS
};

export type AsyncEpicsActionTypes =
    | ClearEpics;
