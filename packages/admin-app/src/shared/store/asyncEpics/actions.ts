import { AsyncEpicsActionTypes, CLEAR_EPICS } from './types';

export const clearEpics = (): AsyncEpicsActionTypes => ({
    type: CLEAR_EPICS
});
