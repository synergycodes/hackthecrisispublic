import {
    ActionsObservable,
    combineEpics,
    createEpicMiddleware,
    Epic,
    StateObservable,
    ofType
} from 'redux-observable';
import { BehaviorSubject } from 'rxjs';
import { mergeMap, takeUntil } from 'rxjs/operators';
import * as _ from 'lodash/fp';

import { RootAction, RootState } from './types';
import { CLEAR_EPICS } from './asyncEpics/types';

const rootEpics = [];

const epics = new Map<string, Epic[]>();
export const registerEpics = (key: string, newEpics: Epic[]) =>
    epics.set(key, newEpics);

export const getAsyncEpics = (keys: string[]) => {
    const asyncEpics = _.flowRight(
        _.flatten,
        _.map((key: string) => epics.get(key)),
        _.filter((key: string) => epics.has(key))
    )(keys);

    return combineEpics(
        ...asyncEpics
    ) as Epic<RootAction, RootAction>;
};

const rootEpics$ = combineEpics(...rootEpics) as Epic<RootAction, RootAction>;
export const epic$ = new BehaviorSubject(rootEpics$);

export const rootEpic = (
    action$: ActionsObservable<RootAction>,
    state$: StateObservable<RootState>
) =>
    epic$
        .pipe(
            mergeMap((epic: Epic) => epic(action$, state$, {}).pipe(
                takeUntil(action$.pipe(
                    ofType(CLEAR_EPICS)
                ))
            ))
        );

export const epicMiddleware
    = createEpicMiddleware<RootAction, RootAction, RootState>();
