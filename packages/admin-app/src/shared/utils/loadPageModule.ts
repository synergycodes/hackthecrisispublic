import * as React from 'react';
import { Epic } from 'redux-observable';

import { mapLocationToStoreKeys } from './mapLocationToStoreKeys';
import { registerReducer } from '../store/reducer';
import { registerEpics } from '../store/epic';
import { loadAsyncStore } from '../store/store';

type LazyModule = {
    component: React.ComponentType;
    reducers?: {};
    epics?: Epic[];
};

export const loadPageModule = (
    moduleName: string,
    module: LazyModule
) => {
    const asyncReducer = module.reducers || {};
    registerReducer(moduleName, asyncReducer);

    const asyncEpics = module.epics || [];
    registerEpics(moduleName, asyncEpics);

    const keys = mapLocationToStoreKeys(moduleName);
    loadAsyncStore(keys);

    return module.component;
};
