export const isInMaterialPopover = (target: Node) =>
    document.getElementsByClassName('MuiPopover-root')
        ?.item(0)
        ?.contains(target);
