import * as _ from 'lodash/fp';

const pathDelimeter = '/';
const notEmpty = (text: string): boolean => !!text.length;

const joinWithDelimiter = _.curry(
    (delimiter: string, accumulator: string[], curr: string) => {
        const prev = _.last(accumulator) || '';
        const next = prev + delimiter + curr;

        return [
            ...accumulator,
            next
        ];
    }
);

export const mapLocationToStoreKeys = _.flowRight(
    _.reduce(joinWithDelimiter(pathDelimeter), []),
    _.filter(notEmpty),
    _.split(pathDelimeter)
);
