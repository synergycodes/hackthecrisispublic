import { PatientStatus } from '@hackthecrisis/common';

import { Category, Symptoms } from './diseaseTypes';

export type Application = {
    category: Category;
    date: Date;
    city: string;
    temperature: number;
    symptoms: Symptoms[];
    age: number;
    id: string;
    name: string;
    status: PatientStatus;
    phone: string;
    email: string;
};
