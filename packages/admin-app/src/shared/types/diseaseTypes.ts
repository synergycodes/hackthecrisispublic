export enum Symptoms {
    TEMPERATURE = 'TEMPERATURE',
    COUGHING = 'COUGHING',
    DYSPNOEA = 'DYSPNOEA'
};

export type Category = 'A' | 'B' | 'C';
