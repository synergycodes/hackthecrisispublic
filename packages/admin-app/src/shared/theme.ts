declare global {
    type Theme = typeof theme;
}

export const theme = {
    colors: {
        main: '#04A2BB',
        mainLight: '#73C2CF',
        mainLighter: '#A0D0D7',
        secondary: '#FEA79C',
        tertiary: '#28519C',
        accentRed: '#BB0446',
        accentGreen: '#24A579',
        accentOrange: '#FF9A2C',
        accentYellow: '#F2C530',
        gray: '#8B8B8B',
        background: '#f3f5f7',
        foreground: '#ffffff',
        black: '#000000'
    },
    sizes: {
        menubarItem: 100,
        cardsMargin: 32,
        baseFontSize: 20,
        cardBorderRadius: 24
    },
    zIndex: {
        dialog: 200,
        menubar: 100
    },
    responsive: {
        lg: 1200,
        md: 992,
        sm: 768
    }
};

export const getFromTheme = (namespace: keyof typeof theme) => (key: string) =>
    ({ theme: providedTheme }) => providedTheme[namespace][key];

export const getSize = (key: keyof typeof theme.sizes) =>
    getFromTheme('sizes')(key);

export const getColor = (key: keyof typeof theme.colors) =>
    getFromTheme('colors')(key);

export const getZIndex = (key: keyof typeof theme.zIndex) =>
    getFromTheme('zIndex')(key);

export const media = (key: keyof typeof theme.responsive) => (value) => {
    const size = getFromTheme('responsive')(key)(value);

    return `@media screen and (min-width: ${size}px)`;
};

export const shadow = () => 'box-shadow: 0px 3px 12px #00000029 !important;';
