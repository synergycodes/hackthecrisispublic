/**
 * Images
 */
declare module '*.png';
declare module '*.jpg';
declare module '*.jpeg';
declare module '*.svg';

/**
 * Styles
 */
declare module '*.css';
