import * as React from 'react';

/* eslint-disable */
export function LoginBackground(props) {
    return (
        <svg width={1254} height={1080} viewBox="0 0 1254 1080" {...props}>
            <defs>
                <filter
                    id="prefix__b"
                    x={-956}
                    y={-472}
                    width={1938}
                    height={1938}
                    filterUnits="userSpaceOnUse"
                >
                    <feOffset dy={3} />
                    <feGaussianBlur stdDeviation={6} result="c" />
                    <feFlood floodOpacity={0.161} />
                    <feComposite operator="in" in2="c" />
                    <feComposite in="SourceGraphic" />
                </filter>
                <filter
                    id="prefix__d"
                    x={-18.146}
                    y={-15.076}
                    width={753.146}
                    height={1117.529}
                    filterUnits="userSpaceOnUse"
                >
                    <feOffset dy={3} />
                    <feGaussianBlur stdDeviation={6} result="e" />
                    <feFlood floodOpacity={0.161} />
                    <feComposite operator="in" in2="e" />
                    <feComposite in="SourceGraphic" />
                </filter>
                <clipPath id="prefix__a">
                    <path stroke="#707070" fill="#fff" d="M0 0h1254v1080H0z" />
                </clipPath>
                <style>
                    {
                        ".prefix__g,.prefix__l,.prefix__r{fill:#fff}.prefix__n{opacity:.2}.prefix__ac{fill:none}.prefix__g{font-size:93px;font-family:&apos;Nunito&apos;,sans-serif;font-weight:700}.prefix__i{fill:#3db6ca}.prefix__j{fill:#e7a350}.prefix__l,.prefix__p{opacity:.6;mix-blend-mode:multiply}.prefix__l,.prefix__n,.prefix__p{isolation:isolate}.prefix__n{mix-blend-mode:screen;fill:#efc087}.prefix__o{opacity:.53}.prefix__p{fill:#04a2bb}.prefix__u{fill:#74cad7}.prefix__y{fill:#b0e1e9}.prefix__ab{stroke:none}"
                    }
                </style>
            </defs>
            <g clipPath="url(#prefix__a)">
                <ellipse
                    cx={1047.5}
                    cy={1048.5}
                    rx={1047.5}
                    ry={1048.5}
                    transform="translate(-998 -644)"
                    opacity={0.2}
                    fill="#02a2bb"
                />
                <g filter="url(#prefix__b)">
                    <circle
                        cx={951}
                        cy={951}
                        r={951}
                        transform="translate(-938 -457)"
                        opacity={0.84}
                        fill="#02a2bb"
                    />
                </g>
                <g
                    transform="translate(-929 -336)"
                    opacity={0.72}
                    stroke="#fff"
                    strokeWidth={2}
                    fill="none"
                >
                    <ellipse
                        className="prefix__ab"
                        cx={890}
                        cy={890.5}
                        rx={890}
                        ry={890.5}
                    />
                    <ellipse
                        className="prefix__ac"
                        cx={890}
                        cy={890.5}
                        rx={889}
                        ry={889.5}
                    />
                </g>
                <g filter="url(#prefix__d)">
                    <path
                        d="M7.782-.076c113.956 0 220.873.215 350.3 0C577.33 113.92 717 332.558 717 597c0 189.156-76.726 360.406-200.864 484.453-180.254-1.285-325.748 0-515.045 0C-1.137 962.893-1.137 327.646 7.782-.076z"
                        fill="#02a2bb"
                    />
                </g>
                <text className="prefix__g" transform="translate(120 553)">
                    <tspan x={0} y={0}>
                        {"Epidemic"}
                    </tspan>
                </text>
                <text
                    transform="translate(420 605)"
                    fontSize={46}
                    fontFamily="'Nunito',sans-serif"
                    fontWeight={700}
                    fill="#fff"
                >
                    <tspan x={0} y={0}>
                        {"App"}
                    </tspan>
                </text>
                <text className="prefix__g" transform="translate(120 553)">
                    <tspan x={0} y={0}>
                        {"Epidemic"}
                    </tspan>
                </text>
                <path
                    className="prefix__i"
                    d="M638.652 330.633h12.06a127.87 127.87 0 010-31.891h-12.06a127.87 127.87 0 010 31.891zM602.063 343.665l10.445-6.03a127.843 127.843 0 01-15.945-27.614l-10.445 6.03a127.916 127.916 0 0115.945 27.614zM576.893 373.248l6.033-10.442a127.988 127.988 0 01-27.617-15.945l-6.027 10.445a128 128 0 0127.611 15.942zM569.888 411.454v-12.061a127.846 127.846 0 01-31.888 0v12.061a127.822 127.822 0 0131.888 0zM582.923 448.041l-6.023-10.442a128.026 128.026 0 01-27.618 15.942l6.03 10.442a127.8 127.8 0 0127.611-15.942zM612.509 473.211l-10.445-6.034a127.853 127.853 0 01-15.942 27.617l10.445 6.027a127.685 127.685 0 0115.942-27.61zM650.712 480.216h-12.061a127.846 127.846 0 010 31.888h12.058a127.8 127.8 0 01.003-31.888zM687.302 467.178l-10.448 6.03a128.05 128.05 0 0115.948 27.614l10.439-6.03a127.749 127.749 0 01-15.939-27.614zM712.472 437.596l-6.036 10.443a127.852 127.852 0 0127.617 15.942l6.03-10.445a127.542 127.542 0 01-27.611-15.94zM719.477 399.39l-.006 12.064a127.846 127.846 0 0131.888 0v-12.061a127.656 127.656 0 01-31.882-.003zM706.439 362.799l6.027 10.451a127.948 127.948 0 0127.617-15.948l-6.03-10.442a127.765 127.765 0 01-27.614 15.939zM676.854 337.628l10.445 6.039a127.905 127.905 0 0115.942-27.617l-10.439-6.03a127.6 127.6 0 01-15.948 27.608z"
                />
                <path
                    className="prefix__j"
                    d="M729.533 405.42a84.854 84.854 0 11-84.852-84.855 84.853 84.853 0 0184.852 84.855z"
                />
                <path
                    d="M729.528 405.42a84.763 84.763 0 01-5.08 29 83.9 83.9 0 01-8 16.275 84.856 84.856 0 11-117.055-117.032 83.89 83.89 0 0116.275-8.018 84.927 84.927 0 01113.86 79.775z"
                    fill="#43b8cb"
                />
                <path
                    className="prefix__l"
                    d="M675.337 459.613a84.953 84.953 0 01-4.205 26.463 84.882 84.882 0 01-107.077-107.112 84.894 84.894 0 01111.282 80.653z"
                />
                <path
                    d="M729.527 405.42a84.763 84.763 0 01-5.08 29 83.9 83.9 0 01-8 16.275 84.858 84.858 0 01-117.054-117.032 83.89 83.89 0 0116.275-8.018 84.927 84.927 0 01113.859 79.775z"
                    style={{
                        mixBlendMode: "screen",
                        isolation: "isolate"
                    }}
                    opacity={0.22}
                    fill="#85c9d4"
                />
                <path
                    className="prefix__n"
                    d="M663.992 356.817a6.54 6.54 0 11-6.54-6.54 6.54 6.54 0 016.54 6.54zM623.346 361.022a11.211 11.211 0 11-11.21-11.213 11.211 11.211 0 0111.21 11.213z"
                />
                <g className="prefix__o" transform="translate(538 298.742)">
                    <path
                        className="prefix__p"
                        d="M174.581 120.676a7.009 7.009 0 11-7.008-7.008 7.007 7.007 0 017.008 7.008zM62.924 101.055A12.614 12.614 0 1150.309 88.44a12.613 12.613 0 0112.615 12.615zM134.403 84.235a9.812 9.812 0 11-9.813-9.81 9.812 9.812 0 019.813 9.81z"
                    />
                    <circle
                        className="prefix__p"
                        cx={5.139}
                        cy={5.139}
                        r={5.139}
                        transform="translate(137.205 151.977)"
                    />
                    <path
                        className="prefix__p"
                        d="M105.794 42.347a4.562 4.562 0 11-4.562-4.565 4.562 4.562 0 014.562 4.565zM165.748 66.794a4.874 4.874 0 11-4.871-4.874 4.869 4.869 0 014.871 4.874z"
                    />
                </g>
                <path
                    className="prefix__n"
                    d="M635.494 451.653a11.213 11.213 0 11-11.213-11.21 11.211 11.211 0 0111.213 11.21zM716.959 382.977a5.314 5.314 0 11-5.314-5.311 5.313 5.313 0 015.314 5.311z"
                />
                <g transform="translate(-38 22)" opacity={0.65}>
                    <circle
                        className="prefix__r"
                        cx={11}
                        cy={11}
                        r={11}
                        transform="translate(672 257)"
                    />
                    <circle
                        className="prefix__r"
                        cx={11}
                        cy={11}
                        r={11}
                        transform="translate(613 273)"
                    />
                    <circle
                        className="prefix__r"
                        cx={11}
                        cy={11}
                        r={11}
                        transform="translate(573 315)"
                    />
                    <circle
                        className="prefix__r"
                        cx={11}
                        cy={11}
                        r={11}
                        transform="translate(557 372)"
                    />
                    <circle
                        className="prefix__r"
                        cx={11}
                        cy={11}
                        r={11}
                        transform="translate(573 429)"
                    />
                    <circle
                        className="prefix__r"
                        cx={11}
                        cy={11}
                        r={11}
                        transform="translate(613 471)"
                    />
                    <circle
                        className="prefix__r"
                        cx={11}
                        cy={11}
                        r={11}
                        transform="translate(672 485)"
                    />
                    <circle
                        className="prefix__r"
                        cx={11}
                        cy={11}
                        r={11}
                        transform="translate(729 471)"
                    />
                    <circle
                        className="prefix__r"
                        cx={11}
                        cy={11}
                        r={11}
                        transform="translate(770 429)"
                    />
                    <circle
                        className="prefix__r"
                        cx={11}
                        cy={11}
                        r={11}
                        transform="translate(785 372)"
                    />
                    <circle
                        className="prefix__r"
                        cx={11}
                        cy={11}
                        r={11}
                        transform="translate(770 315)"
                    />
                    <circle
                        className="prefix__r"
                        cx={11}
                        cy={11}
                        r={11}
                        transform="translate(731 273)"
                    />
                </g>
                <g>
                    <path
                        className="prefix__i"
                        d="M436.935 254.349l5.788 1.552a63.535 63.535 0 014.104-15.306l-5.788-1.551a63.535 63.535 0 01-4.104 15.305zM417.698 255.895l5.79-1.554a63.522 63.522 0 01-4.1-15.305l-5.79 1.554a63.558 63.558 0 014.1 15.305zM401.813 266.853l4.242-4.234a63.593 63.593 0 01-11.202-11.206l-4.237 4.237a63.608 63.608 0 0111.197 11.203zM393.535 284.285l1.552-5.789a63.523 63.523 0 01-15.303-4.103l-1.552 5.788a63.508 63.508 0 0115.303 4.104zM395.087 303.526l-1.554-5.79a63.608 63.608 0 01-15.304 4.098l1.554 5.789a63.498 63.498 0 0115.304-4.097zM406.042 319.412l-4.235-4.242a63.527 63.527 0 01-11.204 11.202l4.236 4.237a63.443 63.443 0 0111.203-11.197zM423.475 327.688l-5.787-1.552a63.523 63.523 0 01-4.103 15.304l5.787 1.551a63.5 63.5 0 014.103-15.303zM442.713 326.14l-5.791 1.553a63.625 63.625 0 014.1 15.305l5.787-1.555a63.475 63.475 0 01-4.096-15.304zM458.6 315.18l-4.242 4.236a63.526 63.526 0 0111.202 11.205l4.242-4.236a63.372 63.372 0 01-11.202-11.204zM466.878 297.747l-1.553 5.789a63.525 63.525 0 0115.304 4.103l1.551-5.787a63.43 63.43 0 01-15.302-4.105zM465.329 278.505l1.548 5.791a63.574 63.574 0 0115.306-4.1l-1.554-5.789a63.482 63.482 0 01-15.3 4.098zM454.369 262.621l4.235 4.242a63.552 63.552 0 0111.204-11.202l-4.232-4.241a63.399 63.399 0 01-11.207 11.201z"
                    />
                    <path
                        className="prefix__j"
                        d="M470.927 301.934a42.161 42.161 0 11-29.803-51.641 42.161 42.161 0 0129.803 51.641z"
                    />
                    <path
                        d="M470.925 301.934a42.116 42.116 0 01-6.17 13.266 41.69 41.69 0 01-5.934 6.782 42.162 42.162 0 11-41.117-71.23 41.682 41.682 0 018.843-1.753 42.197 42.197 0 0144.379 52.936z"
                        fill="#33b2c7"
                    />
                    <path
                        className="prefix__l"
                        d="M437.945 320.969a42.205 42.205 0 01-5.422 12.163 42.175 42.175 0 01-37.607-65.19 42.181 42.181 0 0143.028 53.026z"
                    />
                    <path
                        d="M470.925 301.934a42.116 42.116 0 01-6.17 13.266 41.689 41.689 0 01-5.934 6.782 42.163 42.163 0 01-41.117-71.23 41.683 41.683 0 018.843-1.753 42.197 42.197 0 0144.379 52.936z"
                        style={{
                            mixBlendMode: "screen",
                            isolation: "isolate"
                        }}
                        fill="#abd8df"
                        opacity={0.22}
                    />
                    <path
                        className="prefix__n"
                        d="M445.727 270.175a3.25 3.25 0 11-2.297-3.98 3.25 3.25 0 012.297 3.98zM425.68 266.964a5.57 5.57 0 11-3.938-6.824 5.57 5.57 0 013.937 6.824z"
                    />
                    <g className="prefix__o" transform="rotate(14.98 -743.113 1664.978)">
                        <path
                            className="prefix__p"
                            d="M68.005 41.181a3.482 3.482 0 11-3.481-3.481 3.481 3.481 0 013.481 3.481zM12.533 31.43a6.267 6.267 0 11-6.267-6.267 6.266 6.266 0 016.267 6.267zM48.045 23.078a4.874 4.874 0 11-4.875-4.874 4.875 4.875 0 014.875 4.874z"
                        />
                        <circle
                            className="prefix__p"
                            cx={2.553}
                            cy={2.553}
                            r={2.553}
                            transform="translate(49.437 56.733)"
                        />
                        <path
                            className="prefix__p"
                            d="M33.828 2.264a2.266 2.266 0 11-2.266-2.268 2.266 2.266 0 012.266 2.268zM63.617 14.412a2.421 2.421 0 11-2.42-2.421 2.419 2.419 0 012.42 2.421z"
                        />
                    </g>
                    <path
                        className="prefix__n"
                        d="M419.848 312.021a5.572 5.572 0 11-3.94-6.822 5.57 5.57 0 013.94 6.822zM467.781 289.545a2.64 2.64 0 11-1.866-3.233 2.64 2.64 0 011.866 3.233z"
                    />
                    <g transform="rotate(14.98 -620.507 1572.406)">
                        <circle
                            className="prefix__u"
                            cx={5.5}
                            cy={5.5}
                            r={5.5}
                            transform="translate(57)"
                        />
                        <circle
                            className="prefix__u"
                            cx={5.5}
                            cy={5.5}
                            r={5.5}
                            transform="translate(28 8)"
                        />
                        <circle
                            className="prefix__u"
                            cx={5.5}
                            cy={5.5}
                            r={5.5}
                            transform="translate(8 29)"
                        />
                        <circle
                            className="prefix__u"
                            cx={5.5}
                            cy={5.5}
                            r={5.5}
                            transform="translate(0 57)"
                        />
                        <circle
                            className="prefix__u"
                            cx={5.5}
                            cy={5.5}
                            r={5.5}
                            transform="translate(8 85)"
                        />
                        <circle
                            className="prefix__u"
                            cx={5.5}
                            cy={5.5}
                            r={5.5}
                            transform="translate(28 106)"
                        />
                        <circle
                            className="prefix__u"
                            cx={5.5}
                            cy={5.5}
                            r={5.5}
                            transform="translate(57 113)"
                        />
                        <circle
                            className="prefix__u"
                            cx={5.5}
                            cy={5.5}
                            r={5.5}
                            transform="translate(85 106)"
                        />
                        <circle
                            className="prefix__u"
                            cx={5.5}
                            cy={5.5}
                            r={5.5}
                            transform="translate(106 85)"
                        />
                        <circle
                            className="prefix__u"
                            cx={5.5}
                            cy={5.5}
                            r={5.5}
                            transform="translate(113 57)"
                        />
                        <circle
                            className="prefix__u"
                            cx={5.5}
                            cy={5.5}
                            r={5.5}
                            transform="translate(106 29)"
                        />
                        <circle
                            className="prefix__u"
                            cx={5.5}
                            cy={5.5}
                            r={5.5}
                            transform="translate(86 8)"
                        />
                    </g>
                </g>
                <g
                    transform="translate(-309 701)"
                    opacity={0.17}
                    stroke="#fff"
                    strokeWidth={2}
                    fill="none"
                >
                    <circle className="prefix__ab" cx={269.5} cy={269.5} r={269.5} />
                    <circle className="prefix__ac" cx={269.5} cy={269.5} r={268.5} />
                </g>
                <g
                    transform="translate(-199 818)"
                    opacity={0.42}
                    stroke="#fff"
                    strokeWidth={2}
                    fill="none"
                >
                    <circle className="prefix__ab" cx={157} cy={157} r={157} />
                    <circle className="prefix__ac" cx={157} cy={157} r={156} />
                </g>
                <g transform="translate(133 803.973)" opacity={0.22}>
                    <circle
                        className="prefix__y"
                        cx={41}
                        cy={41}
                        r={41}
                        transform="translate(0 .028)"
                    />
                    <path
                        className="prefix__y"
                        d="M41.015 30.761l-1.865 1.864 7.058 7.058H30.761v2.663h15.447l-7.058 7.058 1.864 1.864 10.254-10.253z"
                    />
                </g>
                <circle
                    cx={6}
                    cy={6}
                    r={6}
                    transform="translate(258 845)"
                    opacity={0.36}
                    fill="#fff"
                />
                <circle
                    cx={6}
                    cy={6}
                    r={6}
                    transform="translate(421 383)"
                    fill="#3cb6ca"
                />
            </g>
        </svg>
    )
}
