import * as React from 'react';
import { createMuiTheme, ThemeProvider } from '@material-ui/core';
import { createGlobalStyle } from 'styled-components';

import { getZIndex, shadow, theme } from '../theme';

const { colors: {
    main,
} } = theme;

const muiTheme = createMuiTheme({
    palette: {
        primary: {
            main,
        },
        secondary: {
            main: '#000000'
        },
    }
});

const GlobalStyles = createGlobalStyle`
    .MuiPopover-root {
        z-index: ${getZIndex('dialog')} !important;
    }

    #root .MuiPaper-root.MuiPaper-elevation1 {
        ${shadow()}
    }

    #menu-.MuiPopover-root .MuiMenu-paper {
        box-shadow: 0 3px 12px #00000029 !important;
        border-radius: 30px;
    }
`;

export const MaterialThemeProvider = ({ children }) => (
    <ThemeProvider theme={muiTheme}>
        <GlobalStyles />
        { children }
    </ThemeProvider>
);
