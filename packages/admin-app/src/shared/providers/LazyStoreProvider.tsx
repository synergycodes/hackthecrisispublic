import { useLocation } from 'react-router-dom';

import { mapLocationToStoreKeys } from '../utils/mapLocationToStoreKeys';
import { loadAsyncStore } from '../store/store';

const useLazyStore = () => {
    const location = useLocation();
    const keys = mapLocationToStoreKeys(location.pathname);
    loadAsyncStore(keys);
};

export const LazyStoreProvider = ({ children }) => {
    useLazyStore();

    return children;
};
