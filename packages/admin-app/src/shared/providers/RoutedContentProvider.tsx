import * as React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import {
    DASHBOARD_PATH,
    ENTRIES_PATH,
    INFECTED_PATH,
    LOGIN_PATH,
    INFECTED_DETAILS_PATH, ENTRIES_DETAILS_PATH
} from '../consts/navigation';
import { loadPageModule } from '../utils/loadPageModule';
import { Shell } from '../components/Shell/Shell';
import * as LoginPageModule from '../../login/index';
import * as DashboardPageModule from '../../dashboard/index';
import * as InfectedPageModule from '../../infected/index';
import * as InfectedDetailsPageModule from '../../InfectedDetails/index';
import * as EntriesPageModule from '../../entries/index';
import * as EntriesDetailsModule from '../../entriesDetails/index';

const LoginPage = loadPageModule(
    LOGIN_PATH,
    LoginPageModule
);

const DashboardPage = loadPageModule(
    DASHBOARD_PATH,
    DashboardPageModule
);

const InfectedPage = loadPageModule(
    INFECTED_PATH,
    InfectedPageModule
);

const InfectedDetailsPage = loadPageModule(
    INFECTED_PATH,
    InfectedDetailsPageModule
);

const EntriesPage = loadPageModule(
    ENTRIES_PATH,
    EntriesPageModule
);

const EntriesDetailsPage = loadPageModule(
    ENTRIES_DETAILS_PATH,
    EntriesDetailsModule
);

export const RoutedContentProvider = () => (
    <Switch>
        <Route path="/" exact>
            <Redirect to={LOGIN_PATH} />
        </Route>
        <Route path={LOGIN_PATH}>
            <LoginPage />
        </Route>
        <Shell>
            <Route path={DASHBOARD_PATH}>
                <DashboardPage />
            </Route>
            <Route path={INFECTED_PATH}>
                <Switch>
                    <Route path={INFECTED_DETAILS_PATH}>
                        <InfectedDetailsPage />
                    </Route>
                    <Route>
                        <InfectedPage />
                    </Route>
                </Switch>
            </Route>
            <Route path={ENTRIES_PATH}>
                <Switch>
                    <Route path={ENTRIES_DETAILS_PATH}>
                        <EntriesDetailsPage />
                    </Route>
                    <Route>
                        <EntriesPage />
                    </Route>
                </Switch>
            </Route>
        </Shell>
    </Switch>
);
