import * as React from 'react';
import { ApolloProvider as BaseApolloProvider } from '@apollo/react-hooks';
import { split } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-boost';

import { CONFIG } from '../../config';

const { APOLLO_SUBSCRIPTIONS_URL, APOLLO_URL } = CONFIG;

const wsLink = new WebSocketLink({
    uri: APOLLO_SUBSCRIPTIONS_URL,
    options: {
        reconnect: true
    }
});

const httpLink = new HttpLink({
    uri: APOLLO_URL
});

const link = split(
    ({ query }) => {
        const definition = getMainDefinition(query);
        return (
            definition.kind === 'OperationDefinition'
            && definition.operation === 'subscription'
        );
    },
    wsLink,
    httpLink,
);

const client = new ApolloClient({
    link,
    cache: new InMemoryCache()
});

type ApolloProviderProps = {
    children: React.ReactChild | React.ReactChild[];
};

export const ApolloProvider: React.FC<ApolloProviderProps> = (
    props: ApolloProviderProps
) => (
    <BaseApolloProvider client={client}>
        {props.children}
    </BaseApolloProvider>
);
