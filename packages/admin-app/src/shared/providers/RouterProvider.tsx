import * as React from 'react';
import { BrowserRouter } from 'react-router-dom';

type RoutingProviderProps = {
    loader: React.ReactChild;
}

export const RouterProvider: React.FC<RoutingProviderProps> = (
    { loader, children }
) => (
    <React.Suspense fallback={loader}>
        <BrowserRouter>
            { children }
        </BrowserRouter>
    </React.Suspense>
);
