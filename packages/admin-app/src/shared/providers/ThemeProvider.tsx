import * as React from 'react';
import {
    ThemeProvider as BaseThemeProvider,
    createGlobalStyle
} from 'styled-components';

import { theme } from '../theme';

const GlobalStyles = createGlobalStyle`
    body {
        font-family: 'Nunito', sans-serif;
        margin: 0;
        background: ${({ theme }) => theme.colors.background};
    }
`;

type ThemeProviderProps = {
    children: React.ReactChild | React.ReactChild[];
}

export const ThemeProvider: React.FC<ThemeProviderProps> = ({ children }) => (
    <BaseThemeProvider theme={theme}>
        <GlobalStyles />
        { children }
    </BaseThemeProvider>
);
