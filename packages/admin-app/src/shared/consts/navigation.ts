export const LOGIN_PATH = '/signin';
export const ENTRIES_PATH = '/entries';
export const ENTRIES_DETAILS_PATH = `${ENTRIES_PATH}/:id`;
export const DASHBOARD_PATH = '/dashboard';
export const INFECTED_PATH = '/infected';
export const INFECTED_DETAILS_PATH = `${INFECTED_PATH}/:id`;
