import * as _ from 'lodash/fp';
import { css } from 'styled-components';

type ShadowOptions = {
    x?: number,
    y?: number,
    blur?: number,
    spread?: number,
    opacity?: number
}

const omitUndefined = _.omitBy(
    _.isUndefined,
);

export const shadow = (options: ShadowOptions = {}) => {
    const { x, y, blur, spread, opacity }
        = {
            x: 0,
            y: 0,
            blur: 4,
            spread: 0,
            opacity: 0.15,
            ...omitUndefined(options)
        };

    return css`
        box-shadow:
            ${x}px ${y}px ${blur}px ${spread}px rgba(0, 0, 0, ${opacity});
    `;
};

type TransitionOptions = {
    properties?: string | string[];
    time?: number;
    type?: string;
}

export const transition = (options: TransitionOptions) => {
    const { properties, time, type } = {
        properties: 'all', time: 300, type: 'ease', ...omitUndefined(options)
    };

    return _.flowRight(
        (transitions: string) => `transition: ${transitions}`,
        _.join(', '),
        _.map((property: string) => `${time}ms ${type} ${property}`),
        _.castArray,
    )(properties);
};
