import * as React from 'react';
import styled from 'styled-components';

import { getColor } from '../shared/theme';

const Container = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: stretch;
    align-items: stretch;
    color: ${getColor('main')};
    margin-left: 48px;
`;

const Button = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: white;
    width: 214px;
    height: 40px;
    box-shadow: 0 3px 12px #00000029;
    border-radius: 24px;
    margin: 10px 0;
    cursor: pointer;
`;

export const Actions: React.FC = () => <Container>
    <div>Actions:</div>
    <Button>Send message</Button>
    <Button>Call</Button>
    <Button>Change status</Button>
</Container>;
