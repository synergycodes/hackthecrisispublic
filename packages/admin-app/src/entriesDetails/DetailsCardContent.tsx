import * as React from 'react';
import styled from 'styled-components';

import { BasicPersonDetails } from '../shared/components/BasicPersonDetails/BasicPersonDetails';
import { PersonDetailsData } from './personDetails.mock';
import { PersonDetailsEntry } from '../shared/components/PersonDetailsEntry/PersonDetailsEntry';
import { SymptomsTable } from '../shared/components/SymptomsTable/SymptomsTable';
import { getColor } from '../shared/theme';
import { Actions } from './Actions';
import { AdditionalInformation } from './AdditionalInformation';
import MapImage from '../shared/assets/mocks/map-infected-new.png';
import { Buttons } from './Buttons';

type Props = {
    personalDetail: PersonDetailsData;
};

const Container = styled.div`
    display: flex;
    flex-direction: column;
    flex-wrap: wrap;
    width: 100%;
`;

const MiddleColumn = styled.div`
    margin: 0 32px;
`;

const FlexContainer = styled.div`
    flex: 1;
    display: flex;
    justify-content: stretch;
    align-items: stretch;
`;

const LeftColumn = styled.div`
    display: flex;
    justify-content: stretch;
    align-items: stretch;
    flex-direction: column;
    flex: 5;

    > div > * {
        height: min-content;
    }

    > :not(:first-of-type) {
        margin-top: 60px;
    }
`;

const RightColumn = styled(LeftColumn)`
    flex: 4;
`;

const Symptoms = styled.div`
    display: flex;
    align-items: flex-start;
    flex-direction: column;
    color: ${getColor('main')};
    font-size: 30px;

    table {
        margin-top: 16px;
        margin-left: -24px;
    }
`;

const MapContainer = styled.div`
    margin-left: 120px;
    height: 100%;
    position: relative;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: stretch;
    box-shadow: 0 3px 12px #00000029;
    border-radius: 41px;
    padding: 16px;
    margin-bottom: 32px;
    font-size: 20px;
`;

const Map = styled.img`
    margin-bottom: 16px;
    width: 436px;
    height: 350px;
    align-self: center;
`;

export const DetailsCardContent: React.FC<Props> = ({ personalDetail }) => (
    <Container>
        <FlexContainer>
            <LeftColumn>
                <FlexContainer>
                    <BasicPersonDetails
                        name={personalDetail.name}
                        age={personalDetail.age}
                        email={personalDetail.email}
                        phone={personalDetail.phone}
                    />
                    <MiddleColumn>
                        <PersonDetailsEntry
                            label='Status'
                            content={personalDetail.status}
                        />
                        <PersonDetailsEntry
                            label='Category'
                            content={personalDetail.category}
                        />
                        <PersonDetailsEntry
                            label='Time'
                            content='09:34 am'
                        />
                    </MiddleColumn>
                    <Actions />
                </FlexContainer>
                <FlexContainer>
                    <Symptoms>
                        <div>Symptoms</div>
                        <SymptomsTable
                            temperature={personalDetail.temperature}
                            symptoms={personalDetail.symptoms}
                        />
                    </Symptoms>
                    <AdditionalInformation />
                </FlexContainer>
            </LeftColumn>
            <RightColumn>
                <MapContainer>
                    <Map src={MapImage} />
                    <div>ul. Piłsudskiego 120</div>
                    <div>50-020 Wrocław</div>
                </MapContainer>
            </RightColumn>
        </FlexContainer>
        <Buttons />
    </Container>
);
