import styled, { ThemeContext } from 'styled-components';
import * as React from 'react';

const StyledButton = styled.div<{ background: string }>`
    width: 223px;
    height: 48px;
    box-shadow: 0 3px 12px #00000029;
    border-radius: 24px;
    transition: 300ms ease all;
    color: white;
    font-size: 20px;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
    margin: 0 15px;
    background: ${({ background }) => background};

    :hover {
        filter: brightness(1.15);
    }
`;

const Container = styled.div`
    margin-top: 32px;
    display: flex;
    justify-content: flex-end;
`;

export const Buttons: React.FC = () => {
    const { colors: { main, secondary, tertiary } }
    = React.useContext(ThemeContext);

    return (
        <Container>
            <StyledButton background={main}>
                Observation
            </StyledButton>
            <StyledButton background={tertiary}>
                Quarantine
            </StyledButton>
            <StyledButton background={secondary}>
                Send an ambulance
            </StyledButton>
        </Container>
    );
};
