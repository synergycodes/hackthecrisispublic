import { Category, Symptoms } from '../shared/types/diseaseTypes';

export type PersonDetailsData = {
    id: string;
    name: string;
    surname: string;
    age: number;
    phone: string;
    email: string;
    status: string;
    category: Category;
    temperature: number;
    symptoms: Symptoms[];
}

export const person_details_mock: PersonDetailsData = {
    id: '1',
    name: 'Anna',
    surname: 'Kowalska',
    age: 46,
    phone: '345 657 242',
    email: 'anna.kowalska@mail.com',
    status: 'Waiting for reply',
    category: 'C',
    temperature: 38.9,
    symptoms: [ Symptoms.COUGHING, Symptoms.DYSPNOEA ]
};
