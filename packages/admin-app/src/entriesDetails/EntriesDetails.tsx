import * as React from 'react';
import styled from 'styled-components';
import { ChevronLeft } from 'react-feather';

import { getColor, getSize } from '../shared/theme';
import { DetailsCardContent } from './DetailsCardContent';
import { person_details_mock } from './personDetails.mock';
import { Navigation } from './Navigation';
import { useEntriesDetails } from './useEntriesDetails';
import { useParams } from 'react-router-dom';
import { usePersonDetails } from '../dashboard/PersonDetails/hooks/usePersonalDetails';
import { PersonDetails } from '../InfectedDetails/PersonDetails/PersonDetails';
import { MedicalHistory } from '../InfectedDetails/MedicalHistory/MedicalHistory';
import { InfectedTraceMap } from '../InfectedDetails/InfectedTraceMap/InfectedTraceMap';
import { InfectedActions } from '../InfectedDetails/InfectedActions/InfectedActions';

const Container = styled.div`
    position: relative;
    box-sizing: border-box;
    flex: 1;
    display: flex;
    flex-direction: column;
    justify-content: stretch;
    align-items: stretch;
    height: 100%;

    .MuiPaper-root {
        border-radius: ${getSize('cardBorderRadius')}px;
    }
`;

const DetailsCard = styled.div`
    border-radius: 41px;
    background-color: ${getColor('foreground')};
    box-shadow: 0 3px 12px #00000029;
    position: relative;
    display: flex;
    flex: 1;
    padding: 64px 90px;
    justify-content: space-between;
    margin:
        0
        ${getSize('menubarItem')}px
        calc(${getSize('menubarItem')}px);
`;

const BackButton = styled.div`
    height: calc(${getSize('menubarItem')}px * 1.5);
    margin-left: ${getSize('menubarItem')}px;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    font-size: 40px;
    color: ${getColor('main')};
    > * {
        stroke: ${getColor('main')};
        stroke-width: 3;
    }
    > :last-child {
        margin-left: 36px;
    }
`;


export const EntriesDetails: React.FC = () => {
    const personalDetails = useEntriesDetails();

    if (!personalDetails) {
        return null;
    }

    return (
        <Container>
            <BackButton>
                <ChevronLeft size={36}/>
                <div>
                    Applications
                </div>
            </BackButton>
            <DetailsCard>
                <DetailsCardContent personalDetail={personalDetails}/>
                <Navigation/>
            </DetailsCard>
        </Container>
    );
};
