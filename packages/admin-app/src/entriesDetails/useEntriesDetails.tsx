import gql from 'graphql-tag';
import { useParams } from 'react-router-dom';
import { useQuery } from '@apollo/react-hooks';
import { PatientExtended } from '../entries/dal/useAplicationsQuery';
import { Application } from '../shared/types/application';
import { getSymptoms } from '../dashboard/PersonDetails/hooks/usePersonalDetails';
import { QueryResult } from '@apollo/react-common';

const DETAILS_QUERY = gql`
    query profile(
        $id: Float!
    ) {
        patientProfile(id: $id) {
            id
            name
            surname
            score
            status
            category
            age
            phone
            email
            symptoms {
                coughing
                dyspoena
                lastBodyTemperature
            }
            newestFormState {
                createDate
            }
        }
    }
`;

// todo: remove any
const mapEntryDetails = (data: PatientExtended): Application | any => ({
    name: `${data.name} ${data.surname}`,
    id: String(data.id),
    date: new Date(data.newestFormState.createDate),
    status: data.status,
    temperature: Number(data.symptoms.lastBodyTemperature),
    category: data.category,
    age: data.age,
    city: 'Wroclaw',
    symptoms: getSymptoms(data),
    phone: data.phone || '+48 123 456 789',
    email: data.email || 'patient@mail.com',
    isolationTime: '5 days',
    firstSymptoms: '18.03.2020',
    lastActivity: '21.03.2020',
    additionalInformation: 'Extended description of the symptoms '
        + 'added by the patient',
});

export const useEntriesDetails = () => {
    const { id } = useParams();
    const result = useQuery<{ patientProfile: PatientExtended }>(
        DETAILS_QUERY,
        {
            variables: {
                id: id ? parseInt(id, 10) : 0,
            }
        }
    );

    const entryData = {
        ...result,
        data: result.data?.patientProfile
    } as any as QueryResult<PatientExtended>;

    if (!entryData.data) {
        return null;
    }

    return mapEntryDetails(entryData.data);
};
