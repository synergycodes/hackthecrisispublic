import * as React from 'react';
import styled from 'styled-components';

import { getColor } from '../shared/theme';

const Container = styled.div`
    flex: 1;
    margin-left: 48px;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    height: 100% !important;
`;

const TextBox = styled.div`
    width: 100%;
    border: 2px solid ${getColor('mainLight')};
    border-radius: 13px;
    padding: 16px;
    margin: 32px 0;
`;

// eslint-disable-next-line max-len
const ipsum = 'I feel very weak';

export const AdditionalInformation: React.FC = () => (
    <Container>
        <div>Additional information:</div>
        <TextBox>
            { ipsum }
        </TextBox>
    </Container>
);
