import * as React from 'react';
import styled from 'styled-components';

import { getColor } from '../shared/theme';
import Chevron from '../shared/assets/images/ChevronDetails.svg';

const StyledNavigation = styled.div`
    display: flex;
    padding-left: 40px;
    flex-direction: column;
    justify-content: space-between;
`;

const LeftButton = styled.div`
    position: absolute;
    border-radius: 50%;
    left: 0;
    top: 50%;
    transform: translate(-50%, -50%);
    color: ${getColor('main')};
    width: 124px;
    height: 124px;
    background-color: ${getColor('foreground')};
    box-shadow: 0 3px 12px #00000029;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
    transition: 300ms ease all;

    > * {
        transform: rotate(180deg);
        margin-right: 8px;
    }

    :hover {
        filter: brightness(1.15);
    }
`;

const RightButton = styled(LeftButton)`
    left: unset;
    right: 0;
    transform: translate(50%, -50%);

    > * {
        transform: none;
        margin-right: 0;
        margin-left: 8px;
    }
`;

export const Navigation: React.FC = () => (
    <StyledNavigation>
        <LeftButton>
            <img src={Chevron} alt={'chevron'} />
        </LeftButton>
        <RightButton>
            <img src={Chevron} alt={'chevron'} />
        </RightButton>
    </StyledNavigation>
);
