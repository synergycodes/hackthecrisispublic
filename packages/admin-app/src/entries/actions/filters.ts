import {
    PatientAgeRange,
    PatientCategory,
    PatientStatus
} from '@hackthecrisis/common/src';
import { Action } from 'redux';

export const SET_CATEGORY_FILTER = '@entriesFilters/SET_CATEGORY_FILTER';
type SetCategoryFilterAction = Action<typeof SET_CATEGORY_FILTER> & {
    payload: PatientCategory
};

export const setCategoryFilter
    = (type?: PatientCategory): SetCategoryFilterAction => ({
        type: SET_CATEGORY_FILTER,
        payload: type });

export const SET_AGE_FILTER = '@entriesFilters/SET_AGE_FILTER';
type SetAgeFilterAction = Action<typeof SET_AGE_FILTER> & {
    payload: PatientAgeRange
};

export const setAgeFilter
    = (type?: PatientAgeRange): SetAgeFilterAction => ({
        type: SET_AGE_FILTER,
        payload: type
    });

export const SET_STATUS_FILTER = '@entriesFilters/SET_STATUS_FILTER';
type SetStatusFilterAction = Action<typeof SET_STATUS_FILTER> & {
    payload: PatientStatus
};

export const setStatusFilter
    = (type?: PatientStatus): SetStatusFilterAction => ({
        type: SET_STATUS_FILTER,
        payload: type
    });

export const SET_TIME_FILTER = '@entriesFilters/SET_TIME_FILTER';
type SetTimeFilterAction = Action<typeof SET_TIME_FILTER> & {
    payload: PatientCategory
};

export const setTimeFilter
    = (type: PatientCategory): SetTimeFilterAction => ({
        type: SET_TIME_FILTER,
        payload: type
    });

export type FilterActions =
    | SetCategoryFilterAction
    | SetAgeFilterAction
    | SetStatusFilterAction
    | SetTimeFilterAction;
