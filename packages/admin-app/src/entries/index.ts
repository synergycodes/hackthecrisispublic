import { entriesFilters } from './reducers/entriesFilters';

export { Entries as component } from './Entries';
export const reducers = {
    entriesFilters
};
