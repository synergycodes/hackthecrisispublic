import { RequestTimeRange } from '@hackthecrisis/common/src';

import { EntriesState } from '../types/EntriesState';

export const entriesFiltersSelector = (state: EntriesState) =>
    state.entriesFilters
        ?? {
            categories: [],
            ageRanges: [],
            statuses: [],
            timeRange: RequestTimeRange.Today
        };
