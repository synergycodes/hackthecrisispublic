import * as React from 'react';
import styled from 'styled-components';

import { getColor } from '../shared/theme';
import { ApplicationsList } from './ApplicationsList/ApplicationsList';
import { FiltersConnected } from './FiltersConnected';

export const Title = styled.h1`
    color: ${getColor('main')};
    font-weight: normal;
`;

export const Container = styled.div`
    padding: 20px 50px 20px 50px;
    display: flex;
    flex-direction: column;
    width: 100%;
    overflow-y: scroll;
`;

export const Entries: React.FC = () => (
    <Container>
        <Title>Applications</Title>
        <FiltersConnected />
        <ApplicationsList/>
    </Container>
);
