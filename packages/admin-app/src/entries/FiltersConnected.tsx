import * as React from 'react';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import {
    Container as FilterContainer,
    FilterSingle
} from '../shared/components/Filters/FilterSingle';
import { entriesFiltersSelector } from './selectors/entriesFilters';
import { ViewSelect } from '../shared/components/Filters/ViewSelect';
import { Filters } from '../shared/components/Filters/Filters';

const Container = styled.div`
    display: flex;
    flex-direction: row;
    margin-bottom: 25px;
    ${FilterContainer} {
        margin-right: 50px;
    }
`;

export const FiltersConnected: React.FC = () => {
    const filters = useSelector(entriesFiltersSelector);

    return (
        <Container>
            <Filters filters={filters}/>
            <FilterSingle
                title='Time'
                options={[
                    { name: 'A', value: 'A' },
                    { name: 'B', value: 'B' },
                    { name: 'C', value: 'C' },
                ]}/>
            <ViewSelect/>
        </Container>
    );
};
