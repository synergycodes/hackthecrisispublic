import { PatientProfileFilterInput } from '@hackthecrisis/apollo/src/modules/patientProfile/patientProfileFilter.type';
import {
    PatientAgeRange,
    PatientCategory,
    PatientStatus
} from '@hackthecrisis/common/src';
import gql from 'graphql-tag';
import { PatientProfile } from '@hackthecrisis/common';
import * as _ from 'lodash/fp';
import { useQuery } from '@apollo/react-hooks';
import { QueryResult } from '@apollo/react-common';

const APPLICATIONS_QUERY = gql`
    query applications(
        $filter: PatientProfileFilterInput
    ) {
        patientProfiles(filter: $filter) {
            id
            name
            surname
            score
            status
            category
            age
            phone
            email
            symptoms {
                coughing
                dyspoena
                lastBodyTemperature
            }
            newestFormState {
                createDate
            }
        }
    }

`;
const APPLICATION_QUERY = gql`
    query applications(
        $id: Float!
    ) {
        patientProfile(id: $id) {
            id
            name
            surname
            score
            status
            category
            age
            phone
            email
            symptoms {
                coughing
                dyspoena
                lastBodyTemperature
            }
            newestFormState {
                createDate
            }
        }
    }
`;

export type PatientExtended = PatientProfile & {
    age: number;
    symptoms: {
        coughing?: boolean;
        dyspoena?: boolean;
        lastBodyTemperature: number;
    };
    newestFormState: {
        createDate: Date;
    };
};

const CATEGORY_MAP = {
    [PatientCategory.A]: 'A',
    [PatientCategory.B]: 'B',
    [PatientCategory.C]: 'C'
};

const STATUS_MAP = {
    [PatientStatus.NewApplication]: 'NewApplication',
    [PatientStatus.Infected]: 'Infected',
    [PatientStatus.InQuarantine]: 'InQuarantine',
    [PatientStatus.Healthy]: 'Healthy',
};

const AGE_MAP = {
    [PatientAgeRange.From1To10]: 'From1To10',
    [PatientAgeRange.From11To20]: 'From11To20',
    [PatientAgeRange.From21To30]: 'From21To30',
    [PatientAgeRange.From31To40]: 'From31To40',
    [PatientAgeRange.From41To50]: 'From41To50',
    [PatientAgeRange.From51To60]: 'From51To60',
    [PatientAgeRange.From61To70]: 'From61To70',
    [PatientAgeRange.From71To80]: 'From71To80',
    [PatientAgeRange.From81To90]: 'From81To90',
    [PatientAgeRange.From91To100]: 'From91To100',
};

// @ts-ignore
const mapEnum = (obj) => _.map(_.get(_, obj));

export const useApplicationsQuery = (filter?: PatientProfileFilterInput) => {
    const result = useQuery<{ patientProfiles: PatientExtended[] }>(
        APPLICATIONS_QUERY,
        filter && {
            variables: {
                filter: {
                    statuses: mapEnum(STATUS_MAP)(filter?.statuses),
                    ageRanges: mapEnum(AGE_MAP)(filter?.ageRanges),
                    categories: mapEnum(CATEGORY_MAP)(filter?.categories)
                }
            }
        }
    );

    return {
        ...result,
        data: _.flowRight(
            _.reverse,
            _.sortBy((p) => new Date(p.newestFormState?.createDate).getTime())
        )(result.data?.patientProfiles)
    } as any as QueryResult<PatientExtended[]>;
};

export const useApplicationQuery = (id: number) => {
    const result = useQuery<{ patientProfile: PatientExtended }>(
        APPLICATION_QUERY,
        { variables: { id } }
    );

    return {
        ...result,
        data: result.data?.patientProfile
    } as any as QueryResult<PatientExtended>;
};
