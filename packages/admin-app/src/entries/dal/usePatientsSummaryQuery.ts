import gql from 'graphql-tag';
import { useQuery } from '@apollo/react-hooks';
import { QueryResult } from '@apollo/react-common';

const PATIENTS_SUMMARY_QUERY = gql`
    query statistics {
        patientsSummary {
            infected
            quarantine
            recovered
        }
    }
`;

export type PatientsSummary = {
    infected: number;
    quarantine: number;
    recovered: number;
};

export const usePatientsSummaryQuery = () => {
    const result = useQuery<
    { patientsSummary: PatientsSummary }
    >(
        PATIENTS_SUMMARY_QUERY
    );

    return {
        ...result,
        data: result.data?.patientsSummary
    } as any as QueryResult<PatientsSummary>;
};
