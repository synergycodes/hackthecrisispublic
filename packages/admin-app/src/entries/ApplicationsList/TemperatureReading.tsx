import * as React from 'react';
import styled from 'styled-components';

import { getColor } from '../../shared/theme';

type TemperatureProps = {
    value: number;
};

const Container = styled.div<TemperatureProps>`
    border: 1px solid ${({ value, ...rest }) => {
        if (value >= 39)
            return getColor('accentRed')(rest);
        if (value >= 38)
            return getColor('accentOrange')(rest);
        return getColor('accentGreen')(rest);
    }};
    color: ${({ value, ...rest }) => {
        if (value >= 39)
            return getColor('accentRed')(rest);
        if (value >= 38)
            return getColor('accentOrange')(rest);
        return getColor('accentGreen')(rest);
    }};
    padding: 0.2em 0.7em;
    border-radius: 30px;
    font-size: 25px;
    white-space: nowrap;
`;

export const TemperatureReading: React.FC<TemperatureProps> = ({
    value
}) => (
    <Container value={value}>
        {value.toFixed(1)} ℃
    </Container>
);
