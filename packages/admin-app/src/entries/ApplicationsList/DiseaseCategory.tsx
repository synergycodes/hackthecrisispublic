import * as React from 'react';
import styled from 'styled-components';

import { getColor } from '../../shared/theme';

type Props = {
    category: 'A' | 'B' | 'C'
};

export const Circle = styled.div<Props>`
    border-radius: 1000px;
    color: ${getColor('foreground')};
    background-color: ${({ category, ...rest }) => {
        if (category === 'C')
            return getColor('accentRed')(rest);
        if (category === 'B')
            return getColor('accentOrange')(rest);
        return getColor('accentGreen')(rest);
    }};
    text-transform: uppercase;
    text-align: center;
    font-size: 25px;
    padding: 0.5em;
    flex: 0 0 1.4em;
`;

export const DiseaseCategory: React.FC<Props> = ({
    category
}) => (
    <Circle category={category}>
        {category}
    </Circle>
);
