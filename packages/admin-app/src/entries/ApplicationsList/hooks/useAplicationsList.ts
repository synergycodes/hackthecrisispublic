import * as _ from 'lodash/fp';
import { useSelector } from 'react-redux';

import {
    PatientExtended,
    useApplicationsQuery
} from '../../dal/useAplicationsQuery';
import { Application, Symptoms } from '../ApplicationList.types';
import { entriesFiltersSelector } from '../../selectors/entriesFilters';

export const getSymptoms = (app: PatientExtended): Symptoms[] =>
    _.filter(_.identity, [
        app.symptoms.coughing && Symptoms.COUGHING,
        app.symptoms.dyspoena && Symptoms.DYSPNOEA,
    ]) as Symptoms[];

const mapApplications = _.map((app: PatientExtended): Application => ({
    name: `${app.name} ${app.surname}`,
    id: String(app.id),
    date: new Date(app.newestFormState.createDate),
    status: app.status,
    temperature: Number(app.symptoms.lastBodyTemperature),
    category: app.category,
    age: app.age,
    city: 'Wroclaw',
    symptoms: getSymptoms(app)
}));

export const useApplicationsList = (): Application[] => {
    const filters = useSelector(entriesFiltersSelector);
    const { data } = useApplicationsQuery(filters);

    if (!data)
        return [];

    return mapApplications(data);
};
