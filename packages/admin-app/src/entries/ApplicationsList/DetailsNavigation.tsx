import * as React from 'react';
import styled from 'styled-components';
import { Link as BaseLink } from 'react-router-dom';

import { getColor } from '../../shared/theme';

export const Link = styled(BaseLink)`
    color: ${getColor('tertiary')};
    font-size: 23px;
    font-weight: bold;
    text-decoration: none;
    transition: color 100ms linear;

    &:hover {
        color: ${getColor('main')};
    }
`;

const Container = styled.div`
    display: flex;
    flex: 0 0 75px;
    overflow: hidden;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`;

type NavigationProps = {
    id: string;
};

export const DetailsNavigation: React.FC<NavigationProps> = ({
    id
}) => (
    <Container>
        <Link to={`/entries/${id}`}>Details</Link>
    </Container>
);
