import * as React from 'react';
import styled from 'styled-components';

import { getColor } from '../../shared/theme';

type CaseCardProps = {
    title: string;
    value: string;
    isEmphasized?: boolean;
};

const CaseContainer = styled.div<{ isEmphasized: boolean }>`
    display: flex;
    flex-direction: column;
    flex: ${({ isEmphasized }) => isEmphasized ? '1 1': '0 1 140px'};
    overflow: hidden;
    padding: 0 5px;
`;

const Title = styled.div`
    color: ${getColor('gray')};
    font-size: 18px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
`;

const Value = styled.div<{ isEmphasized: boolean }>`
    font-size: ${({ isEmphasized }) => isEmphasized ? '23px' : '18px'};
    font-weight: ${({ isEmphasized }) => isEmphasized ? 'bold' : 'normal'};
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
`;

const CaseCard: React.FC<CaseCardProps> = ({
    title,
    value,
    isEmphasized = false
}) => (
    <CaseContainer isEmphasized={isEmphasized}>
        <Title title={title}>{title}</Title>
        <Value
            title={value}
            isEmphasized={isEmphasized}>
            {value}
        </Value>
    </CaseContainer>
);

const Container = styled.div`
    display: flex;
    flex-direction: row;
    flex: 1 1;
`;

type CaseProps = {
    name: string;
    city: string;
    date: Date;
    age: number;
    id: string;
    status: string;
};

const pad = (v: number) => String(v).padStart(2, '0');

export const CaseDetails: React.FC<CaseProps> = ({
    name,
    city,
    date,
    age,
    id,
    status
}) => (
    <Container>
        <CaseCard
            title='Name'
            value={name}
            isEmphasized />
        <CaseCard
            title='City'
            value={city} />
        <CaseCard
            title='Time'
            value={`${pad(date.getHours())}:${pad(date.getMinutes())}`} />
        <CaseCard
            title='Age'
            value={age?.toString() ?? ''} />
        <CaseCard
            title='Application ID'
            value={id} />
        <CaseCard
            title='Status'
            value={status} />
    </Container>
);
