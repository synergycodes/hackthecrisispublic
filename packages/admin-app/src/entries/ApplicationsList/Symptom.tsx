import * as React from 'react';

import coughingUrl from './assets/coughing.svg';
import dyspnoeaUrl from './assets/dyspnoea.svg';
import { Symptoms } from './ApplicationList.types';

const SymptomImage = {
    [Symptoms.COUGHING]: coughingUrl,
    [Symptoms.DYSPNOEA]: dyspnoeaUrl
};

type SymptomProps = {
    value: Symptoms
};

export const Symptom: React.FC<SymptomProps> = ({ value }) => (
    <img alt={value} src={SymptomImage[value]} />
);
