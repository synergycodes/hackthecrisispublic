import * as React from 'react';
import styled from 'styled-components';
import * as _ from 'lodash/fp';

import { DiseaseCategory } from './DiseaseCategory';
import { DetailsSection } from './DetailsSection';
import { CaseDetails } from './CaseDetails';
import { DetailsNavigation } from './DetailsNavigation';
import { Application } from './ApplicationList.types';
import { SectionContainer } from '../../shared/components/SectionContainer';

type ListItemProps = Application;

export const ListItemContainer = styled(SectionContainer)`
    > div {
        margin-left: 30px;
    }
`;

export const ListItem: React.FC<ListItemProps> = ({
    category,
    symptoms,
    temperature,
    ...rest
}) => {
    const caseDetailsProps = _.pick([
        'name',
        'city',
        'date',
        'age',
        'id',
        'status',
    ], rest);
    return (
        <ListItemContainer>
            <DiseaseCategory category={category}/>
            <CaseDetails {...caseDetailsProps} />
            <DetailsSection
                symptoms={symptoms}
                temperature={temperature}/>
            <DetailsNavigation id={rest.id}/>
        </ListItemContainer>
    );
};
