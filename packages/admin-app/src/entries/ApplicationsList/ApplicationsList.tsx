import * as React from 'react';
import styled from 'styled-components';

import { ListItem, ListItemContainer } from './ListItem';
import { useApplicationsList } from './hooks/useAplicationsList';

const Container = styled.div`
    display: flex;
    flex-direction: column;

    ${ListItemContainer} {
        margin-bottom: 27px;
    }
`;

export const ApplicationsList: React.FC = () => {
    const applications = useApplicationsList();

    if (!applications)
        return <Container>No applications currently :)</Container>;

    return (
        <Container>
            {applications.map((item) => (
                <ListItem key={item.id} {...item} />
            ))}
        </Container>
    );
};
