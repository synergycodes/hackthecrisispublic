import * as React from 'react';
import styled from 'styled-components';

import { TemperatureReading } from './TemperatureReading';
import { Symptom } from './Symptom';
import { getColor } from '../../shared/theme';
import { Symptoms } from './ApplicationList.types';

const Container = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    width: 320px;
    border-right: 1px solid ${getColor('mainLight')};
    border-left: 1px solid ${getColor('mainLight')};
    padding: 0 37px 0 0;
    flex: 0 0 320px;

    > div, > img {
        margin-left: 37px;
    }
`;

type DetailsProps = {
    symptoms: Symptoms[];
    temperature: number;
};

export const DetailsSection: React.FC<DetailsProps> = ({
    symptoms, temperature
}) => (
    <Container>
        <TemperatureReading value={temperature}/>
        {symptoms.map((symptom) => (
            <Symptom key={symptom} value={symptom}/>
        ))}
    </Container>
);
