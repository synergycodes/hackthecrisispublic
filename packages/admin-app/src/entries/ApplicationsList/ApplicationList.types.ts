import { PatientStatus } from '@hackthecrisis/common';

export enum Symptoms {
    COUGHING = 'COUGHING',
    DYSPNOEA = 'DYSPNOEA'
}

type Category = 'A' | 'B' | 'C'

export type Application = {
    category: Category,
    date: Date,
    city: string,
    temperature: number,
    symptoms: Symptoms[],
    age: number;
    id: string,
    name: string;
    status: PatientStatus;
};
