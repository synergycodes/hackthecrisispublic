import { PatientProfileFilterInput } from '@hackthecrisis/apollo/src/modules/patientProfile/patientProfileFilter.type';

export type EntriesState = {
    entriesFilters: PatientProfileFilterInput
};
