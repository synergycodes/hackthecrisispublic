import { FilterActions } from '../actions/filters';

export type EntriesAction =
    | FilterActions;
