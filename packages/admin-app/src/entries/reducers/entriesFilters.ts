import { PatientProfileFilterInput } from '@hackthecrisis/apollo/src/modules/patientProfile/patientProfileFilter.type';
import { PatientStatus, RequestTimeRange } from '@hackthecrisis/common/src';

import { EntriesAction } from '../types/EntriesAction';
import {
    SET_AGE_FILTER,
    SET_CATEGORY_FILTER,
    SET_STATUS_FILTER
} from '../actions/filters';

type EntriesState = PatientProfileFilterInput;

const initialState: EntriesState = {
    statuses: [
        PatientStatus.NewApplication,
        PatientStatus.InQuarantine
    ],
    ageRanges: [],
    categories: [],
    timeRange: RequestTimeRange.Today,
};

export const entriesFilters
    = (state: EntriesState = initialState, action: EntriesAction) => {
        switch (action.type) {
            case SET_CATEGORY_FILTER:
                return {
                    ...state,
                    categories: action.payload
                        ? [action.payload]
                        : initialState.categories
                };

            case SET_STATUS_FILTER:
                return {
                    ...state,
                    statuses: action.payload
                        ? [action.payload]
                        : initialState.statuses
                };

            case SET_AGE_FILTER:
                return {
                    ...state,
                    ageRanges: action.payload
                        ? [action.payload]
                        : initialState.ageRanges
                };

            default:
                return state;
        }
    };
