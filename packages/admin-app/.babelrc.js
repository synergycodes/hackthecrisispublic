module.exports = {
    presets: [
        [
            '@babel/preset-env',
            {
                targets: {
                    node: 'current'
                }
            }
        ],
        '@babel/preset-typescript',
        '@babel/preset-react',
    ],
    plugins: [
        [
            'babel-plugin-styled-components',
            {
                displayName: true
            }
        ],
        '@babel/proposal-class-properties',
        '@babel/proposal-optional-chaining',
        '@babel/proposal-nullish-coalescing-operator',
        '@babel/proposal-object-rest-spread',
        'lodash'
    ]
};
