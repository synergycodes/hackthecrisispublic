import 'jest-enzyme';
import 'jest-styled-components';
import dotenv from 'dotenv';
import jestFetchMock from 'jest-fetch-mock';

dotenv.config();
(jestFetchMock as any).enableMocks();

(global as any).IntersectionObserver = class IntersectionObserver {
    constructor() { }

    observe() {
        return null;
    }

    unobserve() {
        return null;
    }
};
