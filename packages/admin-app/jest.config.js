module.exports = {
    clearMocks: true,
    collectCoverageFrom: ['src/**/*.{ts,tsx}'],
    coverageDirectory: 'coverage',
    moduleNameMapper: {
        '\\.svg': '<rootDir>/src/__mocks__/svgMock.tsx',
        '\\.(css|jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$': '<rootDir>/src/__mocks__/filesMock.ts',
    },
    transform:{
        "\\.(js|ts)x?$": ['babel-jest', {
            configFile: './.babelrc.js'
        }]
    },
    setupFilesAfterEnv: ['./jest-setup.ts'],
    snapshotSerializers: [
        'enzyme-to-json/serializer'
    ],
    testEnvironment: 'enzyme',
    testEnvironmentOptions: {
        enzymeAdapter: 'react16'
    }
};
