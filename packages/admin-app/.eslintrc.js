module.exports = {
    'extends': [
        'plugin:react/recommended',
        '../common/.eslintrc.js'
    ],
    'plugins': [
        'react-hooks'
    ],
    'settings': {
        'react': {
            'version': 'detect'
        }
    },
    'rules': {
        'react-hooks/rules-of-hooks': 'error',
        'react-hooks/exhaustive-deps': 'warn',
        'react/prop-types': 'off',
        'react/jsx-no-useless-fragment': 'error'
    }
};
