const helpers = require('@hackthecrisis/common/lintstaged');

module.exports = {
    'src/**/*.{js,ts,tsx}': (filenames) => {
        const ts = helpers.getTSLinting(filenames);
        const style = helpers.getStyleLinting(filenames);

        return ts.concat(style);
    }
};
