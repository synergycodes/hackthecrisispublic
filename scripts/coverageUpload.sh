#!/bin/sh

set -e

CURRENT_DIR_NAME=$(basename "$PWD" | sed 's!-!!')

if [ -z "$(ls | grep coverage)" ]; then
    echo "No coverage found for $CURRENT_DIR_NAME."
    exit 0;
fi

yarn codecov -F "$CURRENT_DIR_NAME" -p ../../
