# EpidemicApp

## Prerequisites

- Docker (https://docs.docker.com/install/)
- NodeJS (https://nodejs.org/en/)
- Android SDK (https://developer.android.com/studio)
- macOS/Windows/Linux (some steps might vary between platforms)

## Installation

1. Open command line in the main catalog (using Administrator account on the Windows)
2. `npm install -g yarn expo-cli`
3. `yarn`

## Running Admin application and backend server

1. Open command line in the main catalog
2. `docker-compose up`
3. Go to `http://localhost` in the web browser

## Running Mobile application

1. Open command line in the main catalog (using Administrator account on the Windows)
2. `cd packages/mobileapp`
3. `yarn start`
4. The command runs a website in the browser with control panel for mobile app development. It is going to be needed later.
5. Configure Android emulator using this website (https://developer.android.com/studio/run/managing-avds)
6. Run any Android 9+ emulator.
7. Run in the command line `adb reverse tcp:4000 tcp:4000`
8. On the website from the point 4. click `Run on Android device/emulator` button.
9. You application is up and running.
